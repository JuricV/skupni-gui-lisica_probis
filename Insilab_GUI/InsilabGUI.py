# -*- coding: utf-8 -*-
"""
@author: Vito Jurić
"""

from __future__ import absolute_import
from __future__ import print_function
import os
#import stat
import urllib.request, urllib.error, urllib.parse
from urllib.request import urlopen
from urllib.error import URLError, HTTPError
import zipfile
import tempfile
import shutil
import sys
import platform
from distutils.dir_util import copy_tree
import time
from threading import Thread
import contextlib

from pymol import cmd
from pymol.Qt import QtWidgets, QtCore
from pymol.Qt.utils import loadUi


def __init_plugin__(app=None):
    '''
    Add an entry to the PyMOL "Plugin" menu
    '''
    from pymol.plugins import addmenuitemqt
    addmenuitemqt("Insilab GUI", lambda: run())

dialog = None

class UpdateCheck:

    def __init__(self):
        # Remember to change links
        self.firstVersionURL = "https://gitlab.com/JuricV/skupni-gui-lisica_probis/-/raw/master/Insilab_GUI/.InsilabGUI/version.txt"
        self.firstArchiveURL = "https://gitlab.com/JuricV/skupni-gui-lisica_probis/-/archive/master/skupni-gui-lisica_probis-progress.zip?path=CommonGUI/.InsilabGUI"
        self.currentVersion=""
        self.latestVersion =""


    def installationFailed(self):
        global running, status
        status = "failed"
        running = False

    def deleteTmpDir(self):
        try:
            shutil.rmtree(self.tmpDir)
            print(("deleted temporary ", self.tmpDir))
        except:
            print ("error : could not remove temporary directory")
            
    def createTmpDir(self):
        try:
            self.tmpDir= tempfile.mkdtemp()
            print(("created temporary ", self.tmpDir))
        except:
            print ("error : could not create temporary directory")
            self.installationFailed()
        self.zipFileName=os.path.join(self.tmpDir, "skupni-gui-lisica_probis-progress-CommonGUI-.InsilabGUI.zip")

    def downloadInstall(self):
        try:
            print ("Fetching plugin from the git server")
            urlcontent = urlopen(self.firstArchiveURL, timeout=5)
            zipcontent = urlcontent.read()
            H2OzipFile = open(self.zipFileName, 'wb')
            H2OzipFile.write(zipcontent)
        except HTTPError as e1:
            print(("HTTP Error:", e1.code, e1.reason))
            self.installationFailed()
        except URLError as e2:
            print(("URL Error:", e2.reason))
            self.installationFailed()
        except Exception as e:
            print ("Error: download failed, check if http://gitlab.com website is up...")
            self.installationFailed()
        finally:
            try:
                H2OzipFile.close()
            except:
                pass
            try:
                urlcontent.close()
            except:
                pass

    def extractInstall(self):        
        try:
            with contextlib.closing(zipfile.ZipFile(self.zipFileName, "r")) as InsilabGUIzip:
                for member in InsilabGUIzip.namelist():
                    masterDir = os.path.dirname(member)
                    break
                InsilabGUIzip.extractall(self.tmpDir)
            copy_tree(os.path.join(self.tmpDir, masterDir, "CommonGUI", ".InsilabGUI"), INSILAB_DIRECTORY)
        except:
            print ("installation of InsilabGUI failed")
            self.installationFailed()

    def firstUpgrade(self):
        return not os.path.isdir(INSILAB_DIRECTORY)

    def upgrade(self):
        
        self.createTmpDir()
        self.downloadInstall()
        self.extractInstall()
        self.deleteTmpDir()
        sys.path.append(os.path.normpath(os.path.join(INSILAB_DIRECTORY, "modules")))
        self.findLatestVersion()
        #self.writeToVersionTxt(self.latestVersion)

        print ("Upgrade finished successfully!")
        global running, status
        status = "start"
        running = False
        
    def findCurrentVersion(self):
        global currentVersion
        if os.path.isfile(versionFile):
            with open(versionFile, 'r') as myFile:
                lines = myFile.readlines()
                self.currentVersion = lines[0].strip()
                currentVersion = self.currentVersion
        else:
            self.currentVersion="0.0.0"
            currentVersion = "0.0.0"

    def findLatestVersion(self):
        
        try:
            global latestVersion
            versionFile = urllib.request.urlopen(self.firstVersionURL, timeout=5)
            print("TEST version file:    ", str(versionFile))
            self.latestVersion = versionFile.read().decode(encoding="utf-8").strip()
            if len(self.latestVersion) > 6:
                self.latestVersion = ""
            latestVersion = self.latestVersion
            print("TEST Latest version:   ", latestVersion)
            
        except HTTPError as e1:
            print(("HTTP Error:", e1.code, e1.reason))
        except URLError as e2:
            print(("URL Error:", e2.reason))
        except Exception as e:
            print ("URL Error: Is Insilab webpage (http://insilab.org) down?")

    def writeToVersionTxt(version, reminder=False):
        if reminder:
            with open(versionFile, 'a') as myfile:
                myfile.write(str(version)+"\t later \t"+time.strftime("%d/%m/%Y")+"\n")
        else:
            with open(versionFile, 'w') as myfile:
                myfile.write((str(version)+"\n").encode("ascii"))

    def updateBox(self):
        global updateDialog

        updateDialog = self.updateWindow()

        updateDialog.show()
    
    def updateWindow(self):
        global updateDialog
        updateDialog = QtWidgets.QMainWindow()

        uifile = os.path.join(UI_DIRECTORY, 'GUIupdate.ui')
        form = loadUi(uifile, updateDialog)

        def cancelButton():
            global status
            if form.checkBoxAsk.isChecked():
                self.writeToVersionTxt(self.latestVersion, True)
            #status = "start"
            updateDialog.close()
            start_common_gui()
            

        def updateButton(vent=None):
            global status
            updateDialog.close()
            self.upgrade()
            updateDialog.close()
            start_common_gui()
            
        self.findLatestVersion()
        self.findCurrentVersion()
    
        form.labelVersionLatest.setText(str(self.latestVersion))
        form.labelVersionCurrent.setText(str(self.currentVersion))
        form.buttonBoxUpdate.accepted.connect(lambda: updateButton())
        form.buttonBoxUpdate.rejected.connect(lambda: cancelButton())
    
        return updateDialog
def internetOn():
        try:
            urllib.request.urlopen('http://insilab.org',timeout=5)
            return True
        except:
            return False
        
def start_common_gui():
    sys.path.append(os.path.normpath(os.path.join(INSILAB_DIRECTORY, "modules")))
    import Insilab_GUI_plugin
    Insilab_GUI_plugin.main()
    
class CustomDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle("Choose Directory")
    def find_dir(self):
        folderpath = str(QtWidgets.QFileDialog.getExistingDirectory(CustomDialog(), "Select Directory"))
        if folderpath.endswith(".InsilabGUI") or folderpath.endswith(".InsilabGUI"+os.path.sep):
            pass
        else:
            folderpath=os.path.join(folderpath, ".InsilabGUI")
        self.lineEdit.setText(folderpath)
    def set_dir(self):
        global SetDir
        SetDir = self.lineEdit.text()        
        if os.path.isdir(SetDir):
            install_datoteka = open(install_dir, "w")
            install_datoteka.write(SetDir)
            install_datoteka.close()
            InstallDialog.close()
        else:
            QtWidgets.QMessageBox.about(dialog, "InsilabGUI Install Warning", "Please choose a valid install directory!")
            print("Please choose a valid install directory!")
            
    def setupUI(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(640, 148)
        Dialog.setMaximumSize(QtCore.QSize(16777215, 210))
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 2)
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 1, 0, 1, 1)
        HOME_DIRECTORY=os.path.expanduser('~')
        DEFAULT_DIRECTORY=os.path.join(HOME_DIRECTORY, ".InsilabGUI")
        self.lineEdit.setText(DEFAULT_DIRECTORY)
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 1, 1, 1, 1)
        self.pushButton.clicked.connect(self.find_dir)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.pushOK = QtWidgets.QPushButton(Dialog)
        self.pushOK.setObjectName("pushOK")
        self.gridLayout.addWidget(self.pushOK, 3, 1, 1, 1)
        self.pushOK.clicked.connect(self.set_dir)
        

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        
    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("Dialog", "Please choose install directory for InsilabGUI:"))
        self.pushButton.setText(_translate("Dialog", "Pick directory"))
        self.label_2.setText(_translate("Dialog", "*If InsilabGUI has been manually installed please input .InsilabGUI file."))
        self.pushOK.setText(_translate("Dialog", "OK"))

def run_custom_dialog():
    global InstallDialog
    InstallDialog=QtWidgets.QDialog()
    InstallDialog.ui = CustomDialog()
    InstallDialog.ui.setupUI(InstallDialog)
    InstallDialog.show()
    InstallDialog.exec_()
    
def NoInstallSetting():
    global SetDir
    run_custom_dialog()
    if not SetDir=="":
        install_datoteka = open(install_dir, "w")
        install_datoteka.write(SetDir)
        install_datoteka.close()
    else:
        QtWidgets.QMessageBox.about(dialog, "InsilabGUI Install Warning", "Please choose an install directory!")
        print("Please choose an install directory!")

def run():
    global INSILAB_DIRECTORY, UI_DIRECTORY, versionFile, install_dir
    from pathlib import Path
    #MY_DIR=os.path.dirname(os.path.realpath(sys.argv[0]))
    MY_DIR=os.path.expanduser("~")
    print("My directory:    ", MY_DIR)
    install_dir=os.path.join(MY_DIR, ".insilabgui_installdir.txt")

    try:
        if os.path.isfile(install_dir) == False:
            NoInstallSetting()        
        install_datoteka = open(install_dir, "r")
        INSILAB_DIRECTORY=install_datoteka.readline()
        install_datoteka.close()
        UI_DIRECTORY=os.path.join(INSILAB_DIRECTORY,"UI")
        versionFile = os.path.join(INSILAB_DIRECTORY, "version.txt")
    except:
        if os.path.exists(install_dir):
            os.remove(install_dir)
        QtWidgets.QMessageBox.about(dialog, "InsilabGUI Install Warning", "Something went wrong!\nPlease choose an install directory!")
        print("Something went wrong!\nPlease choose an install directory!")
    
    print("Initialising Insilab GUI ...")

    global running, status
    status = "start"

    try:
        sys.path.remove('')
    except:
        pass

    if UpdateCheck().firstUpgrade():
        print("First time installation...")
        if not internetOn():
            status = "no-internet"
        else:
            UpdateCheck().findLatestVersion()
            UpdateCheck().upgrade()
    else:
        UpdateCheck().findCurrentVersion()
        print("Not First time installation")
        if internetOn() == True:
            UpdateCheck().findLatestVersion()
            if str(latestVersion) != str(currentVersion):
                print("Update Needed!")
                status = "check-update"
    
    running = False
    main()

def main():
    global status
    if status == "no-internet":
        print("There is no internet or the website http://insilab.org is unreachable. Internet should be turned on during installation.")
        QtWidgets.QMessageBox.about(dialog,"Communication Error: No Internet connection available. Please make sure that your device is connected to the Internet.")

    if status == "check-update":
        UpdateCheck().updateBox()

    if status == "start":
        start_common_gui()

    else:
        print ("\nInsilabGUI has encountered a problem at stratup. Please try reinstalling.\If problem persists please contact konc@cmm.ki.si")