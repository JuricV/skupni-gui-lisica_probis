# -*- coding: utf-8 -*-
"""
@author: Vito Jurić
"""

from __future__ import absolute_import
from __future__ import print_function
import sys, os
from pymol.Qt import QtWidgets, QtGui
from pymol.Qt.utils import loadUi
import platform

MY_DIRECTORY=os.path.expanduser('~')
install_dir=os.path.join(MY_DIRECTORY, ".insilabgui_installdir.txt")
install_datoteka = open(install_dir, "r")
INSILAB_DIRECTORY=install_datoteka.readline()
install_datoteka.close()

UI_DIRECTORY=os.path.join(INSILAB_DIRECTORY,"UI")
MODULES_DIRECTORY=os.path.join(INSILAB_DIRECTORY,"modules")
versionFile = os.path.join(INSILAB_DIRECTORY, "version.txt")

def main():
    print(("Python version: ", platform.architecture()[0]))
    print("Settings file for instalation directory location: ", install_dir)
    sys.path.append(os.path.normpath(MODULES_DIRECTORY))
    GUI().run()

class GUI:
    def run(self):
        """Create and show window"""
        global InsiGUI
    
        #if main is None:
        InsiGUI = self.make_dialog()
    
        InsiGUI.show()
    
    def make_dialog(self):    
        # create a new Window
        global InsiGUI
        InsiGUI = QtWidgets.QMainWindow()
    
        # populate the Window from our *.ui file
        uifile = os.path.join(UI_DIRECTORY, 'CommonGUI.ui')
        self.form = loadUi(uifile, InsiGUI)

        # hook up functions to buttons
        self.form.LisicaButton.clicked.connect(start_lisica_gui)
        self.form.H2oButton.clicked.connect(start_probis_h2o)
        self.form.MDButton.clicked.connect(start_probis_h2o_md)
        self.form.ProbisButton.clicked.connect(start_probis)
        self.form.ProbisDockButton.clicked.connect(start_probis_dock)
        
        return InsiGUI
    
def start_lisica_gui():    
    import lisica
    HOME_DIRECTORY=os.path.expanduser('~')
    LISICA_DIRECTORY=os.path.join(HOME_DIRECTORY,".lisicagui")
    UI_DIRECTORY=os.path.join(LISICA_DIRECTORY,"UI")
    versionFile = os.path.join(LISICA_DIRECTORY, "version.txt")
    lisica.run()
    
def start_probis_h2o():
    import ProBiS_H2O
    ProBiS_H2O.run()
    
def start_probis_h2o_md():
    import ProBiS_H2O_MD
    ProBiS_H2O_MD.run()
    
def start_probis():
    import probis_install
    probis_install.run()

def start_probis_dock():
    import ProBiS_Dock
    ProBiS_Dock.run()