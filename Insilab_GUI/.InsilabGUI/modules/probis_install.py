'''
Described at PyMOL wiki: http://www.pymolwiki.org/index.php/probislite
 
'''
from __future__ import absolute_import
from __future__ import print_function
import os
import stat
import urllib.request, urllib.error, urllib.parse
from urllib.request import urlopen
from urllib.error import URLError, HTTPError
import zipfile
import tempfile
import shutil
import sys
import platform
from distutils.dir_util import copy_tree
import time
from pathlib import Path
from threading import Thread
import contextlib
from pymol import cmd
from pymol.Qt import QtWidgets, QtCore
from pymol.Qt.utils import loadUi

def __init_plugin__(app=None):
    '''
    Add an entry to the PyMOL "Plugin" menu
    '''
    from pymol.plugins import addmenuitemqt
    addmenuitemqt("ProBiS", lambda: run())

dialog = None
class Configuration:
    def __init__(self):
        
        self.system=platform.system()
        self.machine=platform.machine()
        self.architecture=platform.architecture()
        import struct
        self.python_bit=8 * struct.calcsize("P")
        self.python_version=platform.python_version_tuple()

    def is_os_64bit(self):
        if self.system == 'Windows':
            return platform.architecture()[0] == "64bit"
        else:
            return platform.machine().endswith('64')
    
    
class UpdateCheck:

    def __init__(self):
        self.firstVersionURL="http://insilab.org/files/probis-plugin/version.txt"
        self.firstArchiveURL="http://insilab.org/files/probis-plugin/archive.zip"
        self.currentVersion=""
        self.latestVersion = ""


    def deleteTmpDir(self):
        try:
            shutil.rmtree(self.tmpDir)
            print("deleted temporary ", self.tmpDir)
        except:
            print("error : could not remove temporary directory")


    def createTmpDir(self):
        try:
            self.tmpDir= tempfile.mkdtemp()
            print("created temporary ", self.tmpDir)
        except:
            print("error : could not create temporary directory")
            self.installationFailed()
        self.zipFileName=os.path.join(self.tmpDir, "archive.zip")


    def downloadInstall(self):

        try:
            print("Fetching plugin from server")
            urlcontent=urlopen(self.firstArchiveURL, timeout=7)
            zipcontent=urlcontent.read()
            probisZipFile=open(self.zipFileName,'wb')
            probisZipFile.write(zipcontent)
        except:
            print("Error: download failed, check if http://insilab.org website is up...")
            self.installationFailed()
        finally:
            try:
                probisZipFile.close()
            except:
                pass
            try:
                urlcontent.close()
            except:
                pass

    def extractInstall(self):
        #this must be called before import Plugin_GUI, otherwise
        #rmtree will fail on NFS systems due to open log file handle
        try:
            with contextlib.closing(zipfile.ZipFile(self.zipFileName,"r")) as probisZip:
                for member in probisZip.namelist():
                    masterDir = os.path.dirname(member)
                    break
                probisZip.extractall(self.tmpDir)
            copy_tree(os.path.join(self.tmpDir, masterDir, ".probis"), PROBIS_DIRECTORY)
        except:
            print("installation of probis failed")
            self.installationFailed()

    def firstUpgrade(self):
        return not os.path.isdir(PROBIS_DIRECTORY)

    def installationFailed(self):
        try:
            shutil.rmtree(PROBIS_DIRECTORY)
        except:
            pass
        global running, status
        status = "failed"
        running = False


    def upgrade(self):

        self.createTmpDir()
        self.downloadInstall()
        self.extractInstall()
        self.deleteTmpDir()
        
        sys.path.append(os.path.normpath(os.path.join(PROBIS_DIRECTORY, "module")))
        self.findLatestVersion()
        
        self.writeToVersionTxt(self.latestVersion)

        print("Upgrade finished successfully!")
        global running, status
        #status = "start"
        running = False
        #start_probis()

    #
    # read current version of probis from version.txt file
    #
    def findCurrentVersion(self):
        if os.path.isfile(versionFile):
            with open(versionFile, 'r') as myFile:
                global currentVersion
                lines = myFile.readlines()
                self.currentVersion = lines[0].strip()
                currentVersion = self.currentVersion

    def findLatestVersion(self):
        global latestVersion
        try:
            versionFile = urllib.request.urlopen(self.firstVersionURL, timeout=5)
            self.latestVersion = versionFile.read().decode(encoding="utf-8").strip()
            if len(self.latestVersion) > 6:
                self.latestVersion = ""
            latestVersion = self.latestVersion
            
            print("ProBiS version: ", self.latestVersion)
        except HTTPError as e1:
            print("HTTP Error:", e1.code, e1.reason)
        except URLError as e2:
            print("URL Error:", e2.reason)
        except Exception as e:
            print("Error: Gitlab server down?")

    def writeToVersionTxt(version, reminder=False):
        if reminder:
            with open(versionFile, 'a') as myfile:
                myfile.write(str(version)+"\t later \t"+time.strftime("%d/%m/%Y")+"\n")
        else:
            with open(versionFile, 'w') as myfile:
                myfile.write(str(version)+"\n".encode("ascii"))

    def updateBox(self):
        global updateDialog

        updateDialog = self.updateWindow()

        updateDialog.show()
    
    def updateWindow(self):
        global updateDialog
        updateDialog = QtWidgets.QMainWindow()

        uifile = os.path.join(UI_DIRECTORY, 'ProbisUpdate.ui')
        form = loadUi(uifile, updateDialog)

        def cancelButton():
            global status
            if form.checkBoxAsk.isChecked():
                self.writeToVersionTxt(self.latestVersion, True)
            #status = "start"
            updateDialog.close()
            start_probis()
            

        def updateButton(vent=None):
            global status
            #status = "update"
            #run_dialog("ProBis Updating...")
            #updateDialog.close()
            self.upgrade()
            updateDialog.close()
            start_probis()
            
        self.findLatestVersion()
        self.findCurrentVersion()
    
    # hook up button callbacks
        form.labelVersionLatest.setText(str(self.latestVersion))
        form.labelVersionCurrent.setText(str(self.currentVersion))
        form.buttonBoxUpdate.accepted.connect(lambda: updateButton())
        form.buttonBoxUpdate.rejected.connect(lambda: cancelButton())
    
        return updateDialog

def internetOn():
        try:
            urllib.request.urlopen('http://insilab.org',timeout=5)
            return True
        except:
            pass
        return False
    
class FuncThread(Thread):
    def __init__(self, target, *args):
        Thread.__init__(self)
        self._target = target
        self._args = args

    def run(self, *args):
        self._target(*self._args)

def showManualDlInfo():
    msgBox = QtWidgets.QMessageBox()
    msgBox.setIcon(QtWidgets.QMessageBox.Information)
    msgBox.setText("Installation failed: To download and manually install ProBiS Plugin please visit our website http://www.insilab.org/probis-plugin .")
    msgBox.setWindowTitle("Instalation error message")
    msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)

def start_probis():
    print(("Python version: "+platform.architecture()[0]))
    sys.path.append(os.path.normpath(os.path.join(PROBIS_DIRECTORY, "pymol")))
    import probis_pymol
    #dialog.close()
    probis_pymol.main()


class CustomDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle("Choose Directory")
    def find_dir(self):
        folderpath = str(QtWidgets.QFileDialog.getExistingDirectory(CustomDialog(), "Select Directory"))
        if folderpath.endswith(".probis") or folderpath.endswith(".probis"+os.path.sep):
            pass
        else:
            folderpath=os.path.join(folderpath, ".probis")
        self.lineEdit.setText(folderpath)
    def set_dir(self):
        global SetDir
        SetDir = self.lineEdit.text()   
        install_datoteka = open(install_dir, "w")
        install_datoteka.write(SetDir)
        install_datoteka.close()
        InstallDialog.close()
            
    def setupUI(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(640, 148)
        Dialog.setMaximumSize(QtCore.QSize(16777215, 210))
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 2)
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 1, 0, 1, 1)
        HOME_DIRECTORY=os.path.expanduser('~')
        DEFAULT_DIRECTORY=os.path.join(HOME_DIRECTORY, ".probis")
        self.lineEdit.setText(DEFAULT_DIRECTORY)
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 1, 1, 1, 1)
        self.pushButton.clicked.connect(self.find_dir)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.pushOK = QtWidgets.QPushButton(Dialog)
        self.pushOK.setObjectName("pushOK")
        self.gridLayout.addWidget(self.pushOK, 3, 1, 1, 1)
        self.pushOK.clicked.connect(self.set_dir)
        

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        
    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("Dialog", "Please choose install directory for ProBiS:"))
        self.pushButton.setText(_translate("Dialog", "Pick directory"))
        self.label_2.setText(_translate("Dialog", "*If ProBiS has been manually installed please input .probis file."))
        self.pushOK.setText(_translate("Dialog", "OK"))

def run_custom_dialog():
    global InstallDialog
    InstallDialog=QtWidgets.QDialog()
    InstallDialog.ui = CustomDialog()
    InstallDialog.ui.setupUI(InstallDialog)
    InstallDialog.show()
    InstallDialog.exec_()

def NoInstallSetting():
    global SetDir
    run_custom_dialog()
    if not SetDir=="":
        install_datoteka = open(install_dir, "w")
        install_datoteka.write(SetDir)
        install_datoteka.close()
    else:
        QtWidgets.QMessageBox.about(dialog, "ProBiS Install Warning", "Please choose an install directory!")
        print("Please choose an install directory!")
    

def run():
    global PROBIS_DIRECTORY, UI_DIRECTORY, versionFile, install_dir
    MY_DIR=os.path.dirname(os.path.realpath(sys.argv[0]))
    install_dir=os.path.join(Path.home(), ".probis_installdir.txt")
    
    if os.path.isfile(install_dir) == True:
        install_datoteka = open(install_dir, "r")
        if os.path.isdir(install_datoteka.readline()):
            install_datoteka.close()
        else:
            install_datoteka.close()
            os.remove(install_dir)
        
    try:
        if os.path.isfile(install_dir) == False:
            NoInstallSetting()        
        install_datoteka = open(install_dir, "r")
        PROBIS_DIRECTORY=install_datoteka.readline()
        print("TEST ProBiS directory:     ", PROBIS_DIRECTORY)
        install_datoteka.close()
        UI_DIRECTORY=os.path.join(PROBIS_DIRECTORY,"UI")
        versionFile = os.path.join(PROBIS_DIRECTORY, "version.txt")
    except:
        if os.path.exists(install_dir):
            os.remove(install_dir)
        QtWidgets.QMessageBox.about(dialog, "ProBiS Install Warning", "Something went wrong!\nPlease choose an install directory!")
        print("Something went wrong!\nPlease choose an install directory!")
    
    
    print("Initialising ProBiS ...")

    global running, status
    status = "start"

    try:
        sys.path.remove('')
    except:
        pass

    if UpdateCheck().firstUpgrade():
        print("First time installation...")
        if not internetOn():
            status = "no-internet"
        else:
            UpdateCheck().findLatestVersion()
            UpdateCheck().upgrade()
    else:
        UpdateCheck().findCurrentVersion()
        print("Not First time installation")
        if internetOn() == True:
            UpdateCheck().findLatestVersion()
            #print("internet on")
            if str(latestVersion) != str(currentVersion):
                print("Update Needed!")
                status = "check-update"
    
    running = False
    main()


def main():
    global status
    #run_dialog("Starting ProBiS software...")
    if status == "no-internet":
        msgBox = QtWidgets.QMessageBox()
        msgBox.setIcon(QtWidgets.QMessageBox.Information)
        msgBox.setText("Communication Error: No Internet connection available. Please make sure that your device is connected to the Internet.")
        msgBox.setWindowTitle("Communication error message")
        msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)

    if status == "check-update":
        #dialog.close()
        UpdateCheck().updateBox()

    if status == "start":
        start_probis()

    else:
        showManualDlInfo()

