# -*- coding: utf-8 -*-
"""
@author: Vito Jurić
"""

from __future__ import absolute_import
from __future__ import print_function
import os
import urllib.request, urllib.error, urllib.parse
from urllib.request import urlopen
from urllib.error import URLError, HTTPError
import zipfile
import tempfile
import shutil
import sys
import platform
from distutils.dir_util import copy_tree
import time
from pathlib import Path
import contextlib
from pymol.Qt import QtWidgets, QtCore
from pymol.Qt.utils import loadUi

def __init_plugin__(app=None):
    '''
    Add an entry to the PyMOL "Plugin" menu
    '''
    from pymol.plugins import addmenuitemqt
    addmenuitemqt("ProBiS Dock", lambda: run())

dialog = None

class Configuration:
    def __init__(self):
        
        self.system=platform.system()
        self.machine=platform.machine()
        self.architecture=platform.architecture()
        import struct
        self.python_bit=8 * struct.calcsize("P")
        self.python_version=platform.python_version_tuple()

    def is_os_64bit(self):
        if self.system == 'Windows':
            return platform.architecture()[0] == "64bit"
        else:
            return platform.machine().endswith('64')

class UpdateCheck:

    def __init__(self):
        """Initialize url variables"""
        #Ne pozabi spremenit predno se prestavi na insilab
        # self.firstVersionURL="http://insilab.org/files/probis-dock/version.txt"
        # self.firstArchiveURL="http://insilab.org/files/probis-dock/archive.zip"
        self.firstVersionURL="https://gitlab.com/JuricV/skupni-gui-lisica_probis/-/raw/master/ProBiS_Dock/.probisDock/version.txt"
        self.firstArchiveURL="https://gitlab.com/JuricV/skupni-gui-lisica_probis/-/archive/master/skupni-gui-lisica_probis-master.zip?path=ProBiS_Dock/.probisDock"
        self.currentVersion=""
        self.latestVersion = ""


    def deleteTmpDir(self):
        """Delete temp"""
        try:
            shutil.rmtree(self.tmpDir)
            print("deleted temporary ", self.tmpDir)
        except:
            print("error : could not remove temporary directory")


    def createTmpDir(self):
        """Create temp"""
        try:
            self.tmpDir= tempfile.mkdtemp()
            print("created temporary ", self.tmpDir)
        except:
            print("error : could not create temporary directory")
            self.installationFailed()
        self.zipFileName=os.path.join(self.tmpDir, "archive.zip")


    def downloadInstall(self):
        """Connect to archive URL and download plugin zip contetnts to temp"""
        try:
            print("Fetching plugin from server")
            urlcontent=urlopen(self.firstArchiveURL, timeout=7)
            zipcontent=urlcontent.read()
            probisZipFile=open(self.zipFileName,'wb')
            probisZipFile.write(zipcontent)
        except:
            print("Error: download failed, check if http://insilab.org website is up...")
            self.installationFailed()
        finally:
            try:
                probisZipFile.close()
            except:
                pass
            try:
                urlcontent.close()
            except:
                pass

    def extractInstall(self):
        """Extract plugin contents from temp"""
        # this must be called before import Plugin_GUI, otherwise
        # rmtree will fail on NFS systems due to open log file handle
        try:
            with contextlib.closing(zipfile.ZipFile(self.zipFileName,"r")) as probisZip:
                for member in probisZip.namelist():
                    masterDir = os.path.dirname(member)
                    break
                probisZip.extractall(self.tmpDir)
            copy_tree(os.path.join(self.tmpDir, masterDir, "ProBiS_Dock", ".probisDock"), PROBIS_DOCK_DIRECTORY)
        except:
            print("installation of ProBiS Dock failed")
            self.installationFailed()

    def firstUpgrade(self):
        """Check if plugin location file is present. If not return infot that it needs instalation"""
        return not os.path.isdir(PROBIS_DOCK_DIRECTORY)

    def installationFailed(self):
        """Close if installation is not succesful. May also occur if starting sequence breaks"""
        try:
            shutil.rmtree(PROBIS_DOCK_DIRECTORY)
        except:
            pass
        global running, status
        status = "failed"
        running = False


    def upgrade(self):
        """Find new version on internet and install/replace current version"""
        self.createTmpDir()
        self.downloadInstall()
        self.extractInstall()
        self.deleteTmpDir()
        
        # sys.path.append(os.path.normpath(os.path.join(PROBIS_DOCK_DIRECTORY, "module")))
        sys.path.append(os.path.normpath(os.path.join(PROBIS_DOCK_DIRECTORY, "pymol")))
        self.findLatestVersion()
        
        self.writeToVersionTxt(self.latestVersion)

        print("Upgrade finished successfully!")
        global running, status
        status = "start"
        running = False

    #
    # read current version of probis from version.txt file
    #
    def findCurrentVersion(self):
        """Read version.txt file for current version"""
        if os.path.isfile(versionFile):
            with open(versionFile, 'r') as myFile:
                global currentVersion
                lines = myFile.readlines()
                self.currentVersion = lines[0].strip()
                currentVersion = self.currentVersion

    def findLatestVersion(self):
        """Read version file on internet for latest version"""
        global latestVersion
        try:
            versionFile = urllib.request.urlopen(self.firstVersionURL, timeout=5)
            self.latestVersion = versionFile.read().decode(encoding="utf-8").strip()
            if len(self.latestVersion) > 6:
                self.latestVersion = ""
            latestVersion = self.latestVersion
            
        except HTTPError as e1:
            print("HTTP Error:", e1.code, e1.reason)
            """Placeholder for testing"""
            self.latestVersion = "Not found"
            latestVersion = "Not found"
        except URLError as e2:
            print("URL Error:", e2.reason)
            """Placeholder for testing"""
            self.latestVersion = "Not found"
            latestVersion = "Not found"
        except Exception as e:
            print("Error: ", e)
            print("Error: Gitlab server down?")
            """Placeholder for testing"""
            self.latestVersion = "Not found"
            latestVersion = "Not found"

    def writeToVersionTxt(version, reminder=False):
        """Write version to version.txt or reminder to check version later"""
        if os.path.isfile(versionFile):
            if reminder:
                with open(versionFile, 'a') as myfile:
                    myfile.write(str(version)+"\t later \t"+time.strftime("%d/%m/%Y")+"\n")
            else:
                with open(versionFile, 'w') as myfile:
                    myfile.write(str(version)+"\n".encode("ascii"))
        else:
            pass
        
    def updateBox(self):
        """Open window prompting user if he wants to update plugin"""
        global updateDialog

        updateDialog = self.updateWindow()

        updateDialog.show()

        if self.currentVersion == "UPDATE NOW":
            global status
            self.upgrade()
            updateDialog.close()
            start_probis_dock()
    
    def updateWindow(self):
        """Create and populate update window"""
        global updateDialog
        updateDialog = QtWidgets.QMainWindow()

        uifile = os.path.join(UI_DIRECTORY, 'ProbisDockUpdate.ui')
        form = loadUi(uifile, updateDialog)

        def cancelButton():
            """Function for canceling update"""
            global status
            if form.checkBoxAsk.isChecked():
                self.writeToVersionTxt(self.latestVersion, True)
            status = "start"
            updateDialog.close()
            start_probis_dock()
            

        def updateButton(vent=None):
            """Function for triggering update"""
            global status
            self.upgrade()
            updateDialog.close()
            start_probis_dock()
            
        self.findLatestVersion()
        self.findCurrentVersion()
    
    # hook up button callbacks
        form.labelVersionLatest.setText(str(self.latestVersion))
        form.labelVersionCurrent.setText(str(self.currentVersion))
        form.buttonBoxUpdate.accepted.connect(lambda: updateButton())
        form.buttonBoxUpdate.rejected.connect(lambda: cancelButton())
    
        return updateDialog
    

def internetOn():
    """Check internet connection"""
    try:
        urllib.request.urlopen('http://insilab.org',timeout=5)
        return True
    except:
        pass
    return False
    
def showManualDlInfo():
    """Error message"""
    QtWidgets.QMessageBox.about(None, "ProBiS Dock Installation Warning", "Installation failed: To download and manually install ProBiS Dock Plugin please visit our website http://www.insilab.org/probis-dock .")


def start_probis_dock():
    """Start central plugin script"""
    print(("Python version: "+platform.architecture()[0]))
    sys.path.append(os.path.normpath(os.path.join(PROBIS_DOCK_DIRECTORY, "pymol")))
    import ProBiS_Dock_module
    ProBiS_Dock_module.run()


class CustomDialog(QtWidgets.QDialog):
    """Select installation directory window"""
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle("Choose Directory")
        
    def find_dir(self):
        """Open Directory search dialog"""
        folderpath = str(QtWidgets.QFileDialog.getExistingDirectory(CustomDialog(), "Select Directory"))
        if folderpath.endswith(".probisDock") or folderpath.endswith(".probisDock"+os.path.sep):
            pass
        else:
            folderpath=os.path.join(folderpath, ".probisDock")
        self.lineEdit.setText(folderpath)
        
    def set_dir(self):
        """Select directroy in Firectory Line as installation directory and close dialog"""
        global SetDir
        SetDir = self.lineEdit.text()   
        install_datoteka = open(install_dir, "w")
        install_datoteka.write(SetDir)
        install_datoteka.close()
        InstallDialog.close()
            
    def setupUI(self, Dialog):
        """Create, populate and connect functions for installation directory select window"""
        Dialog.setObjectName("Dialog")
        Dialog.resize(640, 148)
        Dialog.setMaximumSize(QtCore.QSize(16777215, 210))
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 2)
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 1, 0, 1, 1)
        HOME_DIRECTORY=os.path.expanduser('~')
        DEFAULT_DIRECTORY=os.path.join(HOME_DIRECTORY, ".probisDock")
        self.lineEdit.setText(DEFAULT_DIRECTORY)
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 1, 1, 1, 1)
        self.pushButton.clicked.connect(self.find_dir)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.pushOK = QtWidgets.QPushButton(Dialog)
        self.pushOK.setObjectName("pushOK")
        self.gridLayout.addWidget(self.pushOK, 3, 1, 1, 1)
        self.pushOK.clicked.connect(self.set_dir)
        

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        
    def retranslateUi(self, Dialog):
        """Set texts in window"""
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("Dialog", "Please choose install directory for ProBiS Dock:"))
        self.pushButton.setText(_translate("Dialog", "Pick directory"))
        self.label_2.setText(_translate("Dialog", "*If ProBiS Dock has been manually installed please input .probisDock file."))
        self.pushOK.setText(_translate("Dialog", "OK"))

def run_custom_dialog():
    """Run select installation directory window if not installed yet"""
    global InstallDialog
    #Define window
    InstallDialog=QtWidgets.QDialog()
    #Connect class to window
    InstallDialog.ui = CustomDialog()
    #Create, populate and connect functions to window
    InstallDialog.ui.setupUI(InstallDialog)
    #Show window to user
    InstallDialog.show()
    InstallDialog.exec_()

def NoInstallSetting():
    """Run if plugin not installed. Opens install select dialog and creates install location file"""
    global SetDir
    run_custom_dialog()
    if not SetDir=="":
        install_datoteka = open(install_dir, "w")
        install_datoteka.write(SetDir)
        install_datoteka.close()
    else:
        QtWidgets.QMessageBox.about(dialog, "ProBiS Dock Install Warning", "Please choose an install directory!")
        print("Please choose an install directory!")

def run():
    """Run function"""
    global PROBIS_DOCK_DIRECTORY, UI_DIRECTORY, versionFile, install_dir
    # MY_DIR=os.path.dirname(os.path.realpath(sys.argv[0]))
    install_dir=os.path.join(Path.home(), ".ProBiS_Dock_installdir.txt")
    
    if os.path.isfile(install_dir) == True:
        install_datoteka = open(install_dir, "r")
        if os.path.isdir(install_datoteka.readline()):
            install_datoteka.close()
        else:
            install_datoteka.close()
            os.remove(install_dir)
        
    try:
        if os.path.isfile(install_dir) == False:
            NoInstallSetting()        
        install_datoteka = open(install_dir, "r")
        PROBIS_DOCK_DIRECTORY=install_datoteka.readline()
        print("ProBiS Dock directory:     ", PROBIS_DOCK_DIRECTORY)
        install_datoteka.close()
        UI_DIRECTORY=os.path.join(PROBIS_DOCK_DIRECTORY,"UI")
        versionFile = os.path.join(PROBIS_DOCK_DIRECTORY, "version.txt")
    except:
        if os.path.exists(install_dir):
            os.remove(install_dir)
        QtWidgets.QMessageBox.about(dialog, "ProBiS Dock Install Warning", "Something went wrong!\nPlease choose an install directory!")
        print("Something went wrong!\nPlease choose an install directory!")
    
    
    print("Initialising ProBiS Dock ...")

    global running, status
    status = "start"

    try:
        sys.path.remove('')
    except:
        pass

    if UpdateCheck().firstUpgrade():
        print("First time installation...")
        if not internetOn():
            status = "no-internet"
        else:
            UpdateCheck().findLatestVersion()
            UpdateCheck().upgrade()
    else:
        UpdateCheck().findCurrentVersion()
        print("Not First time installation")
        if internetOn() == True:
            UpdateCheck().findLatestVersion()
            if str(latestVersion) != str(currentVersion):
                print("Update Needed!")
                status = "check-update"
    
    running = False
    main()


def main():
    """Main function choosing what to open"""
    global status
    if status == "no-internet":
        QtWidgets.QMessageBox.about(None, "ProBiS Dock: Communication error", "Communication Error: No Internet connection available. Please make sure that your device is connected to the Internet.")

    if status == "check-update":
        UpdateCheck().updateBox()

    elif status == "start":
        start_probis_dock()

    else:
        showManualDlInfo()

