# -*- coding: utf-8 -*-
"""
@author: Vito Juric
"""

import os, sys, platform
from pathlib import Path
import multiprocessing
from datetime import datetime
import urllib.request, urllib.error, urllib.parse
from urllib.request import urlopen
from urllib.error import URLError, HTTPError
import gzip
import subprocess
import re
import json
import stat
import random

### pymol imports
from pymol import cmd, CmdException, plugins
from pymol.Qt import QtWidgets, QtGui, QtCore
from pymol.Qt.utils import loadUi

######################### DIRECTORIES ####################################
# Set up directory variables
with open(os.path.join(Path.home(), ".ProBiS_Dock_installdir.txt")) as loc:
    Dock_directory = loc.readline()
print("Running ProBiS Dock plugin in ", Dock_directory)
sys.path.append(Dock_directory)
Setings_directory = os.path.join(Dock_directory, "settings")
Bin_directory = os.path.join(Dock_directory, "bin")
Ui_directory = os.path.join(Dock_directory, "UI")
Results_directory = os.path.join(Dock_directory, "results")
Temp_directory = os.path.join(Results_directory, "temp")

update_messages = ["Please wait, this might take a while.",
                   "Still working...",
                   "Ugh... It's taking a long while, huh? A bit more",
                   "Still hard at work",
                   "Don't worry. I'm still working",
                   "Working... Did you stretch yet?",
                   "Work is going on",
                   "Making sure you get the best results, please hold",
                   "Working...",
                   "Analysis ongoing...",
                   "Don't give up on me, I'm doing my best",
                   "Working...",
                   "Still Working...",
                   "Please wait...",
                   "Analysis ongoing",
                   "Working... Might be time for a coffee"]

# Check for probisdock executable permissions on linux or check bit architecture of windows system
if "linux" in sys.platform:
    proDockDir = os.path.join(Bin_directory, "probisdock")
    if os.access(proDockDir, os.X_OK) == True:
        print("Probis Dock executable ok")
    else:
        st = os.stat(proDockDir)
        os.chmod(proDockDir, st.st_mode | stat.S_IEXEC)
        print("Probis Dock executable permission set")
        print("Probis Dock directory:      : ", proDockDir)

# No windows version of probisdock executable exists yet
elif "win" in sys.platform:
    if platform.architecture()[0] == "64bit":
        proDockDir = os.path.join(Bin_directory, "64", "probisdock.exe")
    else:
        proDockDir = os.path.join(Bin_directory, "32","probisdock.exe")
    print("ProBiS Dock Pluigin is not avaliable on windows systems")
    QtWidgets.QMessageBox.about(None, "ProBiS Dock error!", "ProBiS Dock Pluigin is not avaliable on windows systems.\n Please use a linux system")
else:
    print("ProBiS Dock Pluigin is not avaliable on your system")
    QtWidgets.QMessageBox.about(None, "ProBiS Dock error!", "ProBiS Dock Pluigin is avaliable only on linux systems")


class settings():
    """Analysis chosen settings"""
    
    def default_dirs(self):
        """Set default directories and files values on GUI"""
        dialog.LineReceptor.setText("")
        dialog.LineTemplate.setText("")
        dialog.LineCentro.setText("")
        dialog.LineLigands.setText("")
        dialog.LineResults.setText(Results_directory)
        
        pent("Set default results directory")
        
    def default_sett(self):
        """Set default advanced values in GUI"""
        dialog.SpinTrimDistance.setValue(5.00)
        dialog.SpinProbisScoring.setValue(0.90)
        dialog.SpinMaxSequence.setValue(100) ############################################################################
        dialog.SpinCentroRadius.setValue(3.00)
        dialog.SpinMaxFragmentRadius.setValue(16.0)
        dialog.ComboReferenceState.setCurrentIndex(0) #mean, cumulative
        dialog.ComboAtomTypes.setCurrentIndex(0) #reduced, complete
        dialog.ComboRadOrNorm.setCurrentIndex(0) #radial, normalized_frequency
        dialog.SpinScoringCutoff.setValue(6.00)
        dialog.SpinKBStep.setValue(0.01)
        dialog.SpinKBScale.setValue(10.0)
        dialog.SpinMaxLigands.setValue(10)
        dialog.SpinMaxTempLigands.setValue(10)
        dialog.ComboLigandSpin.setCurrentIndex(6) #0, 5, 10, 15, 20, 30, 60, 90
        dialog.SpinClash.setValue(0.75)
        dialog.SpinTolerance.setValue(2.0)
        dialog.SpinLowTolerance.setValue(2.0)
        dialog.SpinUpperTolerance.setValue(2.0)
        dialog.SpinMaxConf.setValue(100)
        dialog.SpinMaxIter.setValue(1000)
        dialog.SpinRadiusConf.setValue(2.0)
        dialog.SpinMaxEnergy.setValue(0.0)
        dialog.CheckIterative.setChecked(False)
        dialog.SpinMaxCliques.setValue(1000)
        dialog.SpinMCQDW.setValue(10000000)
        dialog.ComboForcefield.setCurrentIndex(0) #kb, phy
        dialog.SpinMinTolerance.setValue(0.0001)
        dialog.SpinMaxLinkIter.setValue(100)
        dialog.SpinMaxFinIter.setValue(100)
        dialog.SpinUpdateSteps.setValue(200)
        dialog.SpinPosTolerance.setValue(1.0)
        dialog.SpinForceCon.setValue(10000.0)
        dialog.SpinFlexRes.setValue(8.0)
        dialog.SpinTopSeed.setValue(0.15)
        dialog.SpinClique.setValue(1000)
        dialog.SpinGrid.setValue(0.375)
        dialog.SpinUnitVector.setValue(256)
        dialog.SpinConfGenSpin.setValue(10.0)
        dialog.SpinExclusionRad.setValue(0.8)
        dialog.SpinMaxInteratomic.setValue(8.0)
        dialog.SpinProbisCutoff.setValue(1.0)
        dialog.SpinProbisDecay.setValue(1.0)
        dialog.SpinRadiusRigid.setValue(2.0)
        dialog.SpinMaxQueue.setValue(1)
        dialog.SpinMaxCache.setValue(1000)
        dialog.SpinNCPU.setValue(int(multiprocessing.cpu_count()))  
        
        pent("Set default advanced analysis settings")
    
    
class GUI(QtWidgets.QWidget):
    """Class for GUI display"""
    def __init__(self):
        """Initilize GUI"""
        super().__init__()
        cmd.set("auto_zoom", "off")
        self.run()
    
    def run(self):
        """Create dialog (window) and show it"""
        global dialog
        dialog = self.make_dialog()
        dialog.show()


    def make_dialog(self):
        """Build window"""
        global dialog
        # create a new Window
        dialog = QtWidgets.QMainWindow()
        # populate the Window
        uifile = os.path.join(Ui_directory, 'ProbisDockMain.ui')
        self.form = loadUi(uifile, dialog)
        
        
        """Connect buttons to functions"""
        self.form.PushDefault.pressed.connect(lambda: settings().default_sett())
        self.form.ComboMulti.currentIndexChanged.connect(self.single_or_multi)
        self.form.PushReceptor.clicked.connect(lambda: self.search_dir("receptor"))
        self.form.PushTemplate.clicked.connect(lambda: self.search_dir("template"))
        self.form.PushCentro.clicked.connect(lambda: self.search_dir("centro"))
        self.form.PushResults.clicked.connect(lambda: self.search_dir("results"))
        self.form.PushLoadSearch.clicked.connect(lambda: self.search_dir("load"))
        self.form.PushSaveAnSearch.clicked.connect(lambda: self.search_dir("save"))
        self.form.PushLoad.clicked.connect(self.loadAnalysis)
        self.form.PushSaveAn.clicked.connect(lambda: self.saveAnalysis(results))
        self.form.PushLigands.clicked.connect(lambda: self.search_dir("ligands"))
        self.form.PushStart.clicked.connect(self.start)
        self.form.PushFilterProteins.clicked.connect(lambda: self.fill_results_tree("proteins", results))
        self.form.PushFilterLigands.clicked.connect(lambda: self.fill_results_tree("ligands", results))
        self.form.PushAll.clicked.connect(lambda: self.fill_results_tree("all", results))
        self.form.PushDisplay.clicked.connect(lambda: self.display_results(results))
        self.form.PushSave.clicked.connect(lambda: self.open_save_window(results))
        self.form.pushUpgradeTo.clicked.connect(self.updateProgramVersion)

        self.form.Tabs.currentChanged.connect(self.if_res)
        self.form.TreeResults.itemSelectionChanged.connect(self.selection_changed)
        self.form.labelVersion.setText(str(current_version))
        
        """Set current and latest version in about tab for updating"""
        if current_version == latest_version:
            self.form.labelUpgradeTo.setText("Upgrade of ProBiS Dock plugin not avaliable")
            self.form.pushUpgradeTo.setText("Not avaliable")
            self.form.pushUpgradeTo.setEnabled(False)
        else:
            self.form.labelUpgradeTo.setText("Upgrade of ProBiS Dock plugin avaliable:")
            self.form.pushUpgradeTo.setText("Update to "+str(latest_version))
            self.form.pushUpgradeTo.setEnabled(True)
        
        """Set Logo"""
        self.form.setWindowIcon(QtGui.QIcon(os.path.join(Ui_directory, "Logo", "Dock_Logo.png")))
        self.form.LabelLogo.setPixmap(QtGui.QPixmap(os.path.join(Ui_directory, "Logo", "Dock_Logo.png")))
        
        """Set default setting in gui"""
        self.form.SpinNCPU.setMaximum(multiprocessing.cpu_count())
        sett = settings()
        sett.default_sett()
        sett.default_dirs()
        self.single_or_multi()
        
        return dialog

    def start(self):
        """Start analysis"""
        dialog.ListProteins.clear()
        dialog.ListLigands.clear()
        dialog.TreeResults.clear()
        
        self.open_working_window()
        working.BarWorking.setMinimum(0)
        working.BarWorking.setMaximum(0)
        working.BarWorking.setValue(0)
        update_win()
        
        dock = docking()
        dock.start_analysis()
        self.fill_results_tree("all", results)
        
        try:
            working.close()
        except:
            pass
        
        pent("Analysis finished!")

    def selection_changed(self):
        """If there are no selected items in results table make Display and Save disabled otherwise enabled"""
        if dialog.TreeResults.selectedItems == []:
            dialog.PushDisplay.setEnabled(False)
            dialog.PushSave.setEnabled(False)
        else:
            dialog.PushDisplay.setEnabled(True)
            dialog.PushSave.setEnabled(True)

    def if_res(self):
        """Disable or enable multiple buttons if there are any results or no"""
        if results == {}:
            dialog.LineSaveAn.setEnabled(False)
            dialog.PushSaveAn.setEnabled(False)
            dialog.PushSaveAnSearch.setEnabled(False)
            dialog.PushFilterProteins.setEnabled(False)
            dialog.ListProteins.setEnabled(False)
            dialog.PushFilterLigands.setEnabled(False)
            dialog.ListLigands.setEnabled(False)
            dialog.PushAll.setEnabled(False)
            dialog.PushDisplay.setEnabled(False)
            dialog.PushSave.setEnabled(False)
            dialog.TreeResults.setEnabled(False)
        else:
            dialog.LineSaveAn.setEnabled(True)
            dialog.PushSaveAn.setEnabled(True)
            dialog.PushSaveAnSearch.setEnabled(True)
            dialog.PushFilterProteins.setEnabled(True)
            dialog.ListProteins.setEnabled(True)
            dialog.PushFilterLigands.setEnabled(True)
            dialog.ListLigands.setEnabled(True)
            dialog.PushAll.setEnabled(True)
            #dialog.PushDisplay.setEnabled(True)
            #dialog.PushSave.setEnabled(True)
            dialog.TreeResults.setEnabled(True)
    
    def open_save_window(self, res):
        """Open prompt for saving complexes in pymols native format"""
        global saveWin
        saveWin = QtWidgets.QDialog()
        saveuifile = os.path.join(Ui_directory, "ProbisDockSave.ui")
        self.saveWin = loadUi(saveuifile, saveWin)

        """Connect functions to buttons"""
        self.saveWin.PushSaveSearch.clicked.connect(lambda: self.search_dir("savePymol"))
        self.saveWin.Box.accepted.connect(lambda: self.exportMoleculesFromPymol(res))
        self.saveWin.Box.rejected.connect(saveWin.close)
    
        self.saveWin.show()
        
    def open_working_window(self):
        global working
        working = QtWidgets.QDialog()
        workingfile = os.path.join(Ui_directory, "ProbisDockWorking.ui")
        self.work = loadUi(workingfile, working)

        working.show()

    def exportMoleculesFromPymol(self, res):
        """Run complex exporting in pymols native format"""
        pent("Exporting molecules")
        exportFile = saveWin.LineSave.text()
        if exportFile == "":
            QtWidgets.QMessageBox.about(dialog, "Error Export", "Please enter save file.")
        else:
            pent("Molecules will also be displayed on PyMOL console")
            self.display_results(res)
            exportObjects = []
            for item in dialog.TreeResults.selectedItems():
                if not item.text(1) in exportObjects:
                    exportObjects.append(item.text(1))
                ligand_name = item.text(2).strip()+"-_-"+item.text(3).strip()+"-_-"+item.text(1).strip()
                if not ligand_name in exportObjects:
                    exportObjects.append(ligand_name)
            exportLine = " or ".join(exportObjects)
            cmd.multisave(exportFile, exportLine)

            pent("Saved chosen molecules succesfully in file: "+exportFile)
            QtWidgets.QMessageBox.about(dialog, "Save succesful", "Chosen molecules have been saved sucesully in "+exportFile)

    def single_or_multi(self):
        """Changed between multiple receptors or single receptor"""
        if self.form.ComboMulti.currentIndex() == 0:
            self.form.LineCentro.setEnabled(True)
            self.form.PushCentro.setEnabled(True)
            self.form.LineTemplate.setEnabled(True)
            self.form.PushTemplate.setEnabled(True)
            self.form.LabelReceptor.setText("Receptor File")
            self.form.LabelReceptor.setToolTip(r"The protein to which small molecules will be docked ({.pdb|.cif|.json}[.gz])")
            self.form.LabelCentro.setToolTip(r"Binding site centroids (.cen|.json[.gz])")
            self.form.LabelTemplate.setToolTip(r"The template ligands for ProBiS-Score - provide when h > 0.0 (.pdb[.gz]|.mol2[.gz]|.json[.gz])")  
            
            pass
        elif self.form.ComboMulti.currentIndex() == 1:
            self.form.LineCentro.setEnabled(False)
            self.form.PushCentro.setEnabled(False)
            self.form.LineTemplate.setEnabled(False)
            self.form.PushTemplate.setEnabled(False)
            self.form.LabelReceptor.setText("ProBiS DB File")
            self.form.LabelReceptor.setToolTip(r"A directory that contains a binding sites dataset (receptors, template ligands, centroids) obtained from the ProBiS-Dock Database (http://probis-dock-database.insilab.org). File names must follow the standard (this is default in ProBiS-Dock Database):   * receptor (protein): receptor_PDBID_CHAINID_*.pdb.gz  * template ligands: predicted_ligands_PDBID_CHAINID_*.pdb.gz  * centroids: probisdock_centro_PDBID_CHAINID_*.cen.gz")
            self.form.LabelCentro.setToolTip(r"Not used with theese settings")
            self.form.LabelTemplate.setToolTip(r"Not used with these settings")  

    def search_dir(self, tip):
        """Different file/directory search and input functions. tip = receptor, centro, template, results, load, save, savePymol""" 
        if tip == "receptor":
            if self.form.ComboMulti.currentIndex() == 0:
                filename, _filter = QtWidgets.QFileDialog.getOpenFileName(dialog, "Open Receptor file...", filter="Pdb, Cif, Json or Gz File (*.pdb *.cif *.json *.gz")
                dialog.LineReceptor.setText(filename)
            else:
                dirname = QtWidgets.QFileDialog.getExistingDirectory(dialog, "Choose database directory...")
                dialog.LineReceptor.setText(dirname)
        elif tip == "ligands":
            filename, _filter = QtWidgets.QFileDialog.getOpenFileName(dialog,
                                                                      "Open Ligands file...", filter="Pdb, Mol2, Json, or Gz File (*.pdb *.mol2 *.json *.gz)")
            dialog.LineLigands.setText(filename)
        elif tip == "centro":
            filename, _filter = QtWidgets.QFileDialog.getOpenFileName(dialog, "Open Centro file...", filter="Cen, Json or Gz File (*.cen, *.json, *.gz)")
            dialog.LineCentro.setText(filename)
        elif tip == "template":
            filename, _filter = QtWidgets.QFileDialog.getOpenFileName(dialog, "Open Template file...", filter="Pdb, Mol2 Json or Gz File (*.pdb *.mol2 *.json *.gz)")
            dialog.LineTemplate.setText(filename)
        elif tip == "results":
            dirname = QtWidgets.QFileDialog.getExistingDirectory(dialog, "Choose database directory...")
            dialog.LineResults.setText(dirname)
        elif tip == "load":
            filename, _filter = QtWidgets.QFileDialog.getOpenFileName(dialog, "Open Results file...", filter="Json file (*.json)")
            dialog.LineLoad.setText(filename)
        elif tip == "save":
            filename, _filter = QtWidgets.QFileDialog.getSaveFileName(dialog, "Choose save file..." + "Json file", ".json")
            if filename and ".json" not in filename:
                filename += ".json"
            dialog.LineSaveAn.setText(filename)
        elif tip == "savePymol":
            filename, _filter = QtWidgets.QFileDialog.getSaveFileName(dialog, "Choose save file..." + "PDB files", ".pdb")
            if filename and ".pdb" not in filename:
                filename += ".pdb"
            saveWin.LineSave.setText(filename)

    def updateProgramVersion(self):
        """Make probisDock plugin update and close current plugin window"""
        versionFile = os.path.join(Dock_directory, "version.txt")
        with open(versionFile, 'w') as myfile:
            myfile.write("UPDATE NOW")
        QtWidgets.QMessageBox.about(dialog, "Update", "Please restart probis.")
        dialog.close()
    
    def display_results(self, res):
        """Start displayin results on the PyMol console"""
        disp = display()
        disp.show_results(res)
        
    def fill_results_tree(self, sorter, res):
        """Fill results table with analysis results. sorter = sttring[proteins, ligands, all]; res = resuts dict""" 
        dialog.TreeResults.clear()
        if sorter == "ligands":
            """filter by chosen lignads in ligands list"""
            dialog.ListProteins.clearSelection()
            to_disp = []
            treeItems = []
            for item in dialog.ListLigands.selectedItems():
                for protein in res["Proteins"]:
                        for ligand in res["Proteins"][protein]["Ligands"]:
                            if item.text() == res["Proteins"][protein]["Ligands"][ligand]["Ligand"]:
                                for alignment in res["Proteins"][protein]["Ligands"][ligand]["Alignments"]:
                                    disp = [str(res["Proteins"][protein]["Protein"]), str(res["Proteins"][protein]["Ligands"][ligand]["Ligand"]), str(res["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment]["Alignment"]), float(res["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment]["Score"])]
                                    disp = [disp[0], disp[1], ('         ' + disp[2])[-9:], disp[3]]
                                    to_disp.append(disp)

            i=0
            # Sort chosen results
            for dock in sorted(to_disp, key=lambda score: score[3], reverse=False):
                # Make QTreeWidgetItem with attributes Rank, Protein, Ligand, Alignment, Score
                i+=1
                treeItem = QtWidgets.QTreeWidgetItem()
                treeItem.setText(0, ('         ' + str(i))[-9:]) # Bumper for string sorting, QTreeWidget accepts only strings and not integers
                treeItem.setText(1, dock[0])
                treeItem.setText(2, dock[1])
                treeItem.setText(3, dock[2])
                treeItem.setText(4, ('               ' + str(dock[3]))[-15:])
                treeItems.append(treeItem)
            dialog.TreeResults.addTopLevelItems(treeItems)
            dialog.TreeResults.sortItems(0, QtCore.Qt.AscendingOrder)
            dialog.TreeResults.setSortingEnabled(True)
        elif sorter == "proteins":
            """filter by chosen receptors in receptor list"""
            dialog.ListLigands.clearSelection()
            to_disp = []
            treeItems = []
            for item in dialog.ListProteins.selectedItems():
                for protein in res["Proteins"]:
                    if item.text() == res["Proteins"][protein]["Protein"]:
                        for ligand in res["Proteins"][protein]["Ligands"]:
                            for alignment in res["Proteins"][protein]["Ligands"][ligand]["Alignments"]:
                                disp = [str(res["Proteins"][protein]["Protein"]), str(res["Proteins"][protein]["Ligands"][ligand]["Ligand"]), str(res["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment]["Alignment"]), float(res["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment]["Score"])]
                                disp = [disp[0], disp[1], ('         ' + disp[2])[-9:], disp[3]]
                                to_disp.append(disp)

            i=0
            for dock in sorted(to_disp, key=lambda score: score[3], reverse=False):
                i+=1
                treeItem = QtWidgets.QTreeWidgetItem()
                treeItem.setText(0, ('         ' + str(i))[-9:])
                treeItem.setText(1, dock[0])
                treeItem.setText(2, dock[1])
                treeItem.setText(3, dock[2])
                treeItem.setText(4, ('               ' + str(dock[3]))[-15:])
                treeItems.append(treeItem)
            dialog.TreeResults.addTopLevelItems(treeItems)
            dialog.TreeResults.sortItems(0, QtCore.Qt.AscendingOrder)
            dialog.TreeResults.setSortingEnabled(True)
        elif sorter == "all":
            """display all results"""
            treeItems = []
            to_disp = []
            for protein in res["Proteins"]:
                for ligand in res["Proteins"][protein]["Ligands"]:
                    for alignment in res["Proteins"][protein]["Ligands"][ligand]["Alignments"]:
                        disp = [str(res["Proteins"][protein]["Protein"]), str(res["Proteins"][protein]["Ligands"][ligand]["Ligand"]), str(res["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment]["Alignment"]), float(res["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment]["Score"])]
                        disp = [disp[0], disp[1], ('         ' + disp[2])[-9:], disp[3]]
                        to_disp.append(disp)

            i=0
            for dock in sorted(to_disp, key=lambda score: score[3], reverse=False):
                i+=1
                treeItem = QtWidgets.QTreeWidgetItem()
                treeItem.setText(0, ('         ' + str(i))[-9:])
                treeItem.setText(1, dock[0])
                treeItem.setText(2, dock[1])
                treeItem.setText(3, dock[2])
                treeItem.setText(4, ('               ' + str(dock[3]))[-15:])
                treeItems.append(treeItem)
            dialog.TreeResults.addTopLevelItems(treeItems)
            dialog.TreeResults.sortItems(0, QtCore.Qt.AscendingOrder)
            dialog.TreeResults.setSortingEnabled(True)
            
    def saveAnalysis(self, res):
        """Save results dict into json file for future importing"""
        projectFile = dialog.LineSaveAn.text()
        if projectFile == "":
            QtWidgets.QMessageBox.about(dialog, "Export Project", "You need to select a valid file location.")

        elif not projectFile.endswith(".json"):
            QtWidgets.QMessageBox.about(dialog, "Export Project", "You need to select a valid .json file name.")
        
        else:
            with open (projectFile, "w") as exportFile:
                json.dump(res, exportFile)

            QtWidgets.QMessageBox.about(dialog, "Export Project", ("Project was exported successfully. \nSaved as: "+projectFile))
            dialog.LineSaveAn.clear()
            pent("Project exported to file: "+projectFile)
    
    def loadAnalysis(self):
        """Load previous results from saved json file"""
        projectFile = dialog.LineLoad.text()
        
        if not os.path.isfile(projectFile):
            QtWidgets.QMessageBox.about(dialog, "Export Project", "You need to select a valid file.")
            return
        
        if os.path.isfile(projectFile):
            with open (projectFile, "r") as importFile:
                global results
                results = json.loads(importFile.read())
                self.fill_lists_load()
                dialog.Tabs.setCurrentIndex(2)

    def fill_lists_load(self):
        """Fill lists of ligands and proteins in results for filtering purposes"""
        dialog.TreeResults.clear()
        dialog.ListProteins.clear()
        dialog.ListLigands.clear()
        
        proteins = []
        ligands = []
        for protein in results["Proteins"]:
            proteins.append(str(results["Proteins"][protein]['Protein']))
            for ligand in results["Proteins"][protein]["Ligands"]:
                lig = results["Proteins"][protein]["Ligands"][ligand]["Ligand"]
                if not lig in ligands:
                    ligands.append(str(lig))
        
        #Sort lists
        proteins = sorted(proteins)
        ligands = sorted(ligands)
        
        #Add items to lists
        dialog.ListProteins.addItems(proteins)
        dialog.ListLigands.addItems(ligands)  

class docking:
    def __init__(self):
        pent("Initializing docking")

    def start_analysis(self):
        """hub function for analysis start"""
        if "win" in sys.platform:
            #### FOR TESTING ONLY
            self.get_settings()
            post = post_docking()
            post.start_results(os.path.join(self.results_dir, "summary"), self.receptor_file)
        else:
            """start analysis"""
            pent("Gathering analysis settings")
            self.get_settings()
            update_win()
            pent("Creating analysis command")
            self.get_command()
            update_win()
            pent("Starting analysis")
            self.run_analysis()
            update_win()
            post = post_docking()
            post.start_results(os.path.join(self.results_dir, self.name), self.receptor_file)
    
    def run_analysis(self):
        """Run subprocess probisdock with commands created from input tab inputs"""
        subprocess.run(self.command, shell=False)
        
    def get_settings(self):
        """Get settings from user inputs and preprocess them for probisdock command line"""
        cur_time = datetime.now()
        self.name = "result_"+cur_time.strftime("%m_%d_%Y_%H_%M_%S") # Name is date/time specific. Format = reults_[month]_[day]_[year]_[hour]_[minute]_[second]
        if dialog.ComboMulti.currentIndex() == 0:
            self.multi_or_single = 0
        else:
            self.multi_or_single = 1
        
        self.probisdock_db_dir = dialog.LineReceptor.text()
        self.receptor_file = dialog.LineReceptor.text()
        self.templste_ligands_file = dialog.LineTemplate.text()
        self.centro_file = dialog.LineCentro.text()
        self.ligands_file = dialog.LineLigands.text()
        
        self.results_dir = dialog.LineResults.text()
        
        self.receptor_trim_dist = dialog.SpinTrimDistance.value()
        self.probis_level = dialog.SpinProbisScoring.value()
        self.max_seq_id = int(dialog.SpinMaxSequence.value())
        self.centro_clus_rad = dialog.SpinCentroRadius.value()
        self.max_frag_radius = dialog.SpinMaxFragmentRadius.value()
        
        if dialog.ComboReferenceState.currentIndex() == 0:
            self.ref_state = "mean"
        else:
            self.ref_state = "cumulative"
        
        if dialog.ComboAtomTypes.currentIndex() == 0:
            self.comp = "reduced"
        else:
            self.comp = "complete"
        
        if dialog.ComboRadOrNorm.currentIndex() == 0:
            self.rad_or_raw = "radial"
        else:
            self.rad_or_raw = "normalized_frequency"
        
        self.dist_cutoff = dialog.SpinScoringCutoff.value()
        self.step_non_bond = dialog.SpinKBStep.value()
        self.scale_non_bond = dialog.SpinKBScale.value()
        self.num_read_ligands = dialog.SpinMaxLigands.value()
        self.num_read_template_ligands = dialog.SpinMaxTempLigands.value()
        
        if dialog.ComboLigandSpin.currentIndex() == 0:
            self.spin_degrees = 0.0
        elif dialog.ComboLigandSpin.currentIndex() == 1:
            self.spin_degrees = 5.0
        elif dialog.ComboLigandSpin.currentIndex() == 2:
            self.spin_degrees = 10.0
        elif dialog.ComboLigandSpin.currentIndex() == 3:
            self.spin_degrees = 15.0
        elif dialog.ComboLigandSpin.currentIndex() == 4:
            self.spin_degrees = 20.0
        elif dialog.ComboLigandSpin.currentIndex() == 5:
            self.spin_degrees = 30.0
        elif dialog.ComboLigandSpin.currentIndex() == 6:
            self.spin_degrees = 60.0
        elif dialog.ComboLigandSpin.currentIndex() == 7:
            self.spin_degrees = 90.0
            
        self.clash_coeff = dialog.SpinClash.value()
        self.tol_seed_dist = dialog.SpinTolerance.value()
        self.lower_tol_seed_dist = dialog.SpinLowTolerance.value()
        self.upper_tol_seed_dist = dialog.SpinUpperTolerance.value()
        self.max_possible_conf = dialog.SpinMaxConf.value()
        self.link_iter = dialog.SpinMaxIter.value()
        self.docked_clus_rad = dialog.SpinRadiusConf.value()
        self.max_allow_energy = dialog.SpinMaxEnergy.value()

        if dialog.CheckIterative.isChecked() == True:
            self.iterative = True
        else:
            self.iterative = False
            
        self.n_cliques = dialog.SpinMaxCliques.value()
        self.max_steps = dialog.SpinMCQDW.value()
        
        if dialog.ComboForcefield.currentIndex() == 0:
            self.fftype = "kb"
        else:
            self.fftype = "phy"
            
        self.mini_tol = dialog.SpinMinTolerance.value()
        self.max_iterations = dialog.SpinMaxLinkIter.value()
        self.max_iterations_final = dialog.SpinMaxFinIter.value()
        self.update_freq = dialog.SpinUpdateSteps.value()
        self.position_tolerance = dialog.SpinPosTolerance.value()
        self.force_tol = dialog.SpinForceCon.value()
        self.flex_radius = dialog.SpinFlexRes.value()
        self.top_percent = dialog.SpinTopSeed.value()
        self.max_clique_size = dialog.SpinClique.value()
        self.grid_spacing = dialog.SpinGrid.value()
        self.num_univec = dialog.SpinUnitVector.value()
        self.conf_spin = dialog.SpinConfGenSpin.value()
        self.excluded_radius = dialog.SpinExclusionRad.value()
        self.max_interatomic_distance = dialog.SpinMaxInteratomic.value()
        self.dist_cutoff_probis_template = dialog.SpinProbisCutoff.value()
        self.pow_probis_template = dialog.SpinProbisDecay.value()
        self.clus_rad = dialog.SpinRadiusRigid.value()
        self.max_jobs = dialog.SpinMaxQueue.value()
        self.max_cache_size = dialog.SpinMaxCache.value()
        self.ncpu = dialog.SpinNCPU.value()
        
    def get_command(self):
        """create probisdock command form preprocessed user inputs"""
        """Be careful. Iterative currently doesn't work, might need to remove or force unchecked"""
        if self.multi_or_single == 0:
            if self.iterative == True:
                self.command = [proDockDir, 
                       "--receptor_file", self.receptor_file,
                       "--template_ligands_file", self.templste_ligands_file,
                       "--centro_file", self.centro_file,
                       "--ligands_file", self.ligands_file,
                       "--tmp_dir", Temp_directory,
                       "--summary_file", os.path.join(self.results_dir, self.name+".txt"),
                       "--docked_complexes_file", os.path.join(self.results_dir, self.name+".pdb"),
                       '--receptor_trim_dist', str(self.receptor_trim_dist),
                       '--probis_level', str(self.probis_level),
                       '--max_seq_id', str(self.max_seq_id),
                       '--centro_clus_rad', str(self.centro_clus_rad),
                       '--max_frag_radius', str(self.max_frag_radius),
                       '--ref_state', str(self.ref_state),
                       '--comp', str(self.comp),
                       '--rad_or_raw', str(self.rad_or_raw),
                       '--dist_cutoff', str(self.dist_cutoff),
                       '--step_non_bond', str(self.step_non_bond),
                       '--scale_non_bond', str(self.scale_non_bond),
                       '--num_read_ligands', str(self.num_read_ligands),
                       '--num_read_template_ligands', str(self.num_read_template_ligands),
                       '--spin_degrees', str(self.spin_degrees),
                       '--clash_coeff', str(self.clash_coeff),
                       '--tol_seed_dist', str(self.tol_seed_dist),
                       '--lower_tol_seed_dist', str(self.lower_tol_seed_dist),
                       '--upper_tol_seed_dist', str(self.upper_tol_seed_dist),
                       '--max_possible_conf', str(self.max_possible_conf),
                       '--link_iter', str(self.link_iter),
                       '--docked_clus_rad', str(self.docked_clus_rad),
                       '--max_allow_energy', str(self.max_allow_energy),
                       '--iterative', 
                       '--n_cliques', str(self.n_cliques),
                       '--max_steps', str(self.max_steps),
                       '--fftype', str(self.fftype),
                       '--mini_tol', str(self.mini_tol),
                       '--max_iterations', str(self.max_iterations),
                       '--max_iterations_final', str(self.max_iterations_final),
                       '--update_freq', str(self.update_freq),
                       '--position_tolerance', str(self.position_tolerance),
                       '--force_tol', str(self.force_tol),
                       '--flex_radius', str(self.flex_radius),
                       '--top_percent', str(self.top_percent),
                       '--max_clique_size', str(self.max_clique_size),
                       '--grid_spacing', str(self.grid_spacing),
                       '--num_univec', str(self.num_univec),
                       '--conf_spin', str(self.conf_spin),
                       '--excluded_radius', str(self.excluded_radius),
                       '--max_interatomic_distance', str(self.max_interatomic_distance),
                       '--dist_cutoff_probis_template', str(self.dist_cutoff_probis_template),
                       '--pow_probis_template', str(self.pow_probis_template),
                       '--clus_rad', str(self.clus_rad),
                       '--max_jobs', str(self.max_jobs),
                       '--max_cache_size', str(self.max_cache_size),
                       '--ncpu', str(self.ncpu)
                       ]
            else:
                self.command = [proDockDir, 
                       "--receptor_file", self.receptor_file,
                       "--template_ligands_file", self.templste_ligands_file,
                       "--centro_file", self.centro_file,
                       "--ligands_file", self.ligands_file,
                       "--tmp_dir", Temp_directory,
                       "--summary_file", os.path.join(self.results_dir, self.name+".txt"),
                       "--docked_complexes_file", os.path.join(self.results_dir, self.name+".pdb"),
                       '--receptor_trim_dist', str(self.receptor_trim_dist),
                       '--probis_level', str(self.probis_level),
                       '--max_seq_id', str(self.max_seq_id),
                       '--centro_clus_rad', str(self.centro_clus_rad),
                       '--max_frag_radius', str(self.max_frag_radius),
                       '--ref_state', str(self.ref_state),
                       '--comp', str(self.comp),
                       '--rad_or_raw', str(self.rad_or_raw),
                       '--dist_cutoff', str(self.dist_cutoff),
                       '--step_non_bond', str(self.step_non_bond),
                       '--scale_non_bond', str(self.scale_non_bond),
                       '--num_read_ligands', str(self.num_read_ligands),
                       '--num_read_template_ligands', str(self.num_read_template_ligands),
                       '--spin_degrees', str(self.spin_degrees),
                       '--clash_coeff', str(self.clash_coeff),
                       '--tol_seed_dist', str(self.tol_seed_dist),
                       '--lower_tol_seed_dist', str(self.lower_tol_seed_dist),
                       '--upper_tol_seed_dist', str(self.upper_tol_seed_dist),
                       '--max_possible_conf', str(self.max_possible_conf),
                       '--link_iter', str(self.link_iter),
                       '--docked_clus_rad', str(self.docked_clus_rad),
                       '--max_allow_energy', str(self.max_allow_energy),
                       '--n_cliques', str(self.n_cliques),
                       '--max_steps', str(self.max_steps),
                       '--fftype', str(self.fftype),
                       '--mini_tol', str(self.mini_tol),
                       '--max_iterations', str(self.max_iterations),
                       '--max_iterations_final', str(self.max_iterations_final),
                       '--update_freq', str(self.update_freq),
                       '--position_tolerance', str(self.position_tolerance),
                       '--force_tol', str(self.force_tol),
                       '--flex_radius', str(self.flex_radius),
                       '--top_percent', str(self.top_percent),
                       '--max_clique_size', str(self.max_clique_size),
                       '--grid_spacing', str(self.grid_spacing),
                       '--num_univec', str(self.num_univec),
                       '--conf_spin', str(self.conf_spin),
                       '--excluded_radius', str(self.excluded_radius),
                       '--max_interatomic_distance', str(self.max_interatomic_distance),
                       '--dist_cutoff_probis_template', str(self.dist_cutoff_probis_template),
                       '--pow_probis_template', str(self.pow_probis_template),
                       '--clus_rad', str(self.clus_rad),
                       '--max_jobs', str(self.max_jobs),
                       '--max_cache_size', str(self.max_cache_size),
                       '--ncpu', str(self.ncpu)
                       ]
        else:
            if self.iterative == True:
                self.command = [proDockDir, 
                       "--probisdock_db_dir", self.probisdock_db_dir,
                       "--ligands_file", self.ligands_file,
                       "--tmp_dir", Temp_directory,
                       "--summary_file", os.path.join(self.results_dir, self.name+".txt"),
                       "--docked_complexes_file", os.path.join(self.results_dir, self.name+".pdb"),
                       '--receptor_trim_dist', str(self.receptor_trim_dist),
                       '--probis_level', str(self.probis_level),
                       '--max_seq_id', str(self.max_seq_id),
                       '--centro_clus_rad', str(self.centro_clus_rad),
                       '--max_frag_radius', str(self.max_frag_radius),
                       '--ref_state', str(self.ref_state),
                       '--comp', str(self.comp),
                       '--rad_or_raw', str(self.rad_or_raw),
                       '--dist_cutoff', str(self.dist_cutoff),
                       '--step_non_bond', str(self.step_non_bond),
                       '--scale_non_bond', str(self.scale_non_bond),
                       '--num_read_ligands', str(self.num_read_ligands),
                       '--num_read_template_ligands', str(self.num_read_template_ligands),
                       '--spin_degrees', str(self.spin_degrees),
                       '--clash_coeff', str(self.clash_coeff),
                       '--tol_seed_dist', str(self.tol_seed_dist),
                       '--lower_tol_seed_dist', str(self.lower_tol_seed_dist),
                       '--upper_tol_seed_dist', str(self.upper_tol_seed_dist),
                       '--max_possible_conf', str(self.max_possible_conf),
                       '--link_iter', str(self.link_iter),
                       '--docked_clus_rad', str(self.docked_clus_rad),
                       '--max_allow_energy', str(self.max_allow_energy),
                       '--iterative', 
                       '--n_cliques', str(self.n_cliques),
                       '--max_steps', str(self.max_steps),
                       '--fftype', str(self.fftype),
                       '--mini_tol', str(self.mini_tol),
                       '--max_iterations', str(self.max_iterations),
                       '--max_iterations_final', str(self.max_iterations_final),
                       '--update_freq', str(self.update_freq),
                       '--position_tolerance', str(self.position_tolerance),
                       '--force_tol', str(self.force_tol),
                       '--flex_radius', str(self.flex_radius),
                       '--top_percent', str(self.top_percent),
                       '--max_clique_size', str(self.max_clique_size),
                       '--grid_spacing', str(self.grid_spacing),
                       '--num_univec', str(self.num_univec),
                       '--conf_spin', str(self.conf_spin),
                       '--excluded_radius', str(self.excluded_radius),
                       '--max_interatomic_distance', str(self.max_interatomic_distance),
                       '--dist_cutoff_probis_template', str(self.dist_cutoff_probis_template),
                       '--pow_probis_template', str(self.pow_probis_template),
                       '--clus_rad', str(self.clus_rad),
                       '--max_jobs', str(self.max_jobs),
                       '--max_cache_size', str(self.max_cache_size),
                       '--ncpu', str(self.ncpu)
                       ]
            else:
                self.command = [proDockDir, 
                       "--probisdock_db_dir", self.probisdock_db_dir,
                       "--ligands_file", self.ligands_file,
                       "--tmp_dir", Temp_directory,
                       "--summary_file", os.path.join(self.results_dir, self.name+".txt"),
                       "--docked_complexes_file", os.path.join(self.results_dir, self.name+".pdb"),
                       '--receptor_trim_dist', str(self.receptor_trim_dist),
                       '--probis_level', str(self.probis_level),
                       '--max_seq_id', str(self.max_seq_id),
                       '--centro_clus_rad', str(self.centro_clus_rad),
                       '--max_frag_radius', str(self.max_frag_radius),
                       '--ref_state', str(self.ref_state),
                       '--comp', str(self.comp),
                       '--rad_or_raw', str(self.rad_or_raw),
                       '--dist_cutoff', str(self.dist_cutoff),
                       '--step_non_bond', str(self.step_non_bond),
                       '--scale_non_bond', str(self.scale_non_bond),
                       '--num_read_ligands', str(self.num_read_ligands),
                       '--num_read_template_ligands', str(self.num_read_template_ligands),
                       '--spin_degrees', str(self.spin_degrees),
                       '--clash_coeff', str(self.clash_coeff),
                       '--tol_seed_dist', str(self.tol_seed_dist),
                       '--lower_tol_seed_dist', str(self.lower_tol_seed_dist),
                       '--upper_tol_seed_dist', str(self.upper_tol_seed_dist),
                       '--max_possible_conf', str(self.max_possible_conf),
                       '--link_iter', str(self.link_iter),
                       '--docked_clus_rad', str(self.docked_clus_rad),
                       '--max_allow_energy', str(self.max_allow_energy),
                       '--n_cliques', str(self.n_cliques),
                       '--max_steps', str(self.max_steps),
                       '--fftype', str(self.fftype),
                       '--mini_tol', str(self.mini_tol),
                       '--max_iterations', str(self.max_iterations),
                       '--max_iterations_final', str(self.max_iterations_final),
                       '--update_freq', str(self.update_freq),
                       '--position_tolerance', str(self.position_tolerance),
                       '--force_tol', str(self.force_tol),
                       '--flex_radius', str(self.flex_radius),
                       '--top_percent', str(self.top_percent),
                       '--max_clique_size', str(self.max_clique_size),
                       '--grid_spacing', str(self.grid_spacing),
                       '--num_univec', str(self.num_univec),
                       '--conf_spin', str(self.conf_spin),
                       '--excluded_radius', str(self.excluded_radius),
                       '--max_interatomic_distance', str(self.max_interatomic_distance),
                       '--dist_cutoff_probis_template', str(self.dist_cutoff_probis_template),
                       '--pow_probis_template', str(self.pow_probis_template),
                       '--clus_rad', str(self.clus_rad),
                       '--max_jobs', str(self.max_jobs),
                       '--max_cache_size', str(self.max_cache_size),
                       '--ncpu', str(self.ncpu)
                       ]
        
class post_docking:
    """Post-processing after docking"""
    def __init__(self):
        """reset file settings"""
        pent("Analyzing results")
        self.textRes = ""
        self.pdbRes = ""
    
    def start_results(self, resultFile, queryLocation):
        """Start postprocessining results. resultsFile = file directory of probisdock results ; queryLocation = location of query receptor(s)"""
        self.textRes = resultFile + ".txt"
        self.pdbRes = resultFile + ".pdb"
        if queryLocation.endswith(".pdb") or queryLocation.endswith(".gz") or queryLocation.endswith(".mol2"):
            self.queryLocation = os.path.split(queryLocation)[0]
        else:
            self.queryLocation = queryLocation
        update_win()
        self.read_results()
        update_win()
        self.fill_lists()
        dialog.Tabs.setCurrentIndex(2)
        
    
    def read_results(self):
        """Read probisdock results. Scores file and ligands in complex structure file"""
        self.results = {}
        self.results["QueryLocation"] = self.queryLocation #Where to find query proteins. Their structures are not saved in results dict/file since it qould massively increase their size.
        self.results["Proteins"]={} #Start results dict
        
        """Results dict is structured as such:
            Proteins:
                Protein 1:
                    Protein = str[protein file name]
                    Location = str[directory of protein file]
                    Centro = str[centroids code]
                    Ligands:
                        Ligand 1:
                            Ligand = str[ligand name]
                            Allignments:
                                Allignment 1:
                                    Alignment = str[alignment number]
                                    Score = str[complex(Protein 1 - Ligand 1 (allignment 1)) probisdock score]
                                    Code = str[pdb structure of liqand in this allignment]
                                Allignemnt 2:
                                    ...
                        Ligand 2:
                            ...
                Protein 2:
                    ...
                Protein 3:
                    ...
        """
                
        
        with open (self.pdbRes, "r") as file:
            pdb_code = file.read()
            split_code = pdb_code.split("END\nTITLE")
        
        for molecule in split_code:
            # try:
            # Analize ligand in complex structure from file, split its attributes and add them split to resutls dict
            molecule = re.split("Receptor=| Ligand=| ConfID=| Score=|\nMODEL        *", molecule)
            protein = molecule[1]
            ligand = molecule[2]
            alignment = molecule[3]
            score = molecule[4]
            code = molecule[5][1:].strip()
            if not protein in self.results["Proteins"]:
                self.results["Proteins"][protein] = {}
                self.results["Proteins"][protein]['Protein'] = protein
                self.results["Proteins"][protein]["Location"] = os.path.join(self.queryLocation, protein)
                
                if dialog.ComboMulti.currentIndex() == 0:
                    centro_file = dialog.LineCentro.text()
                else:
                    file_name = "probisdock_centro_" + protein.split("receptor_")[1].split(".pdb")[0] + ".cen.gz"
                    centro_file = os.path.join(dialog.LineReceptor.text(), file_name)
                    
                with gzip.open(centro_file,'rb') as archive:
                    file = archive.read()
                    gridData = file.decode("utf-8").split("\n")
                sites=[]
                lines=[]
                for i in gridData:
                    if i == gridData[0]:
                        pass
                    else:
                        data = i.split()
                        if data != []:
                            if not data[0] in sites and len(sites) > 0:
                                lines.append("ENDMDL\n")
                            xForm = ("        " + str(data[1]))[-8:]
                            yForm = ("        " + str(data[2]))[-8:]
                            zForm = ("        " + str(data[3]))[-8:]
                            line = "ATOM      1   U  DIK     1    {}{}{}\n".format(xForm, yForm, zForm)
                            lines.append(line)
                        else:
                            pass
                lines.append("ENDMDL\n")
                centro_code = "".join(lines)
                
                self.results["Proteins"][protein]['Centro'] = centro_code

                self.results["Proteins"][protein]['Ligands'] = {}
            if not ligand in self.results["Proteins"][protein]["Ligands"]:
                self.results["Proteins"][protein]["Ligands"][ligand] = {}
                self.results["Proteins"][protein]["Ligands"][ligand]["Ligand"] = ligand
                self.results["Proteins"][protein]["Ligands"][ligand]["Alignments"] = {}
            if not alignment in self.results["Proteins"][protein]["Ligands"][ligand]["Alignments"]:
                self.results["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment] = {}
                self.results["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment]['Alignment'] = alignment
                self.results["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment]['Score'] = score
                self.results["Proteins"][protein]["Ligands"][ligand]["Alignments"][alignment]['Code'] = code
            # except:
            #     pent("Passed a pair")
            
            update_win()

        global results
        results = self.results
        
    def fill_lists(self):
        """Fill lists of receptors and ligands for filtering"""
        dialog.ListProteins.clear()
        dialog.ListLigands.clear()
        
        self.proteins = []
        self.ligands = []
        for protein in self.results["Proteins"]:
            self.proteins.append(str(self.results["Proteins"][protein]['Protein']))
            for ligand in self.results["Proteins"][protein]["Ligands"]:
                lig = self.results["Proteins"][protein]["Ligands"][ligand]["Ligand"]
                if not lig in self.ligands:
                    self.ligands.append(str(lig))
            update_win()

        #Sort lists
        self.proteins = sorted(self.proteins)
        self.ligands = sorted(self.ligands)
        
        #Add items to lists
        dialog.ListProteins.addItems(self.proteins)
        dialog.ListLigands.addItems(self.ligands) 
        update_win()

class display:
    """Displaing resutlts on pymol console"""
    def __init__(self):
        pent("Displaying results")
        
    def show_results(self, res):
        """HUB function for displaying results"""
        self.get_selected() #Check which complexes are chosen
        self.check_current() #Check if any structures are already in pymol console and skip them out of chosen list (For optimization purposes)
        if not dialog.CheckKeep.isChecked(): #Keep display checkbox allows the user to not delete already displayed results
            self.delete_remaining() #Delete displayed structures thet are not in chosen list to be displayed
        self.draw_missing(res) #Display chosen structures that have beem chosen and are not yet displayed
    
    def get_selected(self):
        """See what reults are chosen on the list"""
        selected = dialog.TreeResults.selectedItems()
        self.chosen_proteins = []
        self.chosen_ligands = []
        self.chosen_centros = []
        for item in selected:
            if not item.text(1) in self.chosen_proteins:
                self.chosen_proteins.append(item.text(1))
                if dialog.CheckCentros.isChecked() == True:
                    centro_name = "centro_" + item.text(1).split("receptor_")[1].split(".pdb")[0]
                    self.chosen_centros.append(centro_name)
            ligand_name = item.text(2).strip()+"-_-"+item.text(3).strip()+"-_-"+item.text(1).strip()
            if not ligand_name in self.chosen_ligands:
                self.chosen_ligands.append(ligand_name)
    
    def check_current(self):
        """Check if an chosen reults are already being displayed"""
        self.keep = []
        for item in cmd.get_names('all'):
            if item in self.chosen_proteins:
                self.chosen_proteins.remove(item)
                self.keep.append(item)
            if item in self.chosen_ligands:
                self.chosen_ligands.remove(item)
                self.keep.append(item)
            if item in self.chosen_centros:
                self.chosen_centros.remove(item)
                self.keep.append(item)
    
    def delete_remaining(self):
        """Delete alrteady displayed structures that are not to be shown anymore"""
        for item in cmd.get_names('all'):
            if not item in self.chosen_proteins and not item in self.chosen_ligands and not item in self.chosen_centros and not item in self.keep:
                cmd.delete(item)

    def draw_missing(self, res):
        """Display selected result structures that have are not yet displayed"""
        for item in self.chosen_proteins:
            try:
                cmd.load(res["Proteins"][item]["Location"], os.path.split(res["Proteins"][item]["Location"])[1])
                cmd.zoom(os.path.split(res["Proteins"][item]["Location"])[1])
            except:
                pent("Protein "+res["Proteins"][item]["Location"]+" not found so it can not be displayed")
            if dialog.CheckCentros.isChecked() == True:
                for centro in self.chosen_centros:
                    cmd.read_pdbstr(res["Proteins"][item]["Centro"], centro)
                    self.showCentroGrid(centro)
        for item in self.chosen_ligands:
            prot = item.split("-_-")[2]
            ligand = item.split("-_-")[0]
            al = item.split("-_-")[1]
            cmd.read_pdbstr(res["Proteins"][prot]["Ligands"][ligand]["Alignments"][al]["Code"], item)
    
    def showCentroGrid(self, centro):
        name = centro
        cmd.hide("everything", name)
        cmd.show("surface", name)
        cmd.set("transparency", 0.5, name)
        cmd.set("solvent_radius", 2, name)
        colors = ["red", "green", "blue", "yellow", "orange", "violet", "cyan", "pink", "gray", "magenta"]
        cmd.color(random.choice(colors), name)
        

def pent(string):
    """Print with "ProBiS Dock:    " prefix"""
    print("ProBiS Dock:\t", string) 

def debug(string):
    """ Line for debugging purposes"""
    print("DEBUG: \t", string) 

def check_current_ver():
    """Check which version of plugin is currently installed"""
    try:
        with open(os.path.join(Dock_directory, "version.txt"))as curFile:
            curVer = curFile.readline().strip()
        return(curVer)
    except:
        return("Not found")

def check_latest_ver():
    """Check what is the latest version avaliable on the internet"""
    try:
        # firstVersionURL="http://insilab.org/files/probis-dock/version.txt"
        firstVersionURL = "https://gitlab.com/JuricV/skupni-gui-lisica_probis/-/raw/master/ProBiS_Dock/.probisDock/version.txt"
        versionFile = urllib.request.urlopen(firstVersionURL, timeout=5)
        latestVersion = versionFile.read().decode(encoding="utf-8").strip()
            
    except HTTPError as e1:
        print("HTTP Error:", e1.code, e1.reason)
        latestVersion = current_version
    except URLError as e2:
        print("URL Error:", e2.reason)
        latestVersion = current_version
    except Exception as e:
        print("Error: Insilab server down?")
        latestVersion = current_version

    return(latestVersion)

       
def checkversions():
    """Save current and latest avaliable versions as global variables"""
    global current_version, latest_version
    current_version = check_current_ver()
    latest_version = check_latest_ver()

def update_win():
    try:
        working.LabelCoffee.setText(random.choice(update_messages))
        working.setFocus()
    except:
        pass
    QtWidgets.QApplication.processEvents()

def run():
    """Starting function for plugin.. Should be called by installation script"""
    pent("Starting plugin!")
    checkversions()
    global results
    results = {}
    gui = GUI()
    gui.run()
    