from __future__ import absolute_import
from __future__ import print_function
import sys

#import re
#import distutils
import os
import subprocess
import stat
import logging
#from logging import handlers
#log file is used to debug the code when necessary
from pymol import cmd
from pymol.Qt import QtWidgets, QtCore
from pymol.Qt.utils import loadUi
import time
import multiprocessing
import datetime
import shutil
 # Use for checking License Status- About Tab

import urllib.request, urllib.error, urllib.parse
import pymol
import pymol.plugins

startupDirectories=pymol.plugins.get_startup_path()
for pyDir in startupDirectories:
    sys.path.append(pyDir)
    #print(("pydir1 = ", pyDir.encode('string-escape')))
    #print(("pydir2 = ", pyDir))

#print "sys.path = ", sys.path

import lisica

configure=lisica.Configuration()
exe_filename=configure.exe_File()

lisicaFolder=lisica.LISICA_DIRECTORY
resultsFolder=os.path.abspath(os.curdir)
logFolder=os.path.join(lisicaFolder,"Log")
uiFolder=os.path.join(lisicaFolder,"UI")
versionFile=os.path.join(lisicaFolder, "version.txt")
#lisicaExeDirectory=os.path.join(lisicaFolder, "bin", "lisica")


exe_path=os.path.normpath(os.path.join(lisicaFolder,"bin",exe_filename))

timestamp=datetime.datetime.fromtimestamp(time.time())
log_filename="log_"+str(timestamp.strftime('%m%d%H%M%S'))+".log"
## log file for each lisica process 

####################################################################
####                                                            ####
####      Setting up the log file for LiSiCA                    ####
####                                                            ####
####################################################################

### Configuring a logger named lisica
log=logging.getLogger("lisica")
### logger data is written to the file licisa.log in the LiSiCA folder in C:/Program files
handle=logging.FileHandler(os.path.join(logFolder,log_filename))
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handle.setFormatter(formatter)
log.addHandler(handle)
log.setLevel(logging.DEBUG)

sys.path.append(lisicaFolder)

configure=lisica.Configuration()
exe_filename=configure.exe_File()
exe_path=os.path.normpath(os.path.join(lisicaFolder,"bin",exe_filename))
st = os.stat(exe_path)
#os.chmod(exe_path, st.st_mode | stat.S_IEXEC)
if os.access(exe_path, os.X_OK) == True:
    print("lisica executable ok")
else:
    #print("lisica executable present but no permissions set")
    #st = os.stat(lisicaExeDirectory)
    os.chmod(exe_path, st.st_mode | stat.S_IEXEC)
    print("Lisica executable permission set")
    print("Lisica directory:      : ", exe_path)                

####################################################################
####                                                            ####
####      Function to delete old files                          ####
####                                                            ####
####################################################################

def deleteOldFiles(folder):
    try:
        deleted=0
        if folder==logFolder:
            days=7
        
        now=time.time()
        for f in os.listdir(folder):
            f=os.path.join(folder, f)
        
            if os.stat(f).st_mtime < now - days * 86400:
                #if the contents of the file/folder is last modified before 'days' days
                if os.stat(f).st_atime < now - 5 * 86400:
                    #if the contents of the file/folder is last accessed before 5 days
                    if os.path.isfile(f):
                        os.remove(f) 
                        deleted+=1   
                    elif os.path.isdir(f):
                        os.rmdir(f)
                        deleted+=1
    except IOError:
        log.error("IO Error")
    except OSError:
        log.error("OSError")
    except:
        log.error("Some error while deleting old log files")
    else:
        log.info("Old log files removed")
    
def internetOn():
        try:
            urllib.request.urlopen('http://insilab.org',timeout=5)
            return True
        except:
            return False

class GUI():
    def __init__(self):
        self.firstVersionURL = "http://insilab.org/files/lisica-plugin2/version.txt"
        self.firstArchiveURL = "http://insilab.org/files/lisica-plugin2/archive.zip"
        self.currentVersion=""
        self.latestVersion=""
            
    def inputValidation(self):
        msg=" "
        if os.path.isfile(self.form.lineReference.text()):
            if self.form.lineReference.text().endswith(".mol2"):
                log.info("Ref molecule file: '%s'",self.form.lineReference.text())
            else:
                log.error("Wrong file format")
                self.update()
        else:
            log.error("Ref molecule file path is invalid")
            msg="Invalid path for reference molecule\n"
            
            self.update()
        if os.path.isfile(self.form.lineTarget.text()):
            if self.form.lineTarget.text().endswith(".mol2"):
                log.info("Target molecule file: '%s'",self.form.lineTarget.text())
            elif self.self.form.lineTarget.text().endswith(".mol2.gz"):
                log.info("Ref molecule file: '%s' (gzip)",self.form.lineTarget.text())
            else:
                log.error("Wrong file format")
        else:
            log.error("Target molecule file path is invalid")
            msg=msg + "\nInvalid path for Target molecule"
            
        if os.path.isdir(self.form.lineSave.text()):
            log.info("The results will be written in the folder: {0} ".format(self.form.lineSave.text()))
        else:    
            log.debug("Folder name does not exist..!")
            self.form.lineSave.setText(resultsFolder) 
            msg=msg + "\nInvalid path for saving the results"
        return msg
    
    def createCommand(self):
        self.lisica_path=exe_path
        
        if self.form.radio3D.isChecked():
            dimension=3
        elif self.form.radio2D.isChecked():
            dimension=2
            
        self.command=self.lisica_path\
                    +" -R "+os.path.normpath(self.form.lineReference.text())\
                    +" -T "+os.path.normpath(self.form.lineTarget.text())\
                    +" -n "+str(self.form.spinCores.value())\
                    +" -d "+str(dimension)\
                    +" -w "+self.form.lineNumber.text()\
                    +" -f "+self.form.lineSave.text()
        
        if self.form.checkBox.isChecked():
            self.command=self.command+" -h "
        if self.form.radio2D.isChecked():
            self.command=self.command+" -s "+str(self.form.lineShortestPath.text())
        elif self.form.radio3D.isChecked():
            self.command=self.command\
                        +" -m "+str(self.form.lineShortestPath.text())\
                        +" -c "+str(self.form.lineConformations.text())
        else:
            log.error("Dimension is neither 2 or 3..!")
            
        self.command=self.command+" --plugin"        
        log.info("Command for lisica: '{0}'".format(self.command))
        return self.command
                    
            
    def submitted(self):
        #print ("submitting test")
        self.updateCommand()
        try:
            alert=self.inputValidation()
            if alert==" ":
                self.createCommand()
            
                print((self.command))
    
                self.startupinfo=None
                if os.name=='nt':
                    try:
                        self.startupinfo=subprocess.STARTUPINFO()
                        self.startupinfo.dwFlags|=subprocess.STARTF_USESHOWWINDOW
                    except:
                        self.startupinfo.dwFlags|=subprocess._subprocess.STARTF_USESHOWWINDOW
                
                os.chdir( lisicaFolder ) 
                self.proc = subprocess.Popen(self.command.split(),shell=False,startupinfo=self.startupinfo)
                while self.proc.poll() is None:
                    try:
                        self.update()
                    except:
                        log.info("Application has been terminated")
                if os.path.isfile(os.path.join(lisicaFolder,'done.txt')):
                    
                    #Read the result folder name from the done.txt file
                    done_File=open(os.path.join(lisicaFolder,'done.txt'))
                    self.timestamp_FolderName=done_File.readline()
                    #print((self.timestamp_FolderName))
                    self.timestamp_Folder=os.path.join(self.form.lineSave.text(),self.timestamp_FolderName)
                    done_File.close()
                    self.Ref=self.form.lineReference.text()
    
                    print(("before hokuspokus =", self.timestamp_Folder))
                    self.timestamp_Folder=self.timestamp_Folder[:-1]
                    print(("after =", self.timestamp_Folder))
                    self.addRefFileToResults()
    
                    log.debug("Execution of liSiCA is complete")
                    log.info("Files with common structures are available in results directory")
                    self.form.tabWidget.setCurrentIndex(2)
                    
                    self.uploadResults()
                    try:
                        os.remove(os.path.join(lisicaFolder,'done.txt'))
                    except IOError:
                            log.exception(IOError.message)
                else:
                    self.alertMsg("Abnormal termination of LiSiCA")
            
            else:
                self.alertMsg(alert)
        except Exception as e:
            print ("note: something went wrong in the middle of calculation")
            log.exception(e)
            
        self.updateCommand()
    def loadResults(self):
            
        self.timestamp_Folder=self.form.lineResult.text()
        self.Ref=os.path.join(self.timestamp_Folder,"Ref.mol2")
        self.form.tabWidget.setCurrentIndex(2)
        self.uploadResults()

    def uploadResults(self):
        self.form.treeMolecules.clear()
        self.form.treeAtoms.clear()
        compd_num=0
        try: 
            print(self.form.lineNumber.text())
            result_File=os.path.normpath(os.path.join(self.timestamp_Folder,r"lisica_results.txt"))
            #result_File=result_File.encode('string-escape')
            print(result_File)
            Textfile=open(result_File,"r") 
            
            log.info("No IO Error. File '%s' open for reading" %Textfile.name)
            
            for block in iter(lambda: Textfile.readline(), ""):
                compd_num=compd_num+1
                row=block.split()
                rankedList=(compd_num,row[1],row[0])
                #print(rankedList)
                element = str(rankedList[0])
                padded = ('      '+element)[-6:] # enables numeric sorting of ranks

                rowcount = compd_num-1
                self.form.treeMolecules.setSortingEnabled(False)
                self.form.treeMolecules.addTopLevelItem(QtWidgets.QTreeWidgetItem(rowcount))
                self.form.treeMolecules.topLevelItem(rowcount).setText(2, str(rankedList[2]))
                self.form.treeMolecules.topLevelItem(rowcount).setText(1, str(rankedList[1]))  
                self.form.treeMolecules.topLevelItem(rowcount).setText(0, padded)
            
                #self.form.treeMolecules.topLevelItem(rowcount).setTextAlignment(0, QtCore.Qt.AlignHCenter)
                #self.form.treeMolecules.topLevelItem(rowcount).setTextAlignment(1, QtCore.Qt.AlignHCenter)  
                #self.form.treeMolecules.topLevelItem(rowcount).setTextAlignment(1, QtCore.Qt.AlignHCenter)
                self.form.treeMolecules.sortItems(0, QtCore.Qt.AscendingOrder)
                self.form.treeMolecules.setSortingEnabled(True)
                if compd_num==int(self.form.lineNumber.text()):
                    #print ("Done")
                    break
            log.info("Has finished uploading data")
            Textfile.close()
            
            
        except IOError:
            log.error(IOError.errno)
            log.error(IOError.message)
            log.error(IOError.filename)
    
    def showCorr(self,ar):
        
        try:

            cmd.delete(name="all")
            
            cmd.load(self.Ref,object="obj1")

            pymol.util.cbag(selection="obj1")
            
            curItem = self.form.treeMolecules.selectedItems()
            for a in curItem:
                rank = a.text(0).strip()
                zincId = a.text(1)
                score = a.text(2)
            print("curItem: Rank "+rank+" Zinc ID: "+zincId+" Tanimoto score: "+score)

            #Empty atom tree/table
            self.form.treeAtoms.clear()

            file_type=".mol2"
            result_Filename=rank+ "_" +zincId+file_type
            log.info("Filename:'%s'" %result_Filename)
            i=0
            j=0
            k=0
            line=()
            #timestamp_Folder=os.path.join(resultsFolder,timestamp_Done)
            file_Mol=os.path.join(self.timestamp_Folder,result_Filename)
            print (file_Mol)
           
            ref=[]
            tar=[]
            
            try:  
                fh=open(file_Mol,"r") #handle possible exceptions
                cmd.load(file_Mol,object="obj2")
                pymol.util.cbac(selection="obj2")
            except IOError:
                self.alertMsg("No data found: The result molecules were not written to the output")
            for block in iter(lambda: fh.readline(), ""):
                if i==1:
                    if j==1:
                        if not block=='\n':
                        
                            k=k+1
                            line=block.split()
                            ref.append((line[0],line[1]))
                            tar.append((line[2],line[3]))

                            rowcount = k-1
                            self.form.treeAtoms.setSortingEnabled(False)
                            self.form.treeAtoms.addTopLevelItem(QtWidgets.QTreeWidgetItem(rowcount))
                            self.form.treeAtoms.topLevelItem(rowcount).setText(4, str(line[4]))
                            self.form.treeAtoms.topLevelItem(rowcount).setText(3, str(line[3]))
                            self.form.treeAtoms.topLevelItem(rowcount).setText(2, str(line[2]))
                            self.form.treeAtoms.topLevelItem(rowcount).setText(1, str(line[1]))  
                            self.form.treeAtoms.topLevelItem(rowcount).setText(0, str(line[0]))
                            
                            #self.form.treeAtoms.topLevelItem(rowcount).setTextAlignment(0, QtCore.Qt.AlignHCenter)
                            #self.form.treeAtoms.topLevelItem(rowcount).setTextAlignment(1, QtCore.Qt.AlignHCenter)  
                            #self.form.treeAtoms.topLevelItem(rowcount).setTextAlignment(2, QtCore.Qt.AlignHCenter)
                            #self.form.treeAtoms.topLevelItem(rowcount).setTextAlignment(3, QtCore.Qt.AlignHCenter)
                            #self.form.treeAtoms.topLevelItem(rowcount).setTextAlignment(4, QtCore.Qt.AlignHCenter)
                            self.form.treeMolecules.setSortingEnabled(True)

                        else:
                            i=0
                            j=0
                if "LiSiCA  RESULTS" in block:
                    i=1
                if i==1:
                    if "--------------" in block:
                        j=1
            log.info("Displayed the atom correspondence of the selected atom")
                     
            if self.form.radio2D.isChecked():
                cmd.set("grid_mode",value=1)
                cmd.set("grid_slot",value=1,selection="obj1")
                cmd.set("grid_slot",value=2,selection="obj2")
            elif self.form.radio3D.isChecked():
                cmd.set("grid_mode",value=0)
            else:
                log.error("Dimension value is wrong")    

            selection=[]
            for idx, (atom_id, atom_name) in enumerate(ref):
                ref_atom_id = ref[idx][0]
                ref_atom_name = ref[idx][1]
                tar_atom_id = tar[idx][0]
                tar_atom_name = tar[idx][1]
                selection.append("object obj1 and id "+ref_atom_id+" and name "+ref_atom_name)
                selection.append("object obj2 and id "+tar_atom_id+" and name "+tar_atom_name)
            # PyMOL 2.5.0 on Windows crashes on pair_fit, so just use another fit (not so good) for that version
            if cmd.get_version()[0][0:4] != '2.5.':
                print("Used pair_fit function")
                cmd.pair_fit(*selection)
            else:
                print("Used fit function")
                cmd.fit("object obj1", "object obj2")
            
            cmd.orient()
            cmd.turn("z", 90)
        
        except Exception as e:
            print (e)
        
    def addRefFileToResults(self):
        #print((self.timestamp_Folder))
        shutil.copyfile(self.form.lineReference.text(),os.path.normpath(os.path.join(self.timestamp_Folder,"Ref.mol2")))
        
        
    def showCorrAtoms(self,ar):
        try:
            curItem = self.form.treeAtoms.selectedItems()
            for a in curItem:
                ref_atom_id = str(a.text(0))
                ref_atom_name = str(a.text(1))
                tar_atom_id = str(a.text(2))
                tar_atom_name = str(a.text(3))
            selectedPair="obj1 and id "+ref_atom_id+" and name "+ref_atom_name+" or obj2 and id "+tar_atom_id+" and name "+tar_atom_name
            cmd.select("ref",selection=selectedPair)
            
        except:
            print("None selected from box2")
            
    def updateCommand(self):
        x=self.createCommand()
        self.form.plainTextEdit.setPlainText(x)
        
    def alertMsg(alert_message):
        err = QtWidgets.QApplication([])
        error_dialog = QtWidgets.QErrorMessage()
        error_dialog.showMessage("{0}".format(alert_message))
        err.exec_()  

    def closeEvent(self, event):
        close = QtWidgets.QMessageBox()
        close.setText("Are you sure you want to exit LiSiCA?")
        close.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel)
        close = close.exec()
            
        if close == QtWidgets.QMessageBox.Yes:
            event.accept()
            log.info("lisica.exe terminated")
        else:
            event.ignore()              
    
####################################################################
####                                                            ####
####                GUI design                                  ####
####                                                            ####
####################################################################

    
    def initUI(self):
        global ui
        ui = self.makeUI()
        ui.show()
    
    def makeUI(self):
        # create a new Window
        ui = QtWidgets.QMainWindow()

        # populate the Window from our *.ui file which was created with the Qt Designer
        uifile = os.path.join(uiFolder, 'LisicaMain.ui')
        self.form = loadUi(uifile, ui)
        
        # callback for the "Browse" button
        def browse_filename(actionType, extension, output):
            fileType = "{0} File (*.{1})".format(extension, extension)
            #fileType = "*.{0}".format(extension)
            if actionType == "save":
                filename = QtWidgets.QFileDialog.getExistingDirectory(ui, "Choose directory")
            elif actionType == "load":
                filename = QtWidgets.QFileDialog.getExistingDirectory(ui, "Choose directory")
            elif actionType == "open":
                filename, _filter = QtWidgets.QFileDialog.getOpenFileName(ui, "Open..." + "{0} File".format(extension), '.', fileType)
            if filename:
                output.setText(filename)
                self.updateCommand()
       
        def updateInWindow(self):
           lisica.UpdateCheck().upgrade()
           ui.close()
           lisica.run()
       
        CPUcores = multiprocessing.cpu_count()
        
        # hook up button callbacks
        self.form.pushUpgradeTo.setEnabled(False)
        self.form.radio3D.setChecked(True)
        self.findCurrentVersion()
        if internetOn() == True:
            self.findLatestVersion()
            if str(self.currentVersion) != str(self.latestVersion):
                self.form.labelUpgradeTo.setText("Upgrade of LiSiCA GUI avaliable:")
                self.form.pushUpgradeTo.setEnabled(True)
        
        self.form.label.setOpenExternalLinks(True)
        self.form.lineSave.setText(resultsFolder)
        self.form.spinCores.setMaximum(CPUcores)
        self.form.spinCores.setValue(CPUcores)
        self.form.browseReference.clicked.connect(lambda: browse_filename("open", "mol2", self.form.lineReference))
        self.form.browseTarget.clicked.connect(lambda: browse_filename("open", "mol2", self.form.lineTarget))
        self.form.browseSave.clicked.connect(lambda: browse_filename("save", "", self.form.lineSave))
        self.form.browseResult.clicked.connect(lambda: browse_filename("load", "", self.form.lineResult))
        self.form.radio2D.toggled.connect(self.form.lineConformations.setDisabled)
        self.form.radio3D.toggled.connect(self.form.lineConformations.setEnabled)
        self.form.labelVersion.setText(str(self.currentVersion))
        self.form.buttonGO.clicked.connect(self.submitted)
        self.form.loadResult.clicked.connect(self.loadResults)
        self.form.treeMolecules.selectionModel().selectionChanged.connect(self.showCorr)
        self.form.treeAtoms.selectionModel().selectionChanged.connect(self.showCorrAtoms)
        self.form.pushUpgradeTo.clicked.connect(updateInWindow)
        self.form.radio2D.setChecked(True)
        self.updateCommand()
        return ui
    
    def findCurrentVersion(self):
        if os.path.isfile(versionFile):
            with open(versionFile, 'r') as myFile:
                lines = myFile.readlines()
                if len(lines) == 1:
                    self.currentVersion = lines[0].strip()
                else:
                    date = lines[len(lines)-1].split('\t')[2]
                    t = time.strftime("%d/%m/%Y")

                    tmpTime1 = date.split('/')
                    tmpTime2 = t.split('/')
                    if int(tmpTime2[2]) > int(tmpTime1[2]) or int(tmpTime2[1]) > int(tmpTime1[1]) or int(tmpTime2[0]) > int(tmpTime1[0])+15:
                        self.currentVersion = lines[0].strip()
                    else:
                        self.currentVersion = (lines[len(lines)-1].split('\t')[0]).strip()
                
    def findLatestVersion(self):
        try:
            # from insilab git server
            versionFile = urllib.request.urlopen(self.firstVersionURL, timeout=5)
            self.latestVersion = versionFile.read().decode(encoding="utf-8").strip()
            if len(self.latestVersion) > 6:
                self.latestVersion = ""
        except:
            print("Couldn't find latest version")
        
    def openWebsite(self,event):
        import webbrowser
        try:
            webbrowser.open_new(r"http://www.insilab.org/lisica/")
        except:
            print ("Error. Could not open the Website")

#######################################################################################################################


def main():
    
    deleteOldFiles(logFolder)
    if os.path.isfile(os.path.join(lisicaFolder,'lisica.log')):
        open(os.path.join(lisicaFolder,'lisica.log'),"w").close()
    
    log.info("*******************")
    log.info("LiSiCA: version 1.0") 
    log.info("Plugin_timestamp: '%s'" %timestamp.strftime("%Y-%m-%d %H: %M :%S"))
    
    GUI().initUI()