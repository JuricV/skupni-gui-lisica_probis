Installation:
1. Move .dummy directory to your home directory
2. Install Dummy.py by runnning Pymol, opening Plugin Manager > Plugin > Install New Plugin > Choose file... > add Dummy.py
3. Your Dummy plugin should now run normally, you can run it on the Plugin tab > Dummy plugin