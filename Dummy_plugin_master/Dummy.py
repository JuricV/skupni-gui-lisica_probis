# -*- coding: utf-8 -*-
"""
Created on Sun Jan 10 18:11:49 2021

@author: Vito Juric
"""
from __future__ import absolute_import
from __future__ import print_function
import os

HOME_DIRECTORY=os.path.expanduser('~')
DUMMY_DIRECTORY=os.path.join(HOME_DIRECTORY,".dummy")
PDB_DIRECTORY=os.path.join(DUMMY_DIRECTORY,"PDB")
TEMP_DIRECTORY=os.path.join(DUMMY_DIRECTORY,"Temp")

def __init_plugin__(app=None):
    '''
    Add an entry to the PyMOL "Plugin" menu
    '''
    from pymol.plugins import addmenuitemqt
    addmenuitemqt('Dummy" Plugin', run_plugin_gui)

dialog = None

#Might impliment error message if no internet connection when attempting download

def internetOn():
    import urllib.request, urllib.error, urllib.parse
    try:
        urllib.request.urlopen('http://www.google.com',timeout=1)
        return True
    except:
        pass
    return False

def run_plugin_gui():
    '''
    Open our custom dialog
    '''
    global dialog

    if dialog is None:
        dialog = make_dialog()

    dialog.show()

def make_dialog():
    # entry point to PyMOL's API
    from pymol import cmd

    # pymol.Qt provides the PyQt5 interface, but may support PyQt4
    # and/or PySide as well
    from pymol.Qt import QtWidgets
    from pymol.Qt.utils import loadUi

    #Create new window
    dialog = QtWidgets.QMainWindow()

    # populate the Window from our *.ui file which was created with the Qt Designer
    uifile = os.path.join(DUMMY_DIRECTORY, 'dummy.ui')
    form = loadUi(uifile, dialog)    
    
    def downloadPDB():#Downloads pdb file from rcsb.org and saves it in PDB_DIRECTORY
        import urllib.request
        from urllib.parse import urljoin
        download_domain = "https://files.rcsb.org/download/"
        download_code = str.lower(form.PDBcode_input.text())
        downloaded_file = download_code+".pdb"
        download_file = urljoin(download_domain, downloaded_file)
        save_file = os.path.join(PDB_DIRECTORY, downloaded_file)
        urllib.request.urlretrieve(download_file, save_file)
        
    def downloadPDB_temp():#Downloads pdb file from rcsb.org and saves it in TEMP_DIRECTORY
        import urllib.request
        from urllib.parse import urljoin
        download_domain = "https://files.rcsb.org/download/"
        download_code = str.lower(form.PDBcode_input.text())
        downloaded_file = download_code+".pdb"
        download_file = urljoin(download_domain, downloaded_file)
        save_file_temp = os.path.join(TEMP_DIRECTORY, downloaded_file)
        urllib.request.urlretrieve(download_file, save_file_temp)
        
    def pull():#Reads or downloads, saves and reads .pdb file
        download_code = str.lower(form.PDBcode_input.text())
        downloaded_file = download_code+".pdb"
        loadedPDB= os.path.join(PDB_DIRECTORY, downloaded_file)
        if os.path.isfile(loadedPDB):#If saved
            print("%s loaded from saved files" % download_code)
            pass
        else:
            downloadPDB()#downloads and saves .pdb file in PDB_DIRECTORY
            print("%s downloaded, saved and loaded" % download_code)
        cmd.load(loadedPDB)   
        
    def pull_temp():##Reads or downloads, saves and reads .pdb file then deletes it
        download_code = str.lower(form.PDBcode_input.text())
        downloaded_file = download_code+".pdb"
        loadedPDB= os.path.join(PDB_DIRECTORY, downloaded_file)
        loadedPDB_temp=os.path.join(TEMP_DIRECTORY, downloaded_file)
        if os.path.isfile(loadedPDB):#If saved
            print("%s loaded from saved files" % download_code)
            cmd.load(loadedPDB)
        else:
            downloadPDB_temp()#Temporarily saves .pdb file in TEMP_DIRECTORY
            cmd.load(loadedPDB_temp)
            print("%s downloaded and loaded. Will not be saved!" % download_code)
            if os.path.isfile(loadedPDB_temp):#Deletes file if in TEMP_DIRECTORY
                os.remove(loadedPDB_temp)
            else:
                print("Error: %s.pdb file not found for deletion" % download_code)
    
    def online_if_save():
        if form.SaveCheck.isChecked():
            pull()
        else:
            pull_temp()
    
    def offline_if_save():
        download_code = str.lower(form.PDBcode_input.text())
        downloaded_file = download_code+".pdb"
        loadedPDB=os.path.join(PDB_DIRECTORY, downloaded_file)
        if os.path.isfile(loadedPDB):#If saved
            print("%s loaded from saved files" % download_code)
            cmd.load(loadedPDB)
        else:
            print("Internet connection offline, can only run .pdb files saved in directory %s" % PDB_DIRECTORY)
    
    def run():
        if internetOn():
            online_if_save()
        else:
            offline_if_save()
    
    # Hook up buttons
    form.SaveDirectory.setText(DUMMY_DIRECTORY)
    form.SaveDirectory.setReadOnly(True)
    form.SaveDirectory.setDisabled(True)
    form.PDBcode_push.clicked.connect(lambda: run())

    print("Starting dummy...")
    return dialog


