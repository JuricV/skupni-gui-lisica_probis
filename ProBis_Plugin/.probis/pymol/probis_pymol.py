from itertools import chain
from pymol import cmd, CmdException
import urllib.request, urllib.error, urllib.parse
from urllib.request import urlopen
from urllib.error import URLError, HTTPError
import json, os
from os.path import join
import glob
import zipfile
import subprocess
import multiprocessing
from pathlib import Path
from time import sleep
from threading import Thread
import queue
import platform
import webbrowser
import sys
import re
import shutil
import pickle
import random
import collections
import pickle as cpickle
import csv
import stat

from pymol.Qt import QtWidgets, QtGui, QtCore
from pymol.Qt.utils import loadUi


install_dir = join(Path.home(), ".probis_installdir.txt")
install_datoteka = open(install_dir, "r")
probisliteDir=install_datoteka.readline()
install_datoteka.close()
databaseFile = join(probisliteDir, "databaseLocation.txt")
ui_directory = join(probisliteDir, "UI")
default_results_directory = join(probisliteDir, "results")

colors = ["red", "green", "blue", "yellow", "orange", "violet", "cyan", "pink", "gray", "magenta"]
ligandcolors = ['lightblue', 'lightorange', 'lightteal', 'limegreen', 'lithium', 'lightmagenta', 'lightpink', 'lime', 'limon']

sys.path.append(probisliteDir)

def color_carbons(color, selection):
    """Colors only carbons in the given selection."""
    cmd.color(color, '%s and symbol c' % (selection))


class FuncThread(Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        Thread.__init__(self)

    def run(self):
        self._target(*self._args)


class RunInThread(Thread):

    def __init__(self, ProbisGUI, command, what, pdbID, chainID, compareChainID, queue):

        Thread.__init__(self)
        self.command = command
        self.what = what
        self.pdbID = pdbID
        self.chainID = chainID
        self.ProbisGUI = ProbisGUI
        self.queue = queue
        self.compareChainID = compareChainID
            
    def run(self):
        if self.what == "probis":
            print("Started ProBiS alignment for " + self.pdbID + ", query: " + self.compareChainID + " and ProBiS binding site prediction for " + self.pdbID + ", chain ", self.chainID)

            working.BarWorking.setValue(0)
            working.BarWorking.setFormat("Setting up necessary files")
            working.BarWorking.setTextVisible(True)

            self.database = dialog.LineDatabase.text()
            if os.path.isdir(self.database) == False or self.database == "":
                QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Please chose a valid existing database location")

            self.srfDir = join(probisliteDir, "DBsrfs")
            if not os.path.isdir(self.srfDir):
                os.mkdir(self.srfDir)
            self.analized = dialog.LinePathProtein.text()
            self.srf = join(self.srfDir, "target.srf")
            self.BindingSiteCheck = dialog.CheckBSite.isChecked()
            srfsList = []
            
            if self.BindingSiteCheck == True:
                # if not os.path.isfile (join(dialog.LineSrf.text(), "bslib.txt")):
                #     with open(join(dialog.LineSrf.text(), "bslib.txt"), "w+") as f:
                if not os.path.isfile (join(dialog.LineDatabase.text(), "bslib.txt")):
                    with open(join(dialog.LineDatabase.text(), "bslib.txt"), "w+") as f:
                        fileList = []
                        # for srf in glob.glob(join(dialog.LineSrf.text(), "*.srf")):
                        for srf in glob.glob(join(dialog.LineDatabase.text(), "*.srf")):
                            newFile = (r'"' + srf + r'" ' + srf[-5] + "\n")
                            if not newFile in fileList:
                                fileList.append(newFile)
                        f.writelines(fileList)

            if not os.path.isfile(join(self.database, "database.txt")):
                chainList = []
                for file in glob.glob(join(self.database, "*.pdb")):
                    with open(file) as pdb:
                        lines = pdb.readlines()
                        chains = []
                        for line in lines:
                            if line.startswith("ATOM"):
                                chain = line[21]
                                if not chain in chains:
                                    chains.append(chain)
                    for chain in chains:
                        fullChain = (os.path.split(file)[1][0:-4] + chain + "\n")
                        if not fullChain in chainList:
                            chainList.append(fullChain)

                with open(join(self.database, "database.txt"), "w") as DB:
                    DB.writelines(sorted(chainList))

            sys = platform.system()
            if sys == "Windows":
                info = subprocess.STARTUPINFO()
                info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
                info.wShowWindow = subprocess.SW_HIDE
                self.probis_dir = join(probisliteDir, "bin", "64", "probis.exe")
            else:
                self.probis_dir = join(probisliteDir, "bin", "probis")

            working.BarWorking.setFormat("%p%")

            ############# Create srfs files for Database

            with open(join(self.database, "database.txt"), "r") as DB:
                lines = DB.readlines()
                working.BarWorking.setMaximum(len(lines)*2)
                for line in lines:
                    pdb = join(self.database, (line[0:4] + ".pdb"))
                    chain = line[4:5]
                    srf = join(self.srfDir, (line[0:5] + ".srf"))
                    if not os.path.isfile(srf):
                        if sys == "Windows":
                            step0command = [self.probis_dir, "-extract",
                                            "-ncpu", str(dialog.SpinNCPU.value()),
                                            "-f1", pdb,
                                            "-c1", chain,
                                            "-srffile", srf]
                            self.ProbisGUI.probisProcess = subprocess.run(step0command, shell=False, startupinfo=info)

                        else:
                            step0command = [self.probis_dir, "-extract",
                                            "-ncpu", str(dialog.SpinNCPU.value()),
                                            "-f1", pdb,
                                            "-c1", chain,
                                            "-srffile", srf]
                            self.ProbisGUI.probisProcess = subprocess.run(step0command, shell=False)
                    if os.path.isfile(srf):
                        newSrf = (r'"' + srf + r'" ' + chain + "\n")
                        srfsList.append(newSrf)
                    working.BarWorking.setValue(working.BarWorking.value()+1)

            for file in glob.glob(join(self.database, "*.srf")):
                srfsList.append(r'"' + file + r'" ' + file[-5] + "\n")
            for file in glob.glob(join(self.database, "*.srf.gz")):
                srfsList.append(r'"' + file + r'" ' + file[-8] + "\n")

            srfsFile = join(self.srfDir, "srfsList.txt")

            with open(srfsFile, "w") as srfs:
                srfs.writelines(sorted(srfsList))

            ################################################################################## Create target protein srf
            
            if dialog.ComboAlign.currentText() == "Chain":
                self.chains = dialog.LineAlign.text()
                step1command = [self.probis_dir, "-extract",
                                "-f1", self.analized,
                                "-c1", self.chains,
                                "-srffile", self.srf]

            elif dialog.ComboAlign.currentText() == "Binding Site":
                self.chains = dialog.LineAlign.text().strip()[:-1]
                step1command = [self.probis_dir, "-extract",
                                "-bsite", dialog.LineAlign.text(),
                                "-dist", "3",# Add possibility of custom setting
                                "-f1",  self.analized,
                                "-c1", self.chains,
                                "-srffile", self.srf]
            else: #Residues
                def findall(p, s):
                    i = s.find(p)
                    while i != -1:
                        yield i
                        i = s.find(p, i + 1)
                chainsList = []
                for i in findall(":", dialog.LineAlign.text()):
                    chainsList.append(dialog.LineAlign.text()[i + 1])
                self.chains = "".expand(chainsList)
                step1command = [self.probis_dir, "-extract",
                                "-motif", dialog.LineAlign.text(),
                                "-f1", self.analized,
                                "-c1", self.chains,
                                "-srffile", self.srf]
            if sys == "Windows":
                self.ProbisGUI.probisProcess = subprocess.run(step1command, shell=False, startupinfo=info)
            else:
                self.ProbisGUI.probisProcess = subprocess.run(step1command, shell=False)


            analizedName = (os.path.split(self.pdbID)[1].split(".")[0] + self.chains)
            analizedNosql = (analizedName + ".nosql")
            analizedJson = (analizedName + ".json")
            self.resultsDir = dialog.LineResults.text()

            if not os.path.isdir(self.resultsDir):
                os.mkdir(self.resultsDir)

            if not os.path.isdir(join(self.resultsDir, analizedName)):
                os.mkdir(join(self.resultsDir, analizedName))


            if dialog.CheckLocal.isChecked() == True:
                step2command = [self.probis_dir,
                               "-ncpu", str(dialog.SpinNCPU.value()),
                               "-surfdb", "-local",
                               "-sfile", join(self.srfDir, "srfsList.txt"),
                               "-f1", self.srf,
                               "-c1", self.chains,
                               "-nosql", join(self.resultsDir,
                                              analizedName, analizedNosql),
                               "-longnames"]
            else:
                step2command = [self.probis_dir,
                                "-ncpu", str(dialog.SpinNCPU.value()),
                                "-surfdb",
                                "-sfile", join(self.srfDir, "srfsList.txt"),
                                "-f1", self.srf,
                                "-c1", self.chains,
                                "-nosql", join(self.resultsDir,
                                               analizedName, analizedNosql),
                                "-longnames"]

            step3command = [self.probis_dir, "-results",
                            "-f1", self.analized,
                            "-c1", self.chains,
                            "-nosql", join(self.resultsDir,
                                               analizedName, analizedNosql),
                            "-json", join(self.resultsDir,
                                               analizedName, analizedJson),
                            "-longnames"]

            if sys == "Windows":
                self.ProbisGUI.probisProcess = subprocess.run(step2command, shell=False, startupinfo=info)
                working.BarWorking.setValue(working.BarWorking.maximum() * 0.8)
                self.ProbisGUI.probisProcess = subprocess.run(step3command, shell=False, startupinfo=info)
                working.BarWorking.setValue(working.BarWorking.maximum()*0.9)
            else:
                self.ProbisGUI.probisProcess = subprocess.run(step2command, shell=False)
                working.BarWorking.setValue(working.BarWorking.maximum() * 0.8)
                self.ProbisGUI.probisProcess = subprocess.run(step3command, shell=False)
                working.BarWorking.setValue(working.BarWorking.maximum()*0.9)

            shutil.copyfile(join(self.resultsDir, analizedName, analizedJson),
                            join(self.resultsDir, "result.json"))
            
            if self.BindingSiteCheck == True:
                # Predict binding sites
                if sys == "Windows":
                    self.ProbisGUI.probisProcess = subprocess.run(self.command, shell=False, startupinfo=info)
                    working.BarWorking.setValue(working.BarWorking.maximum() * 0.99)
                else:
                    self.ProbisGUI.probisProcess = subprocess.run(self.command, shell=False)
                    working.BarWorking.setValue(working.BarWorking.maximum() * 0.99)
    
            for file in glob.glob(join(self.srfDir, "*.srf")):
                os.remove(file)
            
            try:
                os.remove(join(self.srfDir, "srfsList.txt"))
            except:
                pass
            self.queue.put("done")
            working.BarWorking.setValue(working.BarWorking.maximum())
            # working.close()


class ProbisGUI(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.queue = queue.Queue()
        self.queryProtein = ""
        self.queryChain = ""
        self.queryCompareChain = ""
        self.BSiteFind = False
        self.ligandselection = []
        # self.selectedBsites = []

        cmd.set("auto_zoom", "off")

        self.run()


    def run(self):
        global dialog
        dialog = self.make_dialog()
        dialog.show()


    def make_dialog(self):
        global dialog
        # create a new Window
        dialog = QtWidgets.QMainWindow()
        # populate the Window
        uifile = join(ui_directory, 'ProbisMain.ui')
        self.form = loadUi(uifile, dialog)

        self.form.ButtonLoadSearch.clicked.connect(lambda: self.searchPathFile(1))
        self.form.ButtonLoadLoad.clicked.connect(self.loadProjectFromFile)
        self.form.ButtonSave.clicked.connect(self.open_save_window)
        self.form.ButtonExportSearch.clicked.connect(lambda: self.searchPathFile(2))
        self.form.ButtonExportExport.clicked.connect(self.exportProjectToFile)
        self.form.ButtonExit.clicked.connect(lambda: self.closeEvent(dialog.close))
        self.form.PushBio.clicked.connect(lambda: self.searchPathDir(2))
        self.form.PushDatabase.clicked.connect(lambda: self.searchPathDir(3))
        # self.form.PushSrf.clicked.connect(lambda: self.searchPathDir(4))
        self.form.PushResults.clicked.connect(lambda: self.searchPathDir(5))
        self.form.ButtonSearchProtein.clicked.connect(lambda: self.searchPathFile(0))
        self.form.ButtonStartProbis.clicked.connect(self.customProtein)
        self.form.pushUpgradeTo.clicked.connect(lambda: self.updateProgramVersion(latestVersion))
        self.form.Tree1.selectionModel().selectionChanged.connect(self.multiListBox1Select)
        self.form.Tree2.selectionModel().selectionChanged.connect(self.multiListBox2Select)
        self.form.TreeProt.selectionModel().selectionChanged.connect(self.proteinBoxSelect)
        self.form.CheckBSite.stateChanged.connect(self.changeCheckBSite)

        # self.form.LineChainID.textEdited.connect(lambda: self.upperCaseChain(2))
        self.form.LineAlign.textEdited.connect(lambda: self.upperCaseChain(3))
        self.form.ComboAlign.currentIndexChanged.connect(self.changeExample)
        self.form.label.mousePressEvent = self.open_url
        self.form.LineResults.setText(default_results_directory)

        self.findCurrentVersion()
        self.findLatestVersion()

        self.form.labelVersion.setText(currentVersion)
        self.form.SpinNCPU.setMaximum(multiprocessing.cpu_count())
        self.form.SpinNCPU.setValue(multiprocessing.cpu_count())
        self.form.LabelExample.setText("You may give one or multiple chains, e.g., 'A' or 'ABC'")

        self.form.LineBio.setEnabled(False)
        # self.form.LineSrf.setEnabled(False)
        self.form.PushBio.setEnabled(False)
        # self.form.PushSrf.setEnabled(False)
        # self.form.LineChainID.setEnabled(False)
        self.form.SpinZ.setEnabled(False)
        self.form.SpinCentroid.setEnabled(False)
        self.form.SpinCluster.setEnabled(False)
        self.form.SpinInteratomic.setEnabled(False)
        self.form.SpinExcluded.setEnabled(False)
        self.form.SpinGrid.setEnabled(False)
        self.form.SpinBsites.setEnabled(False)
        self.form.SpinMin.setEnabled(False)
        self.form.SpinCutoff.setEnabled(False)
            
        if not latestVersion == "":
            if latestVersion != currentVersion:
                self.form.labelUpgradeTo.setText("New ProBiS Plugin version avaliable:")
                upgradeStr = ("Upgrade to: " + latestVersion)
                self.form.pushUpgradeTo.setEnabled(True)
                self.form.pushUpgradeTo.setText(upgradeStr)
            else:
                latestStr = ("Latest Version: " + latestVersion)
                self.form.labelUpgradeTo.setText(latestStr)
                self.form.pushUpgradeTo.setText("No upgrade avaliable")
                self.form.pushUpgradeTo.setEnabled(False)
        else:
            self.form.labelUpgradeTo.setText("Can not check for newer version!")
            self.form.pushUpgradeTo.setText("No upgrade avaliable")
            self.form.pushUpgradeTo.setEnabled(False)

        self.checkPermissions()
        self.changeCheckBSite()

        return dialog

    def changeExample(self):
        if dialog.ComboAlign.currentIndex() == 0:
            self.form.LabelExample.setText("You may give one ormultiple chains, e.g., 'A' or 'ABC'")
        elif dialog.ComboAlign.currentIndex() == 1:
            self.form.LabelExample.setText(
                "For example: ‘ATP.305.A’ - ATP(residue name), 305(residue number), A(chain id)")
        else:
            self.form.LabelExample.setText(
                "To select some residues on chains A and B of the input protein, use '[:A and (14,57,69-71) or :B and (33,34,50)]'")
    
    def changeCheckBSite(self):
        if dialog.CheckBSite.isChecked() == False:
            dialog.LineBio.setEnabled(False)
            # dialog.LineSrf.setEnabled(False)
            dialog.PushBio.setEnabled(False)
            # dialog.PushSrf.setEnabled(False)
            # dialog.LineChainID.setEnabled(False)
            dialog.SpinZ.setEnabled(False)
            dialog.SpinCentroid.setEnabled(False)
            dialog.SpinCluster.setEnabled(False)
            dialog.SpinInteratomic.setEnabled(False)
            dialog.SpinExcluded.setEnabled(False)
            dialog.SpinGrid.setEnabled(False)
            dialog.SpinBsites.setEnabled(False)
            dialog.SpinMin.setEnabled(False)
            dialog.SpinCutoff.setEnabled(False)
            dialog.ComboAlign.setEnabled(True)
            dialog.LabelDatabase.setText("Directory for proteins to align:")
            dialog.LabelDatabase.setToolTip("A directory with *.pdb or *.srf protein files for alignment.")
        else:
            dialog.LineBio.setEnabled(True)
            # dialog.LineSrf.setEnabled(True)
            dialog.PushBio.setEnabled(True)
            # dialog.PushSrf.setEnabled(True)
            # dialog.LineChainID.setEnabled(True)
            dialog.SpinZ.setEnabled(True)
            dialog.SpinCentroid.setEnabled(True)
            dialog.SpinCluster.setEnabled(True)
            dialog.SpinInteratomic.setEnabled(True)
            dialog.SpinExcluded.setEnabled(True)
            dialog.SpinGrid.setEnabled(True)
            dialog.SpinBsites.setEnabled(True)
            dialog.SpinMin.setEnabled(True)
            dialog.SpinCutoff.setEnabled(True)
            dialog.ComboAlign.setCurrentIndex(0)
            dialog.ComboAlign.setEnabled(False)
            dialog.LabelDatabase.setText("Directory of Srf files:")
            dialog.LabelDatabase.setToolTip("A directory with *.srf protein files. !!!NEEDS TO BE COMPLEMENTARY WITH SELECTED BIO FILES!!!")
            dialog.LabelBio.setToolTip("A directory with *.bio protein files. !!!NEEDS TO BE COMPLEMENTARY WITH SELECTED SRF FILES!!!")
        

    def closeEvent(self, event):
        # alert if exit while probis is still working
        QMessageBox = QtWidgets.QMessageBox
        if hasattr(ProbisGUI, 'probisProcess'):
            if ProbisGUI.probisProcess.poll() is None:
                close = QMessageBox()
                close.setText(
                    "ProBiS is still working. Calculation of binding sites will be terminated.\n \nExit anyway?")
                close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
                close = close.exec()

                if close == QMessageBox.Yes:
                    ProbisGUI.probisProcess.kill()
                    ProbisGUI.queue.put(-1)
                    print("ProBiS: normal program termination")
                    event()
            else:
                print("ProBiS: normal program termination")
                event()

        # alert if exit while still dl
        elif hasattr(ProbisGUI, 'dlInProgress'):
            if self.dlInProgress == 1:
                close = QMessageBox()
                close.setText("ProBiS database is still downloading. \n \nExit anyway?")
                close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
                close = close.exec()
                if close == QMessageBox.Yes:
                    if hasattr(ProbisGUI, 'running'):
                        if ProbisGUI.running == 1:
                            ProbisGUI.running = 0
                    ProbisGUI.resetWorkspace(False)
                    print("ProBiS: normal program termination")
                    event()

            elif ProbisGUI.dlInProgress == 2:
                close = QMessageBox()
                close.setText("ProBiS database is still in extraction process. \n \nExit anyway?")
                close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
                close = close.exec()
                if close == QMessageBox.Yes:
                    if hasattr(ProbisGUI, 'running'):
                        if ProbisGUI.running == 1:
                            self.running = 0
                    ProbisGUI.resetWorkspace(False)
                    print("ProBiS: normal program termination")
                    event()
            else:
                close = QMessageBox()
                close.setText("Exit ProBiS plugin?")
                close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
                close = close.exec()
                if close == QMessageBox.Yes:
                    print("ProBiS: normal program termination")
                    ProbisGUI.resetWorkspace(False)
                    event()

        else:
            close = QMessageBox()
            close.setText("Exit ProBiS plugin?")
            close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
            close = close.exec()
            if close == QMessageBox.Yes:
                print("ProBiS: normal program termination")
                ProbisGUI.resetWorkspace(False)
                event()
            else:
                print("Not terminated")


    def upperCaseChain(self, num):
        if num == 2:
            # dialog.LineChainID.setText(dialog.LineChainID.text().upper())
            dialog.LineAlign.setText(dialog.LineAlign.text().upper())
        elif num == 3 and dialog.ComboAlign.currentText() != "Residue":
            dialog.LineAlign.setText(dialog.LineAlign.text().upper())


    def LivedbChange(self):
        if dialog.RadioInstallProbis.isChecked():
            dialog.LabelInstallation.setText("Set install directory:")
            dialog.ButtonNext.setText("Install")
        elif dialog.RadioIsInstalled.isChecked():
            dialog.LabelInstallation.setText("Set ProBiS database directory:")
            dialog.ButtonNext.setText("Next >>")


    def open_save_window(self):
        global savewin
        savewin = QtWidgets.QDialog()
        saveuifile = join(ui_directory, "ProbisSave.ui")
        self.savewin = loadUi(saveuifile, savewin)

        self.savewin.PushSave.clicked.connect(lambda: self.searchPathFile(3))
        self.savewin.Buttons.accepted.connect(self.exportMoleculesFromPymol)
        self.savewin.Buttons.rejected.connect(savewin.close)

        self.savewin.CheckLig.stateChanged.connect(self.applyFilterSelection)
        self.savewin.CheckBind.stateChanged.connect(self.applyFilterSelection)
        self.savewin.CheckAlign.stateChanged.connect(self.applyFilterSelection)


        self.applyFilterSelection(True)

        savewin.show()


    def open_db_window(self):
        global DBwin
        DBwin = QtWidgets.QDialog()
        dbuifile = join(ui_directory, "ProbisDatabase.ui")
        self.db = loadUi(dbuifile, DBwin)
        DBwin.show()


    def open_working_window(self):
        global working
        working = QtWidgets.QDialog()
        dbuifile = join(ui_directory, "ProbisWorking.ui")
        self.work = loadUi(dbuifile, working)

        self.work.ButtonCancelProbis.clicked.connect(self.cancelProbis)

        working.show()

    def checkPermissions(self):
        if "linux" in sys.platform:
            if os.path.isfile(join(probisliteDir, "bin", "probis")) == False:
                QtWidgets.QMessageBox.about(dialog, "ProBiS Warning",
                                            "Please ensure probis executable is downloaded correctly in your /.probis/bin/ file.")
                print("ProBiS: please ensure probis executable is downloaded correctly in your /.probis/bin/ file.")
            else:
                try:
                    if os.access(join(probisliteDir, "bin", "probis"), os.X_OK) == True:
                        print("probis executable ok")
                    else:
                        st = os.stat(join(probisliteDir, "bin", "probis"))
                        os.chmod(join(probisliteDir, "bin", "probis"), st.st_mode | stat.S_IEXEC)
                        print("probis executable permission set")
                        print("probis directory:      : ", join(probisliteDir, "bin", "probis"))
                except:
                    print("Skipped checking for probis")
                    pass

            if os.path.isfile(join(probisliteDir, "bin", "probislite")) == False:
                QtWidgets.QMessageBox.about(dialog, "ProBiS Warning",
                                            "Please ensure probislite executable is downloaded correctly in your /.probis/bin/ file.")
                print("ProBiS: please ensure probislite executable is downloaded correctly in your /.probis/bin/ file.")
            else:
                try:
                    if os.access(join(probisliteDir, "bin", "probislite"), os.X_OK) == True:
                        print("probislite executable ok")
                    else:
                        st = os.stat(join(probisliteDir, "bin", "probislite"))
                        os.chmod(join(probisliteDir, "bin", "probislite"), st.st_mode | stat.S_IEXEC)
                        print("probislite executable permission set")
                        print("probislite directory:      : ", join(probisliteDir, "bin", "probislite"))
                except:
                    print("Skipped checking for probislite")
                    pass
        elif "win32" in sys.platform:
            if os.path.isfile(join(probisliteDir, "bin", "64", "probis.exe")) == False or os.path.isfile(join(probisliteDir, "bin",
                                                                                                   "64",
                                                                                                   "probislite.exe")) == False:
                QtWidgets.QMessageBox.about(None, "ProBiS Warning",
                                            "Please ensure probis.exe and probislite.exe are downloaded correctly in your in your /.probis/bin/64/ file.")
                print(
                    r"ProBiS_H2O: please ensure probis.exe and probislite.exe are downloaded correctly in your /.probis/bin/64/ file")
            else:
                print("probis.exe and probislite.exe are present")


    def createCustomFrame(self):
        status = self.customProteinFrameStatus
        if status == "databaseInput" or status == "databaseInput2":
            dialog.tabWidget.setCurrentIndex(0)
        elif status == "probisWorking":
            self.open_working_window()
        elif status == "databaseWorking":
            self.open_db_window()
        else:
            dialog.tabWidget.setCurrentIndex(0)
            # dialog.tabWidgetInput.setCurrentIndex(1)

    #
    # update progress
    # 

    def poll(self):
        DBwin.BarDL.setRange(0, 1771851923.0)
        if self.queue.qsize():
            try:
                msg = self.queue.get(block = True)
                if msg == "Done":
                    self.queue.queue.clear()
                print("Downloaded ", msg, "kb so far")
                DBwin.BarDL.setValue(msg)

            except Exception:
                pass

        if self.running == 1:
            QtCore.QTimer.singleShot(200, self.poll)

        elif self.running == 0:
            QtWidgets.QMessageBox.about(dialog, "ProBiS", "Installation of ProBiS database succeeded.")
            DBwin.close()
            self.customProteinFrameStatus = "probisInput"
            self.createCustomFrame()

    #
    # start probis for custom protein
    #

    def runProbis (self, pdbID, chainID):
        if self.queue.qsize():
            if self.queue.get() != -1:
                with open(self.proteinLocation, "r") as myfile:
                    protein = myfile.read()
                    cmd.read_pdbstr(protein, pdbID)
                    if chainID:
                        cmd.create(pdbID + chainID, pdbID + " and c. " + "+".join(chainID))
                        cmd.delete(pdbID)
                        cmd.zoom(pdbID + chainID)
                        cmd.remove("hetatm in " + pdbID + chainID)
                    else:
                        cmd.zoom(pdbID)
                        cmd.remove("hetatm in " + pdbID + chainID)
                
                if dialog.CheckBSite.isChecked() == True:
                    #add grids
                    self.pluginResult = {}
                    with open (join(self.resultsLocation, "grid.json"), "r") as f:
                        gridData = json.loads(f.read())
                        lines=[]
                        for i in gridData:
                            if not i['bs_id'] in self.pluginResult:
                                self.pluginResult[i['bs_id']] = {}
                                self.pluginResult[i['bs_id']]['bs_id'] = i['bs_id']
                                self.pluginResult[i['bs_id']]['types'] = []
                                self.pluginResult[i['bs_id']]['ligands'] = {}
                                if len(lines) > 0:
                                    lines.append("ENDMDL\n")
                            if not i['rest'] in self.pluginResult[i['bs_id']]['types']:
                                self.pluginResult[i['bs_id']]['types'].append(i['rest'])
                            xForm = ("        " + str(i['x']))[-8:]
                            yForm = ("        " + str(i['y']))[-8:]
                            zForm = ("        " + str(i['z']))[-8:]
                            line = "ATOM      1   U  DIK     1    {}{}{}\n".format(xForm, yForm, zForm)
                            lines.append(line)
    
                    grids = "".join(lines).split("ENDMDL")
    
                    for i in self.pluginResult:
                        with open(join(probisliteDir, "data", "grid-"+str(pdbID)+str(chainID)+"_"+str(self.pluginResult[i]['bs_id'])+".pdb"), "w") as tmpfile:
                            tmpfile.write(str(grids[i-1]) + "ENDMDL")
                        self.pluginResult[i]["bs_crd"] = (grids[i-1]+"ENDMDL")
                        padded = (('     ' + str(self.pluginResult[i]['bs_id']))[-5:])
                        TreeWidg = QtWidgets.QTreeWidgetItem(self.pluginResult[i]["bs_id"])
                        TreeWidg.setText(0, padded)
                        TreeWidg.setText(1, str(", ".join(self.pluginResult[i]['types'])))
                        dialog.Tree1.addTopLevelItem(TreeWidg)
    
                    # add ligands
                    with open(join(self.resultsLocation, 'clusters.json'), "r") as f:
                        ligands = []
                        ligandsUnranked = []
                        c = 0
                        data = json.loads(f.read())
                        for i in data:
                            nameItems = [str(i['bs_id']), i['ligand']['comments']['pdb_id'],
                                         i['ligand']['comments']['chain_id'], 
                                         str(i['ligand']['comments']['resi']),
                                         i['ligand']['comments']['resn']]
                            name = "_".join(nameItems)
    
                            item = [i['bs_id'], 
                                    name, 
                                    str(i['z_score']),
                                    i['ligand']['assemblies'][0]['models'][0]['chains'][0]['segments'][0]['residues'][0]['atoms'],
                                    i['ligand']['bonds'],
                                    i['ligand']['comments']]
                            if not item in ligands:
                                ligandsUnranked.append(item)
                        for ligand in sorted(ligandsUnranked, key=lambda z_score: z_score[2], reverse=True):
                            c += 1
                            rank = ("        " + str(c))[-8:]
                            ligand.insert(0, rank)
                            ligands.append(ligand)
                        c=0
                        for ligand in ligands:
                            self.pluginResult[ligand[1]]['ligands'][ligand[2]] = {}
                            self.pluginResult[ligand[1]]['ligands'][ligand[2]]['name'] = ligand[2]
                            self.pluginResult[ligand[1]]['ligands'][ligand[2]]['zscore'] = ligand[3]
                            self.pluginResult[ligand[1]]['ligands'][ligand[2]]['atoms'] = ligand[4]
                            self.pluginResult[ligand[1]]['ligands'][ligand[2]]['bonds'] = ligand[5]
                            self.pluginResult[ligand[1]]['ligands'][ligand[2]]['comments'] = ligand[6]
    
                    with open(join(self.resultsLocation, 'pluginResults.json'), "w") as jamie:
                        json.dump(self.pluginResult, jamie)
                    if not os.path.isdir(join(self.resultsLocation, pdbID+chainID)):
                        os.mkdir(join(self.resultsLocation, pdbID+chainID))
                    shutil.copy2(join(self.resultsLocation, 'pluginResults.json'),
                                      join(self.resultsLocation, pdbID+chainID, 'pluginResults.json'))
    
                    for i in ligands:
                        pdbStrings = []
                        # pdbStrings.append("MODEL        1\n")
                        for atom in i[4]:
                            xCor = ("        " + str(atom['crd'][0]))[-8:]
                            yCor = ("        " + str(atom['crd'][1]))[-8:]
                            zCor = ("        " + str(atom['crd'][2]))[-8:]
                            serialNum = ("     " + str(atom['atom_number']))[-5:]
                            atomName = ("    " + str(atom['atom_name']))[-4:]
                            resName = ("   " + str(i[6]['resn']))[-3:]
                            resNum = ("    " + str(i[6]['resi']))[-4:]
                            pdbStrings.append('HETATM'+serialNum+" "+atomName+" "+resName+" "+i[6]['chain_id']+resNum+"    {}{}{}  1.00  1.00 \n".format(xCor, yCor, zCor))
                        pdbStrings.append("TER\n")
                        try:
                            for bond in i[5]:
                                atom1 = ("     " + str(bond['atom1']))[-5:]
                                atom2 = ("     " + str(bond['atom2']))[-5:]
                                pdbStrings.append("REMARK   8 BONDTYPE "+str(bond['bond_order'])+" "+atom1+atom2+'\n')
                        except:
                            pass
                        pdbStrings.append("ENDMDL\nREMARK   6 END\n")
                        try:
                            for bond in i[5]:
                                atom1 = ("     " + str(bond['atom1']))[-5:]
                                atom2 = ("     " + str(bond['atom2']))[-5:]
                                pdbStrings.append('CONECT'+atom1+atom2+"\n")
                        except:
                            pass
                        with open(join(probisliteDir, "data", "lig-"+i[2]+".pdb"), "w") as tmpfile:
                            tmpfile.writelines(pdbStrings)
    
    
                    dialog.Tree1.sortItems(0, QtCore.Qt.AscendingOrder)
                    cmd.hide('lines', pdbID + chainID)
                    cmd.hide('everything', 'resn HOH') # Hide Waters
                    cmd.show('cartoon', pdbID + chainID)
                    # color_carbons('myblue', pdbID + chainID)
                    cmd.orient()  # Glbal viewpoint
    
    
                    QtWidgets.QMessageBox.about(dialog, "ProBiS", "Done with calculation of binding sites (" + pdbID + ":" + chainID + ").")
                print("Done with ProBiS (" + pdbID + ":" + chainID + ")")
                self.customProteinFrameStatus = "probisInput"
                self.createCustomFrame()
                self.runProtAlign()
                working.close()
                dialog.tabWidget.setCurrentIndex(1)

        else:
            try:
                QtCore.QTimer.singleShot(200, lambda: self.runProbis(pdbID, chainID))

            except:
                pass


    def cancelProbis(self):

        result = QtWidgets.QMessageBox()
        result.setText("Calculation of binding sites will be terminated.\n \nAre you sure?")
        result.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel)
        result = result.exec()

        if result == QtWidgets.QMessageBox.Yes:
            try:
                self.probisProcess.kill()
            except:
                pass
            print("ProBiS job termination.")
            self.customProteinFrameStatus = "probisInput"
            self.createCustomFrame()
            self.queue.put(-1)
            working.close()

    #set view for grids

    def showFancyGrid(self, bsiteid):
        name = "bsite-"+str(bsiteid)
        cmd.hide("everything", name)
        cmd.show("surface", name)
        cmd.set("transparency", 0.5, name)
        cmd.set("solvent_radius", 0.5, name)
        ind = int(bsiteid)
        while ind >= len(colors):
            ind -= len(colors)
        cmd.color(colors[ind], name)
        cmd.zoom(name)

    # check chain id

    def runProtAlign(self):

        dialog.TreeProt.clear()

        with open (join(self.resultsLocation, "result.json"), "r") as results:
            data = json.loads(results.read())

        items = []

        for protein in data:
            for i in protein['alignment']:
                if int(i['scores']['z_score']) >= dialog.SpinZCutoff.value():
                    item = [protein['pdb_id'][:-1], protein['chain_id'], i['scores']['alignment_no'],
                            i['scores']['z_score'], i['scores']['alignment_score']]
                    if not item in items:
                        items.append(item)
                else:
                    break

        num = 0
        TreeItems = []
        for item in sorted(items, key=lambda z_score: z_score[3], reverse=True):
            num += 1
            item.insert(0, str(num))
            paddedNum = ('         ' + str(item[0]))[-9:]
            padded3 = ('         ' + str(item[3]))[-9:]
            padded4 = ('         ' + str(item[4]))[-9:]
            padded5 = ('         ' + str(item[5]))[-9:]

            treeItem = QtWidgets.QTreeWidgetItem(num)
            treeItem.setText(0, paddedNum)
            treeItem.setText(1, item[1])
            treeItem.setText(2, item[2])
            treeItem.setText(3, padded3)
            treeItem.setText(4, padded4)
            treeItem.setText(5, padded5)
            TreeItems.append(treeItem)

        dialog.TreeProt.addTopLevelItems(TreeItems)
        dialog.TreeProt.sortItems(0, QtCore.Qt.AscendingOrder)
        dialog.TreeProt.setSortingEnabled(True)

    # get data for custom protein

    def customProtein(self):

        # chainID = dialog.LineChainID.text()
        chainID = dialog.LineAlign.text()
        pdbID = dialog.LinePathProtein.text()

        if not os.path.isdir(dialog.LineResults.text()):
            try:
                os.mkdir(dialog.LineResults.text())
            except:
                pass

        if dialog.CheckBSite.isChecked() == True:
            statusValues = self.checkCustomInput()
            if pdbID == "":
                QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Please set path to protein location.")
            elif chainID == "":
                QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Please set chain ID.")
            elif statusValues[1] == 0:
                QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Input for " + str(statusValues[0]) + " needs to be a number.")
            elif statusValues[1] == 2:
                QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Input for " + str(statusValues[0]) + " needs to be a POSITIVE number.")
            elif statusValues[1] == 3:
                QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Wrong input for " + str(statusValues[0]) + ". Entered value is not allowed.")

        if os.path.isfile(pdbID) == False:
            QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Protein file "+pdbID+" does not exists.")
            dialog.LinePathProtein.clear()
            dialog.LineAlign.clear()
            # dialog.LineChainID.clear()

        elif os.path.isdir(dialog.LineResults.text()) == False:
            try:
                os.mkdir(dialog.LineResults.text())
            except:
                QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Chosen results directory " + dialog.LineResults.text() + " does not exists.")
                dialog.LineResults.clear()
        
        elif self.resetWorkspace():
            
            if os.path.isfile(join(dialog.LineResults.text(), "grid.json")):
                os.remove(join(dialog.LineResults.text(), "grid.json"))
            if os.path.isfile(join(dialog.LineResults.text(), "clusters.json")):
                os.remove(join(dialog.LineResults.text(), "clusters.json"))
            if os.path.isfile(join(dialog.LineResults.text(), "result.json")):
                os.remove(join(dialog.LineResults.text(), "result.json"))
            if os.path.isfile(join(dialog.LineResults.text(), "pluginResults.json")):
                os.remove(join(dialog.LineResults.text(), "pluginResults.json"))
            if os.path.isfile(join(dialog.LineResults.text(), "grid.pdb")):
                os.remove(join(dialog.LineResults.text(), "grid.pdb"))
                
            pdbID = pdbID[-8:][:4].lower()
            self.queryProtein = pdbID
            self.queryChain = chainID
            self.queryCompareChain = dialog.LineAlign.text()
            self.bio = dialog.LineBio.text()
            # self.srfLocation = dialog.LineSrf.text()
            self.srfLocation = dialog.LineDatabase.text()
            self.resultsLocation = dialog.LineResults.text()
            self.database = dialog.LineDatabase.text()
            self.BSiteFind = dialog.CheckBSite.isChecked()
            
            if dialog.CheckBSite.isChecked() == True:
                if os.path.isdir(self.bio) == False:
                    QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Please chose existing Bio database location!")
                if os.path.isdir(self.srfLocation) == False:
                    QtWidgets.QMessageBox.about(dialog, "ProBiS error!", "Please chose existing Srf database location!")

            dialog.LabelInputed.setText("Query Protein: "+self.queryProtein+", Compare site: "+self.queryCompareChain)

            sys = platform.system()

            ###########################################################################################################
            if sys == "Windows":

                if platform.architecture()[0] == "64bit":
                    windowsExe = "64\probislite.exe"
                else:
                    windowsExe = "32\probislite.exe"
                    
                # bashCommand = [join(probisliteDir, "bin", windowsExe), "--noprobis",
                #                 "--ncpu", str(multiprocessing.cpu_count()),
                #                 "--bslib", join(self.srfLocation, "bslib.txt"),
                #                 "--bio", self.bio,
                #                 "--srf_dir", self.srfLocation,
                #                 "--receptor", dialog.LinePathProtein.text(),
                #                 "--receptor_chain_id", chainID,
                #                 # "--grid_file", join(self.resultsLocation, "grid.json")
                #                 "--gridpdb_hcp", join(self.resultsLocation, "grid.json"),
                #                 "--lig_clus_file", join(self.resultsLocation, "clusters.json"),
                #                 "--json", join(self.resultsLocation, "result.json"),
                #                 "--interatomic", str(float(dialog.SpinInteratomic.value())),
                #                 "--excluded", str(float(dialog.SpinExcluded.value())),
                #                 "--grid", str(float(dialog.SpinGrid.value())),
                #                 "--cutoff", str(int(float(dialog.SpinCutoff.value()))),
                #                 "--centro_clus_rad", str(float(dialog.SpinCentroid.value())),
                #                 "--probis_min_z_score", str(float(dialog.SpinZ.value())),
                #                 "--probis_clus_rad", str(float(dialog.SpinCluster.value())),
                #                 "--probis_min_pts", str(int(float(dialog.SpinMin.value()))),
                #                 ]
                
                bashCommand = [join(probisliteDir, "bin", windowsExe), "--noprobis",
                               "--ncpu", str(multiprocessing.cpu_count()),
                               "--bslib_file", join(self.srfLocation, "bslib.txt"),
                               "--bio_dir", self.bio,
                               "--srf_dir", self.srfLocation,
                               "--receptor_file", dialog.LinePathProtein.text(),
                               "--receptor_chain_id", chainID,
                               "--grid_file", join(self.resultsLocation, "grid.json"),
                               # "--centro_out_file", join(self.resultsLocation, "centro.json"),
                               "--lig_clus_file", join(self.resultsLocation, "clusters.json"),
                               # "--bresi_file", join(self.resultsLocation, "bresi.json"),
                               # "--surf_file", join(self.resultsLocation, "surf.json"),
                               # "--inte_file", join(self.resultsLocation, "inte.json"),
                               # "--evo_file", join(self.resultsLocation, "evo.json"),
                               # "--z_scores_file", join(self.resultsLocation, "zscores.pdb"),
                               # "--nosql_file", join(self.resultsLocation, "probis.nosql"),
                               "--json_file", join(self.resultsLocation, "result.json"),
                               "--max_interatomic_distance", str(float(dialog.SpinInteratomic.value())),
                               "--excluded_radius", str(float(dialog.SpinExcluded.value())),
                               "--grid_spacing", str(float(dialog.SpinGrid.value())),
                               # "--num_bsites", str(int(float(dialog.SpinBsites.value()))),
                               "--dist_cutoff", str(int(float(dialog.SpinCutoff.value()))),
                               "--centro_clus_rad", str(float(dialog.SpinCentroid.value())),
                               "--protein_min_z_score", str(float(dialog.SpinZ.value())),
                               "--protein_clus_rad", str(float(dialog.SpinCluster.value())),
                               "--protein_min_pts", str(int(float(dialog.SpinMin.value()))),
                               # "--superimpose_file", join(self.resultsLocation, "super.json")
                               ]

            else:
                bashCommand = [join(probisliteDir, "bin", "probislite"), "--noprobis",
                               "--ncpu", str(multiprocessing.cpu_count()),
                               "--bslib_file", join(self.srfLocation, "bslib.txt"),
                               "--bio_dir", self.bio,
                               "--srf_dir", self.srfLocation,
                               "--receptor_file", dialog.LinePathProtein.text(),
                               "--receptor_chain_id", chainID,
                               "--grid_file", join(self.resultsLocation, "grid.json"),
                               # "--centro_out_file", join(self.resultsLocation, "centro.json"),
                               "--lig_clus_file", join(self.resultsLocation, "clusters.json"),
                               # "--bresi_file", join(self.resultsLocation, "bresi.json"),
                               # "--surf_file", join(self.resultsLocation, "surf.json"),
                               # "--inte_file", join(self.resultsLocation, "inte.json"),
                               # "--evo_file", join(self.resultsLocation, "evo.json"),
                               # "--z_scores_file", join(self.resultsLocation, "zscores.pdb"),
                               # "--nosql_file", join(self.resultsLocation, "probis.nosql"),
                               "--json_file", join(self.resultsLocation, "result.json"),
                               "--max_interatomic_distance", str(float(dialog.SpinInteratomic.value())),
                               "--excluded_radius", str(float(dialog.SpinExcluded.value())),
                               "--grid_spacing", str(float(dialog.SpinGrid.value())),
                               # "--num_bsites", str(int(float(dialog.SpinBsites.value()))),
                               "--dist_cutoff", str(int(float(dialog.SpinCutoff.value()))),
                               "--centro_clus_rad", str(float(dialog.SpinCentroid.value())),
                               "--protein_min_z_score", str(float(dialog.SpinZ.value())),
                               "--protein_clus_rad", str(float(dialog.SpinCluster.value())),
                               "--protein_min_pts", str(int(float(dialog.SpinMin.value()))),
                               # "--superimpose_file", join(self.resultsLocation, "super.json")
                               ]

            self.proteinLocation = dialog.LinePathProtein.text()
            self.customProteinFrameStatus = ("probisWorking")
            self.createCustomFrame()
            self.queue.queue.clear()
            self.runProbis(pdbID, chainID)
            self.t = RunInThread(self, bashCommand, "probis", pdbID, chainID, self.queryCompareChain, self.queue)
            self.t.daemon = True
            self.t.start()

    # check if z_score is float

    def checkCustomInput(self):

        values = {"ProBiS Z-score": dialog.SpinZ,
                  "Cluster Radius": dialog.SpinCluster,
                  "Centroid Radius": dialog.SpinCentroid,
                  "Interatomic": dialog.SpinInteratomic,
                  "Excluded Radius": dialog.SpinExcluded,
                  "Grid Spacing": dialog.SpinGrid,
                  "Number of Sites": dialog.SpinBsites,
                  "Number of Points": dialog.SpinMin,
                  "Cutoff Length": dialog.SpinCutoff}

        defaultVals = {"ProBiS Z-score": "2.5",
                       "Cluster Radius": "3.0",
                       "Centroid Radius": "3.0",
                       "Interatomic": "0.8",
                       "Excluded Radius": "0.8",
                       "Grid Spacing": "0.5",
                       "Number of Sites": "3",
                       "Number of Points": "10",
                       "Cutoff Length": "6" }


        for key, vrednost in values.items():
            try:
                val = float(vrednost.value())
                if "Number" in key:
                    val = int(val)
                if "Cutoff" in key:
                    val = int(val)
                    if val not in [4, 5,6,7,8,9.10,11,12,13,14.15]:
                        return (key, 3)

                if val <= 0:
                    vrednost.setValue(defaultVals[key])
                    return (key, 2)
            except ValueError:
                vrednost.setValue(defaultVals[key])
                return (key, 0)

        return ("ok", 1)


    def open_url(self, e):
        try:
            webbrowser.open_new(r"http://insilab.org/probis-plugin")
        except:
            print("Error: Could not open the website.")

    # update program version

    def updateProgramVersion(self, newVersion):
        #shutil.rmtree(probisliteDir)
        versionFile = join(probisliteDir,"version.txt")
        with open(versionFile, 'w') as myfile:
            myfile.write("UPDATE NOW")
        QtWidgets.QMessageBox.about(dialog, "Update", "Please restart probis.")
        dialog.close()

    # search for location of probis database

    def searchPathDir(self, num):
        dirname = QtWidgets.QFileDialog.getExistingDirectory(dialog, "Choose directory")
        if num == 1:
            dialog.LineCustom.setText(dirname)
        elif num == 2:
            dialog.LineBio.setText(dirname)
        elif num == 3:
            dialog.LineDatabase.setText(dirname)
        elif num == 4:
            # dialog.LineSrf.setText(dirname)
            dialog.LineDatabase.setText(dirname)
        elif num == 5:
            dialog.LineResults.setText(dirname)
    # search for location

    def searchPathFile(self, status):
        # custom protein file
        if status == 0:
            filename, _filter = QtWidgets.QFileDialog.getOpenFileName(dialog, "Open..." + "PDB files", ".pdb")
            dialog.LinePathProtein.setText(filename)

        # load project file
        elif status == 1:
            filename, _filter = QtWidgets.QFileDialog.getOpenFileName(dialog, "Open..." + "Json files", ".json")
            dialog.LineLoad.setText(filename)

        # export project file
        elif status == 2:
            filename, _filter = QtWidgets.QFileDialog.getSaveFileName(dialog, "Choose save file..." + "Probis files", ".json")
            if filename and ".json" not in filename:
                filename += ".json"
            dialog.LineExport.setText(filename)

        # export molecules to file
        elif status == 3:
            filename, _filter = QtWidgets.QFileDialog.getSaveFileName(dialog, "Choose save file..." + "PDB files", ".pdb")
            if filename and ".pdb" not in filename:
                filename += ".pdb"
            savewin.LineSave.setText(filename)

    #
    # check internet connection
    #

    def internetOn(self):
        try:
            urllib.request.urlopen('http://insilab.org',timeout=5)
            return True
        except:
            pass
        return False

    #
    # load project from file
    #

    def loadProjectFromFile(self):
        projectFile = dialog.LineLoad.text()

        if not os.path.isfile(projectFile):
            QtWidgets.QMessageBox.about(dialog, "Export Project", "You need to select a valid file.")
            return

        if self.resetWorkspace() == False:
            return

        self.resultsLocation = join(probisliteDir, "data")
        self.proteinLocation = join(probisliteDir, "data", "query.pdb")

        if os.path.isfile(projectFile):
            with open (projectFile, "r") as importFile:
                data = json.loads(importFile.read())

            self.queryProtein = data['query']['protein']
            self.queryCompareChain = data['query']['compareChain']
            self.database = data['query']['databaseLocation']
            
            if data['query']['BSiteCheck'] == "yes":
                self.BSiteFind = True
            else:
                self.BSiteFind = False
                
            with open(self.proteinLocation, "w") as pdbFile:
                pdbFile.write(data['query']['protein_pdb'])
            with open (join(self.resultsLocation, "result.json"), "w") as resultCompare:
                json.dump(data['compare'], resultCompare)


            if self.BSiteFind == True:
                self.pluginResult = data['lite']
                self.queryChain = data['query']['liteChain']

                for i in self.pluginResult:
                    with open(join(probisliteDir, "data", "grid-" + str(self.queryProtein) + str(self.queryChain) + "_" + str(self.pluginResult[i]['bs_id']) + ".pdb"), "w") as tmpfile:
                        tmpfile.write(self.pluginResult[i]["bs_crd"])
                    padded = (('     ' + str(self.pluginResult[i]['bs_id']))[-5:])
                    TreeWidg = QtWidgets.QTreeWidgetItem(self.pluginResult[i]["bs_id"])
                    TreeWidg.setText(0, padded)
                    TreeWidg.setText(1, str(", ".join(self.pluginResult[i]['types'])))
                    dialog.Tree1.addTopLevelItem(TreeWidg)
    
                for bsite in self.pluginResult:
                    for i in self.pluginResult[bsite]['ligands']:
                        pdbStrings = []
                        model = self.pluginResult[bsite]['ligands'][i]['comments']['model_id']
                        model_id = ("         " + str(model))[-9:]
                        pdbStrings.append("MODEL"+model_id+"\n")
                        comments = self.pluginResult[bsite]['ligands'][i]['comments']
                        chosen_lig = self.pluginResult[bsite]['ligands'][i]
                        for atom in chosen_lig['atoms']:
                            xCor = ("        " + str(atom['crd'][0]))[-8:]
                            yCor = ("        " + str(atom['crd'][1]))[-8:]
                            zCor = ("        " + str(atom['crd'][2]))[-8:]
                            serialNum = ("     " + str(atom['atom_number']))[-5:]
                            atomName = ("    " + str(atom['atom_name']))[-4:]
                            resName = ("   " + str(comments['resn']))[-3:]
                            resNum = ("    " + str(comments['resi']))[-4:]
                            pdbStrings.append('HETATM' + serialNum + " " + atomName + " " + resName + " " + comments['chain_id'] +
                                              resNum + "    {}{}{}  1.00  1.00 \n".format(xCor, yCor, zCor))
                        pdbStrings.append("TER\n")
                        try:
                            for bond in chosen_lig['bonds']:
                                atom1 = ("     " + str(chosen_lig['bonds'][bond]['atom1']))[-5:]
                                atom2 = ("     " + str(chosen_lig['bonds'][bond]['atom2']))[-5:]
                                pdbStrings.append("REMARK   8 BONDTYPE " + str(chosen_lig['bonds'][bond]['bond_order']) +
                                                  " " + atom1 + atom2 + '\n')
                        except:
                            pass
                        pdbStrings.append("ENDMDL\nREMARK   6 END\n")
                        try:
                            for bond in chosen_lig['bonds']:
                                atom1 = ("     " + str(chosen_lig['bonds'][bond]['atom1']))[-5:]
                                atom2 = ("     " + str(chosen_lig['bonds'][bond]['atom2']))[-5:]
                                pdbStrings.append('CONECT' + atom1 + atom2 + "\n")
                        except:
                            pass
                        with open(join(probisliteDir, "data", "lig-" + i + ".pdb"), "w") as tmpfile:
                            tmpfile.writelines(pdbStrings)

                ##### Display query
                with open(self.proteinLocation, "r") as myfile:
                    protein = myfile.read()
                    cmd.read_pdbstr(protein, self.queryProtein)
                    cmd.create(self.queryProtein + self.queryChain,
                               self.queryProtein + " and c. " + "+".join(self.queryChain))
                    cmd.zoom(self.queryProtein + self.queryChain)
                    cmd.remove("hetatm")
                    cmd.delete(self.queryProtein)
    
                dialog.Tree1.sortItems(0, QtCore.Qt.AscendingOrder)
                cmd.hide('lines', self.queryProtein + self.queryChain)
                cmd.hide('everything', 'resn HOH')  # Hide Waters
                cmd.show('cartoon', self.queryProtein + self.queryChain)
                # color_carbons('myblue', self.queryProtein + self.queryChain)
                cmd.orient()  # Glbal viewpoint

                QtWidgets.QMessageBox.about(dialog, "ProBiS",
                                            "Done with calculation of binding sites (" + self.queryProtein+ ":" + self.queryChain + ").")
            print("Done with ProBiS (" + self.queryChain + ":" + self.queryCompareChain + ")")
            self.customProteinFrameStatus = "probisInput"
            self.createCustomFrame()
            self.runProtAlign()
            try:
                working.close()
            except:
                pass
            dialog.tabWidget.setCurrentIndex(1)

        else:
            print("Load Error: "+projectFile+" doesn't exist")
            QtWidgets.QMessageBox.about(dialog, "Load Error", projectFile+" doesn't exist")
            dialog.lineLoad.clear()
            # self.entryProjectFileLocation.delete(0, END)

    #
    # export projcet to file
    #

    def exportProjectToFile(self):
        projectFile = dialog.LineExport.text()

        if projectFile == "":
            QtWidgets.QMessageBox.about(dialog, "Export Project", "You need to select a valid file location.")

        elif not projectFile.endswith(".json"):
            QtWidgets.QMessageBox.about(dialog, "Export Project", "You need to select a valid .json file name.")

        else:
            exportResults = {}
            exportResults['query'] = {}
            exportResults['compare'] = {}
            exportResults['lite'] = {}
            exportResults['query']['protein'] = self.queryProtein
            exportResults['query']['compareChain'] = self.queryCompareChain
            exportResults['query']['databaseLocation'] = self.database
            if self.BSiteFind == True:
                exportResults['query']['BSiteCheck'] = "yes"
                exportResults['query']['liteChain'] = self.queryChain
                exportResults['lite'] = self.pluginResult
            else:
                exportResults['query']['BSiteCheck'] = "no"
                exportResults['query']['liteChain'] = self.queryChain
            
            with open(join(self.resultsLocation, "result.json"), "r") as results:
                exportResults['compare'] = json.loads(results.read())
            with open(self.proteinLocation, "r") as file:
                exportResults['query']['protein_pdb'] = file.read()

            with open (projectFile, "w") as exportFile:
                json.dump(exportResults, exportFile)

            QtWidgets.QMessageBox.about(dialog, "Export Project", ("Project was exported successfully. \nSaved as: "+projectFile))
            dialog.LineExport.clear()
            print("Project exported to file: "+projectFile)


    def proteinBoxSelect(self):
        # cmd.delete("all")
        if not "win" in sys.platform:
            cmd.set("grid_mode", value=0)
        else:
            cmd.set("grid_mode", value=1)
        if not "query" in cmd.get_names("all"):
            cmd.delete("all")
            cmd.load(self.proteinLocation, "prot")
            createStr = ""
            temp = []
            if "." in self.queryCompareChain:
                temp.append(self.queryCompareChain.split(".")[-1])
            elif ":" in self.queryCompareChain:
                for res in self.queryCompareChain.split(":")[1:]:
                    temp.append(res[0])
            else:
                for ch in self.queryCompareChain:
                    temp.append(ch)
            for ch in temp:
                if not createStr == "":
                    createStr = createStr+" or "
                createStr = createStr+("chain " + str(ch))
            createStr = createStr+" in prot"
            cmd.create(('query'), createStr)
            cmd.center('query')
            cmd.delete("prot")

        temp = []
        tempAll = []

        for item in dialog.TreeProt.selectedItems():
            selectedWithChain = item.text(1)+item.text(2).strip(" ")
            fullSelection = selectedWithChain+"_"+item.text(3).strip(" ")
            tempAll.append(fullSelection)
            if fullSelection not in cmd.get_names("all"):
                temp.append(selectedWithChain)
                if os.path.isfile(join(self.database, selectedWithChain+".pdb")):
                    cmd.load(join(self.database, selectedWithChain+".pdb"), fullSelection)
                elif os.path.isfile(join(self.database, item.text(1)+".pdb")):
                    cmd.load(join(self.database, item.text(1) + ".pdb"))
                    cmd.create(fullSelection, (item.text(1) + " and c. " + item.text(2)))
                    cmd.delete(join(self.database, item.text(1)))
                else:
                    print(join(self.database, selectedWithChain+".pdb")+" or "+join(self.database, item.text(1)+".pdb")+" not found. Attempting to fetch from PDB server")
                    cmd.fetch(selectedWithChain, fullSelection)

        for prot in cmd.get_names("all"):
            if not "query" in prot and not prot in tempAll:
                cmd.delete(prot)

        cmd.remove("resn hoh")
        cmd.remove("hetatm")
        self.alignProteins(temp, tempAll)
        cmd.zoom("query")


    def alignProteins(self, proteins, alignments):
        with open(join(self.resultsLocation, "result.json"), "r") as file:
            data = json.loads(file.read())

        for i in range(0, len(data)):
            if data[i]['pdb_id'] in proteins:
                for al in range(0, len(data[i]['alignment'])):
                    sele = data[i]['pdb_id']+"_"+str(al)
                    if sele in alignments:
                        selection = []
                        lent = 0
                        for pair in range(0, len(data[i]['alignment'][al]['aligned_residues'])):
                            info = data[i]['alignment'][al]['aligned_residues'][pair]['a'].split("=")
                            ref = info[0].split(":")
                            tar = info[1].split(":")
                            lenref = cmd.count_atoms(r"/query///" + ref[1]+ r"/CA")
                            lentar = cmd.count_atoms(r"/"+sele+"///" + tar[1] + r"/CA")
                            if lenref == lentar:
                                lent += lentar + lenref
                                if lent <= 186:
                                    selection.append(r"/"+sele+"///" + tar[1] + r"/CA")
                                    selection.append(r"/query///" + ref[1] + r"/CA")
                                else:
                                    numAtoms = (lent-lentar-lenref)//2
                                    print("Used alignment of first ", numAtoms, " atoms for display of ", str(data[i]['pdb_id']), " alignment number ", str(al))
                                    break
                        try:
                            if not "win" in sys.platform:
                                cmd.pair_fit(*selection)
                            else:
                                cmd.align(sele, "query")
                        except:
                            print ("Fitting unsuccessful")


    def multiListBox1Select(self, ar):
        if "win" in sys.platform:
            cmd.set("grid_mode", value=0)
            
        if not "query" in cmd.get_names("all"):
            cmd.load(self.proteinLocation, "prot")
            createStr = ""
            temp = []
            if "." in self.queryCompareChain:
                temp.append(self.queryCompareChain.split(".")[-1])
            elif ":" in self.queryCompareChain:
                for res in self.queryCompareChain.split(":")[1:]:
                    temp.append(res[0])
            else:
                for ch in self.queryCompareChain:
                    temp.append(ch)
            for ch in temp:
                if not createStr == "":
                    createStr = createStr + " or "
                createStr = createStr + ("chain " + str(ch))
            createStr = createStr + " in prot"
            cmd.create(('query'), createStr)
            cmd.center('query')
            cmd.delete("prot")
            cmd.remove("resn hoh")
            cmd.remove("hetatm")

        dialog.Tree2.clear()

        selFrame = dialog.tabWidgetOutput.currentIndex()

        array = []
        curArray = []
        for item in dialog.Tree1.selectedItems():
            array.append(int(item.text(0)))

        for curBsite in cmd.get_names("all"):
            if "bsite-" in curBsite:
                if not int(curBsite.split("-")[1]) in array:
                    cmd.delete(curBsite)
                else:
                    curArray.append(int(curBsite.split("-")[1]))
            elif "query" in curBsite or "lig-" in curBsite:
                pass
            else:
                cmd.delete(curBsite)


        itemList = []
        rankedTreeItems = []
        rank = 0

        for i in self.pluginResult:
            tmp = self.pluginResult[i]

            if tmp['bs_id'] in array:
                for lig in tmp['ligands']:
                    item = [str(tmp['ligands'][lig]['name']), tmp['ligands'][lig]['zscore'], tmp['ligands'][lig]['comments']['rest']]
                    itemList.append(item)
                if not tmp['bs_id'] in curArray:
                    cmd.load(join(probisliteDir, "data", "grid-"+str(self.queryProtein)+str(self.queryChain)+"_"+str(self.pluginResult[i]['bs_id'])+".pdb"), "bsite-"+str(self.pluginResult[i]['bs_id']))
                    self.showFancyGrid(str(self.pluginResult[i]['bs_id']))

        for item in sorted(itemList, key=lambda zscore: zscore[1], reverse=True):
            rank += 1
            strRank = ('      '+str(rank))[-6:]
            ligScore = ('      '+str(item[1]))[-6:]
            info = item[0].split("_")
            TreeWidg = QtWidgets.QTreeWidgetItem(rank)
            TreeWidg.setText(0, strRank)
            TreeWidg.setText(1, item[0])
            TreeWidg.setText(2, info[1])
            TreeWidg.setText(3, info[2])
            TreeWidg.setText(4, info[3])
            TreeWidg.setText(5, item[2])
            TreeWidg.setText(6, info[4])
            TreeWidg.setText(7, ligScore)
            rankedTreeItems.append(TreeWidg)
        dialog.Tree2.addTopLevelItems(rankedTreeItems)
        dialog.Tree2.sortItems(0, QtCore.Qt.AscendingOrder)
        for i in range(0,7):
            dialog.Tree2.resizeColumnToContents(i)


    def multiListBox2Select(self, ar):

        array = []
        bsitesArray = []
        ligandFileList = []
        ligandNameList = []

        if "win" in sys.platform:
            cmd.set("grid_mode", value=0)

        for item in dialog.Tree2.selectedItems():
            lig_id = item.text(1)
            array.append(lig_id)
            b_id = lig_id.split("_")[0]
            if not b_id in bsitesArray:
                bsitesArray.append(b_id)
        for s in bsitesArray:
            try:
                site = self.pluginResult[s]
            except:
                site = self.pluginResult[int(s)]
            for lig in site['ligands']:
                if lig in array:
                    ligandFile = join(probisliteDir, "data", "lig-"+site['ligands'][lig]['name']+".pdb")
                    ligandFileList.append(ligandFile)
                    ligandNameList.append("lig-"+site['ligands'][lig]['name'])
                    
        self.loadLigandsToPymol(ligandFileList, ligandNameList)
        cmd.hide('spheres')

    def removeMoleculeFromPymol(self, moleculeId):
        cmd.delete(moleculeId)

    def loadLigandsToPymol(self, ligandlist, ligandNameList):
        if self.ligandselection != []:
            for a in self.ligandselection:
                cmd.delete(a)
        self.ligandselection = []
        for i in range(0, len(ligandlist)):
            self.ligandselection.append(ligandNameList[i])
            cmd.load(ligandlist[i], ligandNameList[i])
            if not "win" in sys.platform:
                color_carbons(random.choice(ligandcolors), ligandNameList[i])
        if len(ligandlist) == 1:
            self.static_zoom_to_ligand(ligandNameList[0])

    def static_zoom_to_ligand(self, ligname):
        """Zoom into ligand and its interactions."""
        cmd.center(ligname)
        cmd.orient(ligname)
        cmd.turn('x', 110)
        if 'AllBSRes' in cmd.get_names("selections"):
            cmd.zoom('%s or AllBSRes' % ligname, 5)
        else:
            cmd.zoom(ligname, 5)
        cmd.origin(ligname)

    def exportMoleculesFromPymol(self):
        print("exporting molecules")
        exportFile = savewin.LineSave.text()
        if exportFile == "":
            QtWidgets.QMessageBox.about(dialog, "Error Export", "Please enter path to export file.")
        else:
            exportObjects = []
            for item in savewin.TreeSave.selectedItems():
                if item.text(0) == "query protein":
                    exportObjects.append("query")
                else:
                    exportObjects.append(item.text(1))
            exportLine = " or ".join(exportObjects)
            cmd.multisave(exportFile, exportLine)

            print("Saved chosen molecules succesfully")

    #
    # filer on selected molecules
    #

    def applyFilterSelection(self, flag = False):
        savewin.TreeSave.clear()
        e=1
        try:
            if e == 1:
                if len(self.pluginResult) != 0:
                    pdbId = self.queryProtein
                    chainId = self.queryChain
                    TreeItems = []
    
                    item = ("query protein", pdbId+chainId)
    
                    TreeItem = QtWidgets.QTreeWidgetItem(item)
                    TreeItem.setText(0, str(item[0]))
                    TreeItem.setText(1, str(item[1]))
                    TreeItems.append(TreeItem)
    
                    statusBsite = savewin.CheckBind.isChecked()
                    statusLigands = savewin.CheckLig.isChecked()
    
                    list = cmd.get_object_list(selection='(all)')
    
                    for i in range(0, len(list)):
                        item = ""
                        if re.search("bsite-", list[i]) and statusBsite == 1:
                            id = list[i].split('-')[1]
                            # item = (self.pluginResult[id]['bs_id'], list[i])
                            item = ("binding site", list[i])
    
                        elif re.search("lig-", list[i]) and statusLigands == 1:
                            id = list[i].split('-')[1]
                            bs = id.split("_")[0]
                            try:
                                item = (self.pluginResult[bs]["ligands"][id]["comments"]["rest"], list[i])
                            except:
                                item = (self.pluginResult[int(bs)]["ligands"][id]["comments"]["rest"], list[i])
    
                        if item != "":
                            TreeItem = QtWidgets.QTreeWidgetItem(item)
                            TreeItem.setText(0, str(item[0]))
                            TreeItem.setText(1, str(item[1]))
                            TreeItems.append(TreeItem)
    
                    savewin.TreeSave.addTopLevelItems(TreeItems)
                    # select all at the beginning
                    if flag:
                        for item in savewin.TreeSave.findItems("*", QtCore.Qt.MatchWrap | QtCore.Qt.MatchWildcard | QtCore.Qt.MatchRecursive):
                            savewin.TreeSave.setCurrentItem(item)
        except:
            QtWidgets.QMessageBox.about(dialog, "Save error", "Please run a probis analysis before trying to save.")
            print("Please run a probis analysis before trying to save.")


    def resetWorkspace(self, ask=True):

        flag = True
        if dialog.Tree1.topLevelItemCount() != 0:
            if ask:
                reply = QtWidgets.QMessageBox.question(dialog, "Delete Project", 'All data for current project will be lost. Continue?',
                                                       QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)
                if not reply == QtWidgets.QMessageBox.Yes:
                    flag = False


            if flag:
                # remove from pymol
                list = cmd.get_object_list(selection='(all)')
                for i in range(0, len(list)):
                    if re.search("bsite", list[i]) or re.search("lig", list[i]) or re.search("align", list[i]) or re.search("mut", list[i]):
                        self.removeMoleculeFromPymol(list[i])


                # remove protein assembly from pymol
                pdbId = self.queryProtein
                chainId = self.queryChain
                self.removeMoleculeFromPymol(pdbId+chainId)

                # clear all listboxs
                dialog.TreeProt.clear()
                dialog.Tree1.clear()
                dialog.Tree2.clear()

                self.queryProtein = ""
                self.queryChain = ""
                self.queryCompareChain = ""
                self.BSiteFind = False
                self.ligandselection = []
                # self.selectedBsites = []

                dialog.LabelInputed.setText("")

                # delete files in working directory
                for file in os.listdir(join(probisliteDir, "data")):
                    filePath = join(probisliteDir, "data", file)
                    os.remove(filePath)
                    try:
                        if os.path.isfile(filePath):
                            os.unlink(filePath)
                    except Exception as e:
                        print(e)

        return flag


    def findLatestVersion(self):
        self.latestVersionURL = "https://gitlab.com/JuricV/skupni-gui-lisica_probis/-/raw/master/ProBis_Plugin/.probis/version.txt"
        try:
            global latestVersion
            latestVersionFile = urllib.request.urlopen(self.latestVersionURL, timeout=5)
            latestVersion = latestVersionFile.read().decode(encoding="utf-8").strip()
            if len(latestVersion) > 6:
                latestVersion = ""
        except HTTPError as e1:
            print(("HTTP Error:", e1.code, e1.reason))
            latestVersion = currentVersion
        except URLError as e2:
            print(("URL Error:", e2.reason))
            latestVersion = currentVersion
        except Exception as e:
            print("URL Error: Is Insilab webpage (http://insilab.org) down?")
            latestVersion = currentVersion


    def findCurrentVersion(self):
        versionFile = join(probisliteDir, "version.txt")
        with open(versionFile, "r") as vFile:
            global currentVersion
            lines = vFile.readlines()
            currentVersion = lines[0].strip()


def main():
    if not os.path.exists(probisliteDir):
        os.makedirs(probisliteDir)
    if not os.path.exists(join(probisliteDir, "data")):
        os.makedirs(join(probisliteDir, "data"))

    app = ProbisGUI()
