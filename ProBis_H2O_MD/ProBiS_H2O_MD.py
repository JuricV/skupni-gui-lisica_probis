# Copyright Notice
# ================
#
# The PyMOL Plugin source code in this file is copyrighted, but you can
# freely use and copy it as long as you don't change or remove any of
# the copyright notices.
#
# ----------------------------------------------------------------------
# This PyMOL Plugin is Copyright (C) 2017 by Marko Jukic <jukic.marko@gmail.com>
#
#                        All Rights Reserved
#
# Permission to use, copy and distribute
# versions of this software and its documentation for any purpose and
# without fee is hereby granted, provided that the above copyright
# notice appear in all copies and that both the copyright notice and
# this permission notice appear in supporting documentation, and that
# the name(s) of the author(s) not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.
#
# THE AUTHOR(S) DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.  IN
# NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY SPECIAL, INDIRECT OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
# USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------


# -*- coding: utf-8 -*-
# ProBis_H2O_MD
# written for python 2.7.x
# Not Yet Described at PyMOL wiki: /
# Author : Marko Jukic
# Date: September 2019
# License: CC BY-NC-SA
# Version: 0.98
#__author__ = "Marko Jukic"
#__licence__ = "CC BY-NC-SA"
#__version__ = "0.98"


# Comments: Script is commented in Slovene language
#

from __future__ import absolute_import
from __future__ import print_function
import os
#import stat
import urllib.request, urllib.error, urllib.parse
from urllib.request import urlopen
from urllib.error import URLError, HTTPError
import zipfile
import tempfile
import shutil
import sys
import platform
from distutils.dir_util import copy_tree
#import webbrowser
import time
from threading import Thread
import contextlib
# entry point to PyMOL's API
from pymol import cmd
# pymol.Qt provides the PyQt5 interface, but may support PyQt4
# and/or PySide as well
from pymol.Qt import QtWidgets, QtCore
from pymol.Qt.utils import loadUi


def __init_plugin__(app=None):
    '''
    Add an entry to the PyMOL "Plugin" menu
    '''
    from pymol.plugins import addmenuitemqt
    addmenuitemqt("ProBiS H2O MD", lambda: run())

dialog = None

class UpdateCheck:

    def __init__(self):
        self.firstVersionURL = "http://insilab.org/files/probis-h2o-md/version.txt"
        self.firstArchiveURL = "http://insilab.org/files/probis-h2o-md/archive.zip"
        self.currentVersion=""
        self.latestVersion =""


    def installationFailed(self):
        global running, status
        status = "failed"
        running = False

    def deleteTmpDir(self):
        try:
            shutil.rmtree(self.tmpDir)
            print(("deleted temporary ", self.tmpDir))
        except:
            print ("error : could not remove temporary directory")
            
    def createTmpDir(self):
        try:
            self.tmpDir= tempfile.mkdtemp()
            print(("created temporary ", self.tmpDir))
        except:
            print ("error : could not create temporary directory")
            self.installationFailed()
        self.zipFileName=os.path.join(self.tmpDir, "archive.zip")

    def downloadInstall(self):
        try:
            print ("Fetching plugin from the Insilab server")
            urlcontent = urlopen(self.firstArchiveURL, timeout=5)
            zipcontent = urlcontent.read()
            H2OzipFile = open(self.zipFileName, 'wb')
            H2OzipFile.write(zipcontent)
        except HTTPError as e1:
            print(("HTTP Error:", e1.code, e1.reason))
            self.installationFailed()
        except URLError as e2:
            print(("URL Error:", e2.reason))
            self.installationFailed()
        except Exception as e:
            print ("Error: download failed, check if http://insilab.org website is up...")
            self.installationFailed()
        finally:
            try:
                H2OzipFile.close()
            except:
                pass
            try:
                urlcontent.close()
            except:
                pass

    def extractInstall(self):
        try:
            with contextlib.closing(zipfile.ZipFile(self.zipFileName, "r")) as H2Ozip:
                H2Ozip.extractall(self.tmpDir)
            # copy new
            copy_tree(os.path.join(self.tmpDir, "ProBis_H2O_MD", ".probisH2OMD"), H2O_DIRECTORY)
        except:
            print ("installation of probis H2O MD failed")
            self.installationFailed()

    def firstUpgrade(self):
        return not os.path.isdir(H2O_DIRECTORY)

    def upgrade(self):
        
        self.createTmpDir()
        self.downloadInstall()
        self.extractInstall()
        self.deleteTmpDir()
        sys.path.append(os.path.normpath(os.path.join(H2O_DIRECTORY, "module")))
        self.findLatestVersion()
        #self.writeToVersionTxt(self.latestVersion)

        print ("Upgrade finished successfully!")
        global running, status
        status = "start"
        running = False
        
    def findCurrentVersion(self):
        if os.path.isfile(versionFile):
            with open(versionFile, 'r') as myFile:
                global currentVersion
                lines = myFile.readlines()
                self.currentVersion = lines[0].strip()
                currentVersion = self.currentVersion

    def findLatestVersion(self):
        try:
            global latestVersion
            versionFile = urllib.request.urlopen(self.firstVersionURL, timeout=5)
            self.latestVersion = versionFile.read().decode(encoding="utf-8").strip()
            if len(self.latestVersion) > 6:
                self.latestVersion = ""
            latestVersion = self.latestVersion
            
        except HTTPError as e1:
            print(("HTTP Error:", e1.code, e1.reason))
        except URLError as e2:
            print(("URL Error:", e2.reason))
        except Exception as e:
            print ("URL Error: Is Insilab webpage (http://insilab.org) down?")

    
    def writeToVersionTxt(version, reminder=False):
        if reminder:
            with open(versionFile, 'a') as myfile:
                myfile.write(str(version)+"\t later \t"+time.strftime("%d/%m/%Y")+"\n")
        else:
            with open(versionFile, 'w') as myfile:
                myfile.write((str(version)+"\n").encode("ascii"))
    def updateBox(self):
        global updateDialog

        updateDialog = self.updateWindow()

        updateDialog.show()
    
    def updateWindow(self):
        global updateDialog
        updateDialog = QtWidgets.QMainWindow()

        uifile = os.path.join(UI_DIRECTORY, 'GUIupdate.ui')
        form = loadUi(uifile, updateDialog)

        def cancelButton():
            global status
            if form.checkBoxAsk.isChecked():
                self.writeToVersionTxt(self.latestVersion, True)
            #status = "start"
            updateDialog.close()
            start_probis_h2o_md()
            

        def updateButton(vent=None):
            global status
            updateDialog.close()
            self.upgrade()
            updateDialog.close()
            start_probis_h2o_md()
            
        self.findLatestVersion()
        self.findCurrentVersion()
    
        form.labelVersionLatest.setText(str(self.latestVersion))
        form.labelVersionCurrent.setText(str(self.currentVersion))
        form.buttonBoxUpdate.accepted.connect(lambda: updateButton())
        form.buttonBoxUpdate.rejected.connect(lambda: cancelButton())
    
        return updateDialog
def internetOn():
        try:
            urllib.request.urlopen('http://insilab.org',timeout=5)
            return True
        except:
            return False

def showManualDlInfo():
    msgBox = QtWidgets.QMessageBox()
    msgBox.setIcon(QtWidgets.QMessageBox.Information)
    msgBox.setText("Installation failed: To download and manually install Probis H2O MD please visit our website http://www.insilab.org/probis-h2o-md/ .")
    msgBox.setWindowTitle("Instalation error message")
    msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)

def start_probis_h2o_md():
    print(("Python version: "+platform.architecture()[0]))
    sys.path.append(os.path.normpath(os.path.join(H2O_DIRECTORY, "module")))
    import ProBiS_H2O_MD_plugin
    ProBiS_H2O_MD_plugin.main()

class CustomDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle("Choose Directory")
    def find_dir(self):
        folderpath = str(QtWidgets.QFileDialog.getExistingDirectory(CustomDialog(), "Select Directory"))
        if folderpath.endswith(".probisH2OMD") or folderpath.endswith(".probisH2OMD"+os.path.sep):
            pass
        else:
            folderpath=os.path.join(folderpath, ".probisH2OMD")
        self.lineEdit.setText(folderpath)
    def set_dir(self):
        global SetDir
        SetDir = self.lineEdit.text() 
        try:
            install_datoteka = open(install_dir, "w")
            install_datoteka.write(SetDir)
            install_datoteka.close()
            InstallDialog.close()
        except:
            QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Install Warning", "Please choose a valid install directory!")
            print("Please choose a valid install directory!")
            
    def setupUI(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(640, 148)
        Dialog.setMaximumSize(QtCore.QSize(16777215, 210))
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 2)
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 1, 0, 1, 1)
        HOME_DIRECTORY=os.path.expanduser('~')
        DEFAULT_DIRECTORY=os.path.join(HOME_DIRECTORY, ".probisH2OMD")
        self.lineEdit.setText(DEFAULT_DIRECTORY)
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 1, 1, 1, 1)
        self.pushButton.clicked.connect(self.find_dir)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.pushOK = QtWidgets.QPushButton(Dialog)
        self.pushOK.setObjectName("pushOK")
        self.gridLayout.addWidget(self.pushOK, 3, 1, 1, 1)
        self.pushOK.clicked.connect(self.set_dir)
        

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        
    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("Dialog", "Please choose install directory for ProBiS H2O MD:"))
        self.pushButton.setText(_translate("Dialog", "Pick directory"))
        self.label_2.setText(_translate("Dialog", "*If ProBiS H2O MD has been manually installed please input .probisH2O file."))
        self.pushOK.setText(_translate("Dialog", "OK"))

def run_custom_dialog():
    global InstallDialog
    InstallDialog=QtWidgets.QDialog()
    InstallDialog.ui = CustomDialog()
    InstallDialog.ui.setupUI(InstallDialog)
    InstallDialog.show()
    InstallDialog.exec_()

def NoInstallSetting():
    global SetDir
    run_custom_dialog()
    if not SetDir=="":
        install_datoteka = open(install_dir, "w")
        install_datoteka.write(SetDir)
        install_datoteka.close()
    else:
        QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Install Warning", "Please choose an install directory!")
        print("Please choose an install directory!")
    
def run():
    global H2O_DIRECTORY, UI_DIRECTORY, versionFile, install_dir
    MY_DIR=os.path.os.path.expanduser("~")
    install_dir=os.path.join(MY_DIR, ".probish2omd_installdir.txt")
    try:
        if os.path.isfile(install_dir) == False:
            NoInstallSetting()        
        install_datoteka = open(install_dir, "r")
        H2O_DIRECTORY=install_datoteka.readline()
        install_datoteka.close()
        UI_DIRECTORY=os.path.join(H2O_DIRECTORY,"UI")
        versionFile = os.path.join(H2O_DIRECTORY, "version.txt")
    except:
        if os.path.exists(install_dir):
            os.remove(install_dir)
        QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Install Warning", "Something went wrong!\nPlease choose a valid install directory!")
        print("Something went wrong!\nPlease choose a valid install directory!")


    print("Initialising Probis H2O MD ...")

    global running, status
    status = "start"

    try:
        sys.path.remove('')
    except:
        pass

    if UpdateCheck().firstUpgrade():
        print("First time installation...")
        if not internetOn():
            status = "no-internet"
            print("No internet connection")
        else:
            UpdateCheck().findLatestVersion()
            UpdateCheck().upgrade()
    else:
        UpdateCheck().findCurrentVersion()
        print("Not First time installation")
        if internetOn() == True:
            UpdateCheck().findLatestVersion()
            if str(latestVersion) != str(currentVersion):
                print("Update Needed!")
                status = "check-update"
    
    running = False
    main()

def main():
    global status
    #run_dialog("Starting Probis H2O software...")
    if status == "no-internet":
        msgBox = QtWidgets.QMessageBox()
        msgBox.setIcon(QtWidgets.QMessageBox.Information)
        msgBox.setText("Communication Error: No Internet connection available. Please make sure that your device is connected to the Internet.")
        msgBox.setWindowTitle("Communication error message")
        msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)

    if status == "check-update":
        #dialog.close()
        UpdateCheck().updateBox()

    if status == "start":
        #dialog.close()
        start_probis_h2o_md()

    else:
        showManualDlInfo()
