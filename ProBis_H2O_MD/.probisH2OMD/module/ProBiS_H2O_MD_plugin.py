# Copyright Notice
# ================
#
# The PyMOL Plugin source code in this file is copyrighted, but you can
# freely use and copy it as long as you don't change or remove any of
# the copyright notices.
#
# ----------------------------------------------------------------------
# This PyMOL Plugin is Copyright (C) 2017 by Marko Jukic <jukic.marko@gmail.com>
#
#                        All Rights Reserved
#
# Permission to use, copy and distribute
# versions of this software and its documentation for any purpose and
# without fee is hereby granted, provided that the above copyright
# notice appear in all copies and that both the copyright notice and
# this permission notice appear in supporting documentation, and that
# the name(s) of the author(s) not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.
#
# THE AUTHOR(S) DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.  IN
# NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY SPECIAL, INDIRECT OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
# USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------


# -*- coding: utf-8 -*-
# ProBis_H2O_MD
# written for python 2.7.x
# Not Yet Described at PyMOL wiki: /
# Author : Marko Jukic
# Date: September 2019
# License: CC BY-NC-SA
# Version: 0.98
#__author__ = "Marko Jukic"
#__licence__ = "CC BY-NC-SA"
#__version__ = "0.98"


# Comments: Script is commented in Slovene language
#

from __future__ import absolute_import
from __future__ import print_function
import sys, re, os, random, urllib.request, urllib.parse, urllib.error, urllib.request, urllib.error, urllib.parse
import stat
import time
import pymol, json, math, multiprocessing
from pathlib import Path
import numpy as np
from pymol.cgo import *
from pymol import cmd
import subprocess
from glob import glob
from itertools import groupby

from pymol.Qt import QtWidgets, QtGui, QtCore
from pymol.Qt.utils import loadUi

# ---------------------------------------INITIALIZE-----------------------------
MY_DIRECTORY=os.path.dirname(os.path.realpath(sys.argv[0]))#
if sys.platform == "win32":
    install_dir=os.path.join(MY_DIRECTORY, ".probish2omd_installdir.txt")
else:
    install_dir=os.path.join(Path.home(), ".probish2omd_installdir.txt")
with open(install_dir, "r") as install_datoteka:
    H2O_MD_DIRECTORY=install_datoteka.readline()
MODULE_DIRECTORY=os.path.join(H2O_MD_DIRECTORY, "module")
UI_DIRECTORY=os.path.join(H2O_MD_DIRECTORY,"UI")
versionFile = os.path.join(H2O_MD_DIRECTORY, "version.txt")
SETTINGS_DIRECTORY=os.path.join(H2O_MD_DIRECTORY, "settings")
check_path=os.path.join(H2O_MD_DIRECTORY, "Probis_H2O_MD")
probis_dir = os.path.join(check_path, "probis")
probis_exe_dir = os.path.join(check_path, "probis.exe")
platform=sys.platform


# SCIKIT LEARN & MDANALYSIS------------------------------------------------------------------
try:
    from pip._internal import main as pip
    print("Pip module is installed")
except:
    print("Please install the 'pip3' Python package! On Ubuntu: 'sudo apt install python3-pip' \nPip3 is required, so that the plugin can automatically install scikit-learn and MDAnalysis packages. \nIf automatic installation fails, try to manually install these packages.")
    QtWidgets.QMessageBox.about(None, "ProBiS H2O Install Warning", "Please install the 'pip3' Python package! On Ubuntu: 'sudo apt install python3-pip' \nPip3 is required, so that the plugin can automatically install scikit-learn and MDAnalysis packages. \nIf automatic installation fails, try to manually install these packages.")

try:
    from sklearn.cluster import DBSCAN
    print("Sklearn module is installed")
except:
    try:
        from pip._internal import main as pip
        print("Sklearn module has not been installed yet. \nProgram will now install Scikit-learn module")
        pip(['install', 'scikit-learn'])
        from sklearn.cluster import DBSCAN
    except:
        
        TempApp = QtWidgets.QApplication(sys.argv)
        QtWidgets.QMessageBox.about(None, "ProBiS H2O Install Warning", "Encountered problem installing dependencies. Please manually install Sklearn module in your working enviroment!")
try:
    import MDAnalysis as md
    print("MDAnalysis module is installed")
except:
    try:
        from pip._internal import main as pip
        print("MDAnalysis module has not been installed yet. \nProgram will now install MDAnalysis module")
        pip(['install', 'MDAnalysis'])
        import MDAnalysis as md
    except:
        TempApp = QtWidgets.QApplication(sys.argv)
        QtWidgets.QMessageBox.about(None, "ProBiS H2O Install Warning", "Encountered problem installing dependencies. Please manually install MdAnalysis module in your working eviroment!")

#--------------------------------------------------------------------------------
def get_current_version():
    global current_version
    with open(versionFile) as version:
        current_version = version.readline().strip()

def main():
    print("\nSettings file for instalation directory location: ", install_dir)
    get_current_version()
    print("Current ProBiS H2O MD plugin version: ", current_version)
    GUI().run()
    
# global reference to avoid garbage collection of our dialog
dialog = None

red01 = QtGui.QColor('#ffe6e6')
red02 = QtGui.QColor('#ffcccc')
red03 = QtGui.QColor('#ffb3b3')
red04 = QtGui.QColor('#ff9999')
red05 = QtGui.QColor('#ff8080')
red06 = QtGui.QColor('#ff6666')
red07 = QtGui.QColor('#ff4d4d')
red08 = QtGui.QColor('#ff3333')
red09 = QtGui.QColor('#ff1a1a')
red10 = QtGui.QColor('#ff0000')

update_messages = ["Please wait, this might take a while.",
                   "Still working...",
                   "Ugh... It's taking a long while, huh? A bit more",
                   "Still hard at work",
                   "Don't worry. I'm still working",
                   "Working... Did you stretch yet?",
                   "Work is going on",
                   "Making sure you get the best results, please hold",
                   "Working...",
                   "Analysis ongoing...",
                   "Don't give up on me, I'm doing my best",
                   "Working...",
                   "Still Working...",
                   "Please wait...",
                   "Analysis ongoing",
                   "Working... Might be time for a coffee"]

class GUI(QtWidgets.QMainWindow):
    
    def run(self):
        '''
        Open our custom dialog
        '''
        global dialog
    
        #if main is None:
        dialog = self.make_dialog()
    
        dialog.show()
    
    def make_dialog(self):    
        # create a new Window
        global dialog
        dialog = QtWidgets.QMainWindow()
    
        # populate the Window from our *.ui file which was created with the Qt Designer
        uifile = os.path.join(UI_DIRECTORY, 'ProbisH2OMDMain.ui')
        self.form = loadUi(uifile, dialog)
        
        def CheckCompare_changed():
            checked = dialog.CheckCompare.isChecked()
            if checked == True:
                dialog.CheckWater.setChecked(False)
                dialog.CheckAnalyze.setChecked(False)
                # dialog.CheckResidue.setChecked(False)
                dialog.CheckWater.setEnabled(False)
                dialog.CheckAnalyze.setEnabled(False)
                # dialog.CheckResidue.setEnabled(False)
                
            else:
                dialog.CheckWater.setEnabled(True)
                dialog.CheckAnalyze.setEnabled(True)
                # dialog.CheckResidue.setEnabled(True)
                
        def analysisRun():
            # process = processWin()
            process = processWin()
            process.run()            
        
            
        self.form.CheckResidue.setEnabled(False)
        self.form.PushTop.clicked.connect(custom_disk_file_get.load_file)
        self.form.PushTraj.clicked.connect(custom_disk_file_get.load_files)
        self.form.PushIdentify.clicked.connect(BindingSites.get_binding_sites)
        # self.form.PushGo.clicked.connect(h20Analysis.analyze_waters)
        self.form.PushGo.clicked.connect(analysisRun)
        self.form.PushDisplay.clicked.connect(pyMOLinterface.pyMOL_display_cluster)
        self.form.PushMD.clicked.connect(pyMOLinterface.pyMOL_fetch_MD)
        self.form.PushBSite.clicked.connect(pyMOLinterface.pyMOL_bsite_cluster)
        self.form.PushContacts.clicked.connect(pyMOLinterface.pyMOL_water_contacts)
        self.form.PushChain.clicked.connect(pyMOLinterface.pyMOL_chain_box)
        self.form.PushFetch.clicked.connect(pyMOLinterface.pyMOL_fetch_system)
        self.form.CheckCompare.stateChanged.connect(CheckCompare_changed)
        
        self.form.LabelCurrent.setText(current_version)
        
        if os.path.isdir(check_path) == False:
            dialog.CheckAnalyze.setEnabled(False)
            dialog.CheckCompare.setEnabled(False)
            dialog.CheckDebye.setEnabled(False)
            dialog.CheckWater.setEnabled(False)
            dialog.ComboBlastclust.setEnabled(False)
            dialog.LineTop.setEnabled(False)
            dialog.LineTraj.setEnabled(False)
            dialog.ListIdentify.setEnabled(False)
            dialog.PushTraj.setEnabled(False)
            dialog.PushTop.setEnabled(False)
            dialog.PushGo.setEnabled(False)
            dialog.PushIdentify.setEnabled(False)
            dialog.CheckKeep.setEnabled(False)
            dialog.ListCalculated.setEnabled(False)
            dialog.PushBSite.setEnabled(False)
            dialog.PushChain.setEnabled(False)
            dialog.PushContacts.setEnabled(False)
            dialog.PushDisplay.setEnabled(False)
            dialog.PushFetch.setEnabled(False)
            QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Database Warning", "Please ensure ProBiS executable is correctly installed in .probisH2OMD/ProBiS_H2O_MD folder.")
            print("Please ensure ProBiS executable is correctly installed in .probisH2OMD/ProBiS_H2O_MD folder.")
        else:
            os.chdir(check_path)
            RSCB_contact.file_checks()
        
        return dialog

class processWin():
    def __init__(self):
        print("Starting analysis")
    
    def run(self):
        self.open_process_window()
        QtWidgets.QApplication.processEvents()
        self.analysis = h20Analysis()
        self.analysis.analyze_waters()
        
        
    def open_process_window(self):
        global progressDialog
        progressDialog = QtWidgets.QDialog()
        progFile = os.path.join(UI_DIRECTORY, "ProbisH2OMDProgress.ui")
        self.prog = loadUi(progFile, progressDialog)
        progressDialog.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        progressDialog.show()
        QtWidgets.QApplication.processEvents()
        

# ----------------------------------------------------FUNCTIONS-----------------        
class custom_disk_file_get:

    global traj_list
    traj_list = []

    @staticmethod
    def load_file():
        global filename
        filename, _filter = QtWidgets.QFileDialog.getOpenFileName(dialog, "Open..." + "Topology File", ".")
        if filename:
            try:
                print("File read.")
                print((str(filename)))
            except:
                QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "File error or \nFile not Found! \nPlease investigate!")
            pdb_text = str(filename)
            dialog.LineTop.setText(pdb_text)    

    @staticmethod
    def load_files():
        global find_value, fajlname2
        fajlname2, _filter = QtWidgets.QFileDialog.getOpenFileName(dialog, "Open..." + "Trajectory File", ".")
        if fajlname2:
            
            try:
            
                print("Files read.")
                print((str(fajlname2)))
            except:
                QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "File error or \nFile not Found! \nPlease investigate!")   
            traj_text = str(fajlname2)
            dialog.LineTraj.setText(traj_text)



class RSCB_contact:

    """PDB database server setup"""
    
    lista_cluster_fajlov = ["bc-30.out",
                            "bc-40.out", "bc-50.out", "bc-70.out",
                            "bc-90.out", "bc-95.out", "bc-100.out"]

    @staticmethod
    def fetch_probis():
        urllib.request.urlretrieve ("http://insilab.org/files/probis-algorithm/probis", "probis")
    def fetch_probis_exe():
        urllib.request.urlretrieve("https://gitlab.com/JuricV/skupni-gui-lisica_probis/-/raw/testfiles/probis_exe/probis.exe", "probis.exe")
        urllib.request.urlretrieve("https://gitlab.com/JuricV/skupni-gui-lisica_probis/-/raw/testfiles/probis_exe/libgsl.dll", "libgsl.dll")
        urllib.request.urlretrieve("https://gitlab.com/JuricV/skupni-gui-lisica_probis/-/raw/testfiles/probis_exe/libgslcblas.dll", "libgslcblas.dll")
    
    @staticmethod
    def file_checks():
        """preveri ce imamo instaliran probis program"""
        print("\nProBis_H2O: setting up...")
        if platform == "linux":
            if os.path.isfile(probis_dir) == False:
                QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "Please ensure probis is downloaded. \nYou may need to reinstall.")
                print("ProBiS_H2O: please ensure probis is downloaded correctly. You may need to reinstall.")
            else:
                try:
                    if os.access(probis_dir, os.X_OK) == True:
                        print("ProBiS executable ok")
                    else:
                        st = os.stat(probis_dir)
                        os.chmod(probis_dir, st.st_mode | stat.S_IEXEC)
                        print("ProBiS executable permission set")
                        print("ProBiS directory:      : ", probis_dir)
                except:
                    pass
        elif platform == "win32":
            if os.path.isfile(probis_exe_dir) == False:
                QtWidgets.QMessageBox.about(dialog, "ProBiS H2O Warning", "Please ensure probis.exe is downloaded correctly.\nYou may need to reinstall.")
                print("ProBiS_H2O: please ensure probis.exe is downloaded correctly. You may need to reinstall.")

# ------------------------------------------------------------------------------
class BindingSites:
    """definiraj binding site"""

    bsite_unique_centers = []

    @staticmethod
    def get_binding_sites():
        dialog.PushIdentify.setText("Wait...")
        dialog.PushIdentify.setEnabled(False)
        update_win()
        if os.path.isfile(r"master_water_list.txt"):
            os.system("mv master_water_list.txt master_water_list.txt.backup")

        nedostopni_datoteka = open("./unavailable_pdb_list.txt", "w")
        nedostopni_datoteka.close()
        # try:
        try:
            dialog.ListIdentify.clear()
        except:
            pass
        update_win()
        
        water_sel = dialog.CheckWater.isChecked()
        chain_sel = dialog.CheckCompare.isChecked()
        target_complex_2 = dialog.LineTop.text()
        target_trajectory = dialog.LineTraj.text()
        razdalja = dialog.SpinRange.value()
        
        u=md.Universe(target_complex_2, target_trajectory)
        
        update_win()
        
        target_complex_2 = os.path.join(check_path, "examined.pdb")
        izbira = ("byres (resname HOH and around {} protein) or ".format(razdalja) + 
                  "byres (resname SPC and around {} protein) or ".format(razdalja) + 
                  "byres (resname T3P and around {} protein) or ".format(razdalja) + 
                  "byres (resname T4P and around {} protein) or ".format(razdalja) + 
                  "byres (resname TIP3 and around {} protein) or ".format(razdalja) + 
                  "byres (resname OH2 and around {} protein) or protein".format(razdalja))
        full = u.select_atoms(izbira)
        full.write(target_complex_2)

        update_win()

        # VSE LISTE---------------------------------------------------------
        lista_za_heteroatome = []
        lista_za_atome = []
        lista_za_vode = []
        lista_binding_sites = []
        lista_water_binding_sites = []
        lista_verige = []
        lista_verige_konc = []
        warning_lista = []
        #lista_verige_unique = []
        bsite_unique = []
        # podaj rezultate v tri glavne liste--------------------------------
        vode_seznam = ["HOH", "SPC", "T3P", "T4P"]
        try:
            with open(target_complex_2, "rt") as infile:
                for linenumber, line in enumerate(infile):
                    if line.startswith("ATOM") or line.startswith("HETATM"):
                        if line[21] == " ":
                            line = line[:21]+"X"+line[22:]
                        if line[17:20] in vode_seznam:
                            lista_za_vode.append(line.rstrip("\n"))
                        elif line.startswith("HETATM"):
                            lista_za_heteroatome.append(line.rstrip("\n"))
                        elif line.startswith("ATOM"):
                            lista_za_atome.append(line.rstrip("\n"))
                    update_win()
        except OSError:
            QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "File not found \n Please Investigate!")
            print("File not found!, please investigate.")
        
        # VKLJUCITEV CHAIN VOD
        global lista_za_atome_xyzchain
        lista_za_atome_xyzchain = []
        global lista_za_atome_x
        lista_za_atome_x = []
        global lista_za_atome_y
        lista_za_atome_y = []
        global lista_za_atome_z
        lista_za_atome_z = []

        for entry in lista_za_atome:
            try:
                # ATOM    477  CG2 ILE A  78       6.540   0.762  34.941  1.00 13.42           C
                # ATOM   2930  C   ASP   192       7.178  21.044  51.862
                lista_za_atome_x.append(entry[30:38].strip())
                lista_za_atome_y.append(entry[38:46].strip())
                lista_za_atome_z.append(entry[46:54].strip())
                lista_za_atome_xyzchain.append([entry[30:38].strip(), entry[38:46].strip(), entry[46:54].strip(), entry[21].strip()])
                update_win()
            except:
                pass

        if water_sel == False:
            for linija in lista_za_heteroatome:
                #naslednja linija je pomembna za korekturo "posebnih" pdb (4bqp, ...), to je ze 123 korektura...
                unique_binding_site = str(linija[17:20].strip()) + "." + str(linija[22:26].strip()) + "." + str(linija[21])
                lista_binding_sites.append(unique_binding_site)
                bsite_unique.append([unique_binding_site, linija[30:38].strip(), linija[38:46].strip(), linija[46:53].strip()])
                update_win()
                    
        else:
            for linija in lista_za_vode:
                unique_binding_site = str(linija[17:20].strip())+"."+str(linija[22:25].strip())+"."+str(linija[21])
                lista_binding_sites.append(unique_binding_site)
                bsite_unique.append([unique_binding_site, linija[30:38].strip(), linija[38:46].strip(), linija[46:53].strip()])
                update_win()
               
                
        # priprava bsite ---------------------------------------------------
        # ok grupiranje ker zelimo UNIQUE
        for key, group in groupby(bsite_unique, lambda x: x[0]):
            bsx = []
            bsy = []
            bsz = []
            for el in group:
                bsx.append(float(el[1]))
                bsy.append(float(el[2]))
                bsz.append(float(el[3]))

            # name of bsite, axerage x, average y, average z, min x, max x, min y, max y, min z, max z
            BindingSites.bsite_unique_centers.append([key, sum(bsx)/len(bsx), sum(bsy)/len(bsy), sum(bsz)/len(bsz), min(bsx), max(bsx), min(bsy), max(bsy), min(bsz), max(bsz)])
            update_win()

        # priprava vode-----------------------------------------------------
        for linija in lista_za_vode:
            # ATOM    477  OH2 HOH A  78       6.540   0.762  34.941  1.00 13.42           C
            water_binding_sites = str(linija[17:20].strip())+"."+str(linija[22:26].strip())+"."+str(linija[21])
            lista_water_binding_sites.append(water_binding_sites)
            update_win()
            

        # priprava ostalo---------------------------------------------------
        for linija in lista_za_atome:
            atom_site = []
            try:
                atom_site.append(str(linija[21]))
                atom_site.append(int(linija[22:26]))
                lista_verige.append(atom_site)
                update_win()
            except:
                pass

        # priprava verige---------------------------------------------------
        for key, group in groupby(lista_verige, lambda x: x[0]):
            temp_group = []
            #residue_number = 0
            for el in group:

                # TO JE ZA STETJE UNIKATNIH AK OSTANKOV
                if el not in temp_group:
                    temp_group.append(el)
                else:
                    pass

                # uporbljeno za opcijo kjer se steje unikatne AK ostanke
                ins_str = str(temp_group[-1][0]) + " chain with " + str(len(temp_group)) + " residues"
                # ins_str = str(temp_group[-1][0]) + " chain with " + str(temp_group[-1][1]) + " residues"


            lista_verige_konc.append(ins_str)
            update_win()

        # resevanje prolematicnih pdbjev - naj preveri uporabnik
        # v bodoce mogoce avtomatsko
        #-------------------------------------------------------------------
        
        if chain_sel == False:
            for chainelement in lista_verige_konc:
                if float(chainelement.split()[3]) < 30.0:
                    temp_str = "ONLY " + str(chainelement.split()[3]) + " residues found in chain " + str(chainelement.split()[0])
                    # problemi v warning listi
                    warning_lista.append(temp_str)
                    update_win()
                else:
                    pass

            if len(warning_lista) >= 1:
                QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "Try to compare whole chains instead of individual binding sites at the short chain location.")
            
            else:
                pass
            
            if water_sel == False:
                for entry in sorted(list(set(lista_binding_sites))):
                    dialog.ListIdentify.addItem(entry)   
            else:
                for entry in sorted(list(set(lista_binding_sites))):
                    dialog.ListIdentify.addItem(entry)  
                for entry in sorted(list(set(lista_water_binding_sites))):
                    dialog.ListIdentify.addItem(entry) 
                    
        else:
            for entry in lista_verige_konc:
                dialog.ListIdentify.addItem(entry)
        
        update_win()
        dialog.PushIdentify.setText("Identify")
        dialog.PushIdentify.setEnabled(True)
        return None
        # flow out except-------------------------------------------------------
        
        # except:
        #     QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "Invalid PDB ID or \nDatabase File not Found! \n\nPlease investigate!")
        #     print("Database File not Found!")
        #     print((sys.exc_info()))

        # class end

# report lista 1----------------------------------------------------------------
report_list_1 = []
report_list_2 = []
report_list_1.append("ProBiS H2O REPORT file")
report_list_1.append("-" * 25 + "\n\n")

       
class h20Analysis():
    """collect, prepare, cluster, analyze, display, crystal h20 data"""
    def analyze_waters(self):
        global SELECTED_SITE
        global SELECTED_SITE_CHAIN
        global protein_name
        # NUM CPU:
        try:
            processors_available_local = str(multiprocessing.cpu_count())
        except:
            processors_available_local = "1"
        # NUM_CPUS //
        #peepee
        prot = dialog.LineTop.text().lower()
        prot1= os.path.normpath(prot).split(os.path.sep)
        protein_name = prot1[-1].split(".")[0]
        nova_datoteka = open("report_" + protein_name + ".txt", "w")
        nova_datoteka.close()

        # get setting on superposition starting point analysis
        bsite_space_check = dialog.CheckAnalyze.isChecked()

        #chain_sel = whole_chain_setting.get()
        chain_sel = dialog.CheckCompare.isChecked()
        water_sel = dialog.CheckWater.isChecked()
        examined_list_unique = []
        
        target_complex_universe = str(dialog.LineTop.text())
        target_trajectory = dialog.LineTraj.text()
        target_complex_2=os.path.join(check_path, "examined.pdb")
        step = int(dialog.LineInterval.text())
        razdalja = dialog.SpinRange.value()
        protein=target_complex_2[:-4]
        u=md.Universe(target_complex_universe, target_trajectory)
        
        update_win()
        
        # izbira = "byres (resname HOH and around {} protein)".format(dialog.SpinRange.value())
        izbira = ("byres (resname HOH and around {} protein) or ".format(razdalja) + 
                  "byres (resname SPC and around {} protein) or ".format(razdalja) + 
                  "byres (resname T3P and around {} protein) or ".format(razdalja) + 
                  "byres (resname T4P and around {} protein) or ".format(razdalja) + 
                  "byres (resname TIP3 and around {} protein) or ".format(razdalja) + 
                  "byres (resname OH2 and around {} protein) or protein".format(razdalja))
        full = u.select_atoms(izbira)
        print("ProBiS H2O MD: Writing protein with selected settings")
        update_win()
        full.write(target_complex_2)
        print("ProBiS H2O MD: Protein written")
        update_win()
        
        examined_list=[]
        for frame in u.trajectory[::step]:
            examined_list.append(frame)
        update_win()

        with open('./unavailable_pdb_list.txt', 'r') as f:
            removed_list = f.read().splitlines()
        removed_list.append(target_complex_2.lower())

        for fr in examined_list:
            num = str(fr.frame)
            fr_name = (num + "_" + protein_name)
            izbira = ("byres (resname HOH and around {} protein) or ".format(razdalja) + 
                      "byres (resname SPC and around {} protein) or ".format(razdalja) + 
                      "byres (resname T3P and around {} protein) or ".format(razdalja) + 
                      "byres (resname T4P and around {} protein) or ".format(razdalja) + 
                      "byres (resname TIP3 and around {} protein) or ".format(razdalja) + 
                      "byres (resname OH2 and around {} protein) or protein".format(razdalja))
            full = u.select_atoms(izbira)
            full.write(fr_name + ".pdb")
            if fr_name not in removed_list:
                if fr_name not in examined_list_unique:
                    examined_list_unique.append(fr_name)
            update_win()
                    
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        
        lista_za_heteroatome = []
        lista_za_atome = []
        lista_za_vode = []
        lista_binding_sites = []
        lista_water_binding_sites = []
        lista_verige = []
        #lista_verige_unique = []
        bsite_unique = []
        # podaj rezultate v tri glavne liste--------------------------------
        vode_seznam = ["HOH", "SPC", "T3P", "T4P"]
        try:
            with open(target_complex_2, "rt") as infile:
                for linenumber, line in enumerate(infile):
                    if line.startswith("ATOM") or line.startswith("HETATM"):
                        if line[21] == " ":
                            line = line[:21]+"X"+line[22:]
                        if line[17:20] in vode_seznam:
                            lista_za_vode.append(line.rstrip("\n"))
                        elif line.startswith("HETATM"):
                            lista_za_heteroatome.append(line.rstrip("\n"))
                        elif line.startswith("ATOM"):
                            lista_za_atome.append(line.rstrip("\n"))
                    update_win()
        except OSError:
            QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "File not found \n Please Investigate!")
            print("File not found!, please investigate.")
        
        # VKLJUCITEV CHAIN VOD
        global lista_za_atome_xyzchain
        lista_za_atome_xyzchain = []
        global lista_za_atome_x
        lista_za_atome_x = []
        global lista_za_atome_y
        lista_za_atome_y = []
        global lista_za_atome_z
        lista_za_atome_z = []

        for entry in lista_za_atome:
            try:
                # ATOM    477  CG2 ILE A  78       6.540   0.762  34.941  1.00 13.42           C
                # ATOM   2930  C   ASP   192       7.178  21.044  51.862
                lista_za_atome_x.append(entry[30:38].strip())
                lista_za_atome_y.append(entry[38:46].strip())
                lista_za_atome_z.append(entry[46:54].strip())
                lista_za_atome_xyzchain.append([entry[30:38].strip(), entry[38:46].strip(), entry[46:54].strip(), entry[21].strip()])
                update_win()
            except:
                pass

        if water_sel == False:
            for linija in lista_za_heteroatome:
                #naslednja linija je pomembna za korekturo "posebnih" pdb (4bqp, ...), to je ze 123 korektura...
                unique_binding_site = str(linija[17:20].strip()) + "." + str(linija[22:26].strip()) + "." + str(linija[21])
                lista_binding_sites.append(unique_binding_site)
                bsite_unique.append([unique_binding_site, linija[30:38].strip(), linija[38:46].strip(), linija[46:53].strip()])
                update_win()
                    
        else:
            for linija in lista_za_vode:
                unique_binding_site = str(linija[17:20].strip())+"."+str(linija[22:25].strip())+"."+str(linija[21])
                lista_binding_sites.append(unique_binding_site)
                bsite_unique.append([unique_binding_site, linija[30:38].strip(), linija[38:46].strip(), linija[46:53].strip()])
                update_win()
               
                
        # priprava bsite ---------------------------------------------------
        # ok grupiranje ker zelimo UNIQUE
        for key, group in groupby(bsite_unique, lambda x: x[0]):
            bsx = []
            bsy = []
            bsz = []
            for el in group:
                bsx.append(float(el[1]))
                bsy.append(float(el[2]))
                bsz.append(float(el[3]))

            # name of bsite, axerage x, average y, average z, min x, max x, min y, max y, min z, max z
            BindingSites.bsite_unique_centers.append([key, sum(bsx)/len(bsx), sum(bsy)/len(bsy), sum(bsz)/len(bsz), min(bsx), max(bsx), min(bsy), max(bsy), min(bsz), max(bsz)])
            update_win()

        # priprava vode-----------------------------------------------------
        for linija in lista_za_vode:
            # ATOM    477  OH2 HOH A  78       6.540   0.762  34.941  1.00 13.42           C
            water_binding_sites = str(linija[17:20].strip())+"."+str(linija[22:26].strip())+"."+str(linija[21])
            lista_water_binding_sites.append(water_binding_sites)
            update_win()
            

        # priprava ostalo---------------------------------------------------
        for linija in lista_za_atome:
            atom_site = []
            try:
                atom_site.append(str(linija[21]))
                atom_site.append(int(linija[22:26]))
                lista_verige.append(atom_site)
                update_win()
            except:
                pass
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        #----------------------------------------------------------------------
        
            
        try:
            bsite_selection_full = dialog.ListIdentify.currentItem()
            bsite_selection = bsite_selection_full.text()
            chain_selection = bsite_selection_full.text()[-1]
            whole_chain_compare_selection = bsite_selection_full.text()[0]
        except:
            progressDialog.close()
            QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "Invalid selection \n\nPlease select b-site or chain!")
            return None

        if bsite_space_check == True:
            for element in BindingSites.bsite_unique_centers:
                if element[0] == bsite_selection:
                    SELECTED_SITE = element
                    SELECTED_SITE_CHAIN = str(chain_selection).upper()

        if bsite_space_check == False:
            for element in BindingSites.bsite_unique_centers:
                if element[0] == bsite_selection:
                    SELECTED_SITE = element
                    SELECTED_SITE_CHAIN = str(chain_selection).upper()
        
        if chain_sel == True:
            SELECTED_SITE = []
            SELECTED_SITE.append("no binding site used in analysis")
            SELECTED_SITE_CHAIN = str(whole_chain_compare_selection).upper()
                
        else:
            pass
        update_win()
        #report 2,3,4,5,6,7,8,9, 10
        report_list_1.append("\n\n\nExamined complex: " + target_complex_2)
        report_list_1.append("Whole chain setting used: " + str(chain_sel))
        if chain_sel == True:
            report_list_1.append("Whole chain selection: " + whole_chain_compare_selection)
            report_list_1.append("Binding site selection: / (not used)")
            report_list_1.append("Chain selection: " + whole_chain_compare_selection)
        else:
            report_list_1.append("Whole chain selection: / (not used)")
            report_list_1.append("Binding site selection: " + bsite_selection)
            report_list_1.append("Chain selection: " + chain_selection)

        report_list_1.append("Unique structures: " + str(examined_list_unique) + "\n\n\n")

        report_list_1.append("**************************************")
        report_list_1.append("***Doing MD trajectory analysis !!!***")
        report_list_1.append("**************************************")


        probis_starter=probis_exe_dir
        
        print("ProBiS H2O MD: creating query protein .srf file")
        if platform == "win32":
            if chain_sel == False:
                subprocess.run(args=[probis_starter, "-ncpu", processors_available_local, 
                                     "-extract", "-bsite", bsite_selection, "-dist 3.0", "-f1", 
                                     "{}.pdb".format(protein), "-c1", chain_selection, 
                                     "-srffile", "{}.srf".format(protein)])              
            else:
                subprocess.run(args=[probis_starter, "-ncpu", processors_available_local, 
                                     "-extract" "-f1", "{}.pdb".format(protein), "-c1", 
                                     whole_chain_compare_selection, "-srffile", 
                                     "{}.srf".format(protein)])
                
        elif platform == "linux":
            if chain_sel == False:
                subprocess.run(args=[probis_dir, "-ncpu", processors_available_local, 
                                     "-extract", "-bsite", bsite_selection, "-dist 3.0", "-f1", 
                                     "{}.pdb".format(protein), "-c1", chain_selection, 
                                     "-srffile", "{}.srf".format(protein)])
            else:
                subprocess.run(args=[probis_dir, "-ncpu", processors_available_local, 
                                     "-extract" "-f1", "{}.pdb".format(protein), "-c1", 
                                     whole_chain_compare_selection, "-srffile", 
                                     "{}.srf".format(protein)])
        update_win()
        
        master_chain_list = []
        zacasna_delete_lista = []

        open("./srfs.txt", 'w').close()
        prot_list = []
        

        for element in examined_list_unique:
            unique_chain_list = []
            try:
                with open(element + ".pdb", "rt") as infile:
                    for linenumber, line in enumerate(infile):
                        if line.startswith("HETATM"):
                            unique_chain = str(line[21])
                            unique_chain_list.append(unique_chain)
                        elif line.startswith("ATOM"):
                            unique_chain = str(line[21])
                            unique_chain_list.append(unique_chain)
                        else:
                            pass

                unique_chain_list = list(set(unique_chain_list))
                master_chain_list.append(unique_chain_list)
                
                for chain_id in unique_chain_list:
                    # zacasna lista of unwanted
                    wanted_chains = ["A", "a", "B", "b", "C", "c", "D", "d", "E", "e", "F", "f", "G", "g", "H", "h", "I", "i", "J", "j", "K", "k", "L", "l", "M", "m", "N", "n", "O", "o", "P", "p", "Q", "q", "R", "r", "S", "s", "T", "t", "U", "u", "V", "v", "Z", "z", "X", "x", "Y", "y"]
                    if chain_id in wanted_chains:
                        if platform == "win32":
                            subprocess.run(args=[probis_starter, "-ncpu", processors_available_local, 
                                                 "-extract", "-f1", "{}.pdb".format(element), "-c1", 
                                                 chain_id, "-srffile", "{}{}.srf".format(element, chain_id)])
                        else:
                            subprocess.run(args=[probis_dir, "-ncpu", processors_available_local, 
                                                 "-extract", "-f1", "{}.pdb".format(element), "-c1", 
                                                 chain_id, "-srffile", "{}{}.srf".format(element, chain_id)])
                        srf_fajl = open("./srfs.txt", 'a')
                        srf_fajl.write(element + chain_id + ".srf " + chain_id + "\n")
                        srf_fajl.close()
                        #to je korekcija za longnames
                        #################################################
                        if platform == "win32":
                            os.system("copy {}.pdb {}{}.pdb".format(element, element, chain_id))
                        else:
                            os.system("cp {}.pdb {}{}.pdb".format(element, element, chain_id))
                        zacasna_delete_lista.append("{}{}.pdb".format(element, chain_id))
                        prot_list.append(element + " " + chain_id)
                    else:
                        pass
            except OSError:
                print("Database File not Found!, please investigate.")
            update_win()
                
    ##############################
        if platform == "win32":
            os.system("DEL *.rota.pdb /S")
            os.system("DEL AAA_NOSQL.nosql /S")
        else:
            os.system("rm ./*.rota.pdb")
            os.system("rm ./AAA_NOSQL.nosql")
    ##############################
        print("ProBiS H2O MD: comparing protein structures")
        update_win()
        if platform == "win32":
            if chain_sel == False:
                subprocess.run(args=[probis_starter, "-ncpu", processors_available_local, 
                                     "-surfdb", "-local", "-sfile", "srfs.txt", "-f1", 
                                     "{}.srf".format(protein), "-c1", 
                                     chain_selection, "-nosql", "AAA_NOSQL.nosql"])
                subprocess.run(args=[probis_starter, "-ncpu", processors_available_local, 
                                     "-results", "-f1", "{}.pdb".format(protein), "-c1", 
                                     chain_selection, "-nosql", "AAA_NOSQL.nosql", "-json", "AAA_NOSQL.json"])
            else:
                subprocess.run(args=[probis_starter, "-ncpu", processors_available_local, 
                                     "-surfdb", "-sfile", "srfs.txt", "-f1", "{}.srf".format(protein), 
                                     "-c1", whole_chain_compare_selection, "-nosql", "AAA_NOSQL.nosql"])
                
                subprocess.run(args=[probis_starter, "-ncpu", processors_available_local, 
                                     "-results", "-f1", "{}.pdb".format(protein), "-c1", 
                                     whole_chain_compare_selection, "-nosql", "AAA_NOSQL.nosql", "-json", "AAA_NOSQL.json"])
        else:
            if chain_sel == False:
                subprocess.run(args=[probis_dir, "-ncpu", processors_available_local, 
                                     "-surfdb", "-local", "-sfile", "srfs.txt", "-f1", 
                                     "{}.srf".format(protein), "-c1", 
                                     chain_selection, "-nosql", "AAA_NOSQL.nosql"])
                subprocess.run(args=[probis_dir, "-ncpu", processors_available_local, 
                                     "-results", "-f1", "{}.pdb".format(protein), "-c1", 
                                     chain_selection, "-nosql", "AAA_NOSQL.nosql", "-json", "AAA_NOSQL.json"])
            else:
                subprocess.run(args=[probis_dir, "-ncpu", processors_available_local, 
                                     "-surfdb", "-sfile", "srfs.txt", "-f1", "{}.srf".format(protein), 
                                     "-c1", whole_chain_compare_selection, "-nosql", "AAA_NOSQL.nosql"])

                subprocess.run(args=[probis_dir, "-ncpu", processors_available_local, 
                                     "-results", "-f1", "{}.pdb".format(protein), "-c1", 
                                     whole_chain_compare_selection, "-nosql", "AAA_NOSQL.nosql", "-json", "AAA_NOSQL.json"])
        print("ProBiS H2O MD: comparison of protein structures complete")
        update_win()
        
        for element in prot_list:
            print("ProBiS H2O MD: aligning protein:    ", element)
            ele0=element.split()[0]
            ele1=element.split()[1]
            
            if platform == "win32":
                if chain_sel == False:
                    subprocess.run(args=[probis_starter, "-ncpu", processors_available_local, 
                                         "-align", "-bkeep", "-alno", "0", "-nosql", "AAA_NOSQL.nosql", "-f1", 
                                         "{}.pdb".format(protein), "-c1", chain_selection, 
                                         "-f2", "{}.pdb".format(ele0), "-c2", 
                                         ele1])
                else:
                    subprocess.run(args=[probis_starter, "-ncpu", processors_available_local, 
                                         "-align", "-bkeep", "-alno", "0", "-nosql", "AAA_NOSQL.nosql", "-f1", 
                                         "{}.pdb".format(protein), "-c1", 
                                         whole_chain_compare_selection, "-f2", 
                                         "{}.pdb".format(ele0), "-c2", ele1]) 
            else:
                if chain_sel == False:
                    subprocess.run(args=[probis_dir, "-ncpu", processors_available_local, 
                                         "-align", "-bkeep", "-alno", "0", "-nosql", "AAA_NOSQL.nosql", "-f1", 
                                         "{}.pdb".format(protein), "-c1", chain_selection, 
                                         "-f2", "{}.pdb".format(ele0), "-c2", 
                                         ele1])
                else:
                    subprocess.run(args=[probis_dir, "-ncpu", processors_available_local, 
                                         "-align", "-bkeep", "-alno", "0", "-nosql", "AAA_NOSQL.nosql", "-f1", 
                                         "{}.pdb".format(protein), "-c1", 
                                         whole_chain_compare_selection, "-f2", 
                                         "{}.pdb".format(ele0), "-c2", ele1]) 
            update_win()
            
        print("Rotas done ... (alignment 0 !)")


        # correction of multiple or one chain per protein allignment
        aligned_unique = []
        aligned_discard = []
        helper = []

        update_win()
        PDB_master_file_list = []
        imena_fajlov = glob("*.rota.pdb")
        # ZADNJI JE KOMPLEKS
        
        imena_fajlov.append(protein + ".pdb")

        for ime_fajla in imena_fajlov:
            print("ProBiS H2O MD: analyzing waters in file:    ", ime_fajla)
            lista_h2o = []
            imena_vod = ["HOH", "SPC", "T3P", "T4P"]
            update_win()
            with open(ime_fajla, 'r') as brani_fajl:
                for linija in brani_fajl:
                    if linija.startswith("HETATM") or linija.startswith("ATOM"):
                        if linija[17:20] in imena_vod:
                            if len(linija) <= 78:
                                linija += "                                        "
                                linija = linija[:77]
                            if linija[21] == " ":
                                linija = linija[:21]+"X"+linija[22:]
                            voda = [linija[0:6].strip(), linija[6:11].strip(), 
                                    linija[12:16].strip(), linija[17:20].strip(),
                                    linija[21], linija[22:26].strip(), 
                                    linija[30:38].strip(), linija[38:46].strip(),
                                    linija[46:54].strip(), linija[54:60].strip(),
                                    linija[60:66].strip(), linija[76:78].strip(),
                                    ime_fajla]
                            lista_h2o.append(voda)
                        elif linija[17:21] == "TIP3" and linija[12:16].strip() == "OH2":
                            if len(linija) <= 78:
                                linija += "                                        "
                                linija = linija[:77]
                            if linija[21] == " ":
                                linija = linija[:21]+"X"+linija[22:]
                            voda = [linija[0:6].strip(), linija[6:11].strip(), 
                                    linija[12:16].strip(), linija[17:20].strip(),
                                    linija[21], linija[22:26].strip(), 
                                    linija[30:38].strip(), linija[38:46].strip(),
                                    linija[46:54].strip(), linija[54:60].strip(),
                                    linija[60:66].strip(), linija[76:78].strip(),
                                    ime_fajla]
                            lista_h2o.append(voda)
                    elif linija.startswith("ENDMDL"):
                        break
                    
                    else:
                        pass
                        
                PDB_master_file_list.append(lista_h2o)

        # ----debug-----------------------------------------------------------------

        #print (PDB_master_file_list)
        MASTER_h2o_list = []
        fajl_list = []
        update_win()
        for PDB_water_fajl in PDB_master_file_list:
            for water_mol in PDB_water_fajl:
                if len(water_mol) < 13:
                    water_mol.insert(4, unique_chain_list[0])
                if water_mol[4] == '':
                    water_mol[4] = unique_chain_list[0]

            for het, stev, atom, molekula, veriga, zapor, x, y, z, occ, R, syst, fajl in PDB_water_fajl:
                test = []
                test.append(x), test.append(y), test.append(z), test.append(fajl), test.append(R), test.append(zapor)

                MASTER_h2o_list.append(test)
                fajl_list.append(fajl)

        entities = int(len(set(fajl_list)))
        
        dialog.PlainInfo.clear()
        # REPORT list 11,12
        report_list_1.append("Master water list includes %r waters" % (len(MASTER_h2o_list)))

        dialog.PlainInfo.insertPlainText("Master water list includes %r molecules /n" % (len(MASTER_h2o_list)))
        dialog.PlainInfo.insertPlainText("Superimposed chains: %r\n" % (entities))

        print("Master lista vod narejena in vsebuje %r vod" % (len(MASTER_h2o_list)))
        print("Writing H2O master list")
        nova_datoteka = open("master_water_list.txt", "w")
        for tocka in MASTER_h2o_list:
            nova_datoteka.write("%s\n" % tocka)
        nova_datoteka.close()
        print("done...")
        #nb.select(p2)
        dialog.tabWidget.setCurrentIndex(1)

        # DBSCAN formatting-----------------------------------------------------

        # binding site clustering
        # BindingSites.bsite_unique_centers
        master_bsite_lista_vod = []
        master_bsite_lista_vod_koordinata_x = []
        master_bsite_lista_vod_koordinata_y = []
        master_bsite_lista_vod_koordinata_z = []

        master_lista_vod = []
        master_lista_vod_koordinata_x = []
        master_lista_vod_koordinata_y = []
        master_lista_vod_koordinata_z = []

        # POPRAVA VOD DA NE SEGAJO IZVEN CHAINA

        correction_x = []
        correction_y = []
        correction_z = []
        
        for element in lista_za_atome_xyzchain:
            if SELECTED_SITE_CHAIN == element[3]:
                correction_x.append(float(element[0]))
                correction_y.append(float(element[1]))
                correction_z.append(float(element[2]))

        global atom_max_x
        global atom_min_x
        global atom_max_y
        global atom_min_y
        global atom_max_z
        global atom_min_z

        atom_max_x = max(correction_x) + 4
        atom_min_x = min(correction_x) - 4
        atom_max_y = max(correction_y) + 4
        atom_min_y = min(correction_y) - 4
        atom_max_z = max(correction_z) + 4
        atom_min_z = min(correction_z) - 4

        global boundingBox

        boundingBox = [LINEWIDTH, 2.0, BEGIN, LINES,
                COLOR, float(1), float(0), float(0),

                VERTEX, atom_min_x, atom_min_y, atom_min_z,       #1
                VERTEX, atom_min_x, atom_min_y, atom_max_z,       #2

                VERTEX, atom_min_x, atom_max_y, atom_min_z,       #3
                VERTEX, atom_min_x, atom_max_y, atom_max_z,       #4

                VERTEX, atom_max_x, atom_min_y, atom_min_z,       #5
                VERTEX, atom_max_x, atom_min_y, atom_max_z,       #6

                VERTEX, atom_max_x, atom_max_y, atom_min_z,       #7
                VERTEX, atom_max_x, atom_max_y, atom_max_z,       #8


                VERTEX, atom_min_x, atom_min_y, atom_min_z,       #1
                VERTEX, atom_max_x, atom_min_y, atom_min_z,       #5

                VERTEX, atom_min_x, atom_max_y, atom_min_z,       #3
                VERTEX, atom_max_x, atom_max_y, atom_min_z,       #7

                VERTEX, atom_min_x, atom_max_y, atom_max_z,       #4
                VERTEX, atom_max_x, atom_max_y, atom_max_z,       #8

                VERTEX, atom_min_x, atom_min_y, atom_max_z,       #2
                VERTEX, atom_max_x, atom_min_y, atom_max_z,       #6


                VERTEX, atom_min_x, atom_min_y, atom_min_z,       #1
                VERTEX, atom_min_x, atom_max_y, atom_min_z,       #3

                VERTEX, atom_max_x, atom_min_y, atom_min_z,       #5
                VERTEX, atom_max_x, atom_max_y, atom_min_z,       #7

                VERTEX, atom_min_x, atom_min_y, atom_max_z,       #2
                VERTEX, atom_min_x, atom_max_y, atom_max_z,       #4

                VERTEX, atom_max_x, atom_min_y, atom_max_z,       #6
                VERTEX, atom_max_x, atom_max_y, atom_max_z,       #8

                END
        ]


        # ----------------------------------------------------------------------
        update_win()
        # cluster TESTING
        # mlv_datoteka = open("random_water_list5.txt", "r")

        mlv_datoteka = open("master_water_list.txt", "r")
        for linija in mlv_datoteka:
            vmesna_lista = []
            linija2 = linija.replace("[", "")
            linija3 = linija2.replace("]", "")
            linija4 = linija3.replace(" ", "")
            linija5 = linija4.replace("'", "")
            linija_lista = linija5.split(",")
            x = float(linija_lista[0])
            y = float(linija_lista[1])
            z = float(linija_lista[2])

            vmesna_lista.append(x)
            vmesna_lista.append(y)
            vmesna_lista.append(z)

            if bsite_space_check == True and chain_sel == False:
                if SELECTED_SITE[4] - 4 <= vmesna_lista[0] <= SELECTED_SITE[5] + 4:
                    if SELECTED_SITE[6] - 4 <= vmesna_lista[1] <= SELECTED_SITE[7] + 4:
                        if SELECTED_SITE[8] - 4 <= vmesna_lista[2] <= SELECTED_SITE[9] + 4:
                            master_bsite_lista_vod.append(vmesna_lista)
                            x2 = float(vmesna_lista[0])
                            master_bsite_lista_vod_koordinata_x.append(x2)
                            y2 = float(vmesna_lista[1])
                            master_bsite_lista_vod_koordinata_y.append(y2)
                            z2 = float(vmesna_lista[2])
                            master_bsite_lista_vod_koordinata_z.append(z2)
                        else:
                            pass
                    else:
                        pass
                else:
                    pass

            if bsite_space_check == False and chain_sel == False:
                if atom_min_x <= vmesna_lista[0] <= atom_max_x:
                    if atom_min_y <= vmesna_lista[1] <= atom_max_y:
                        if atom_min_z <= vmesna_lista[2] <= atom_max_z:
                            master_lista_vod_koordinata_x.append(x)
                            master_lista_vod_koordinata_y.append(y)
                            master_lista_vod_koordinata_z.append(z)
                            master_lista_vod.append(vmesna_lista)

            if chain_sel == True:
                if atom_min_x <= vmesna_lista[0] <= atom_max_x:
                    if atom_min_y <= vmesna_lista[1] <= atom_max_y:
                        if atom_min_z <= vmesna_lista[2] <= atom_max_z:
                            master_lista_vod_koordinata_x.append(x)
                            master_lista_vod_koordinata_y.append(y)
                            master_lista_vod_koordinata_z.append(z)
                            master_lista_vod.append(vmesna_lista)
            else:
                pass


        mlv_datoteka.close()
        # /DBSCAN formatting----------------------------------------------------

        def display_cluster_info(mlist, mlist_x, mlist_y, mlist_z, epsilon):
            try:
                x_dim = max(mlist_x) - min(mlist_x)
                y_dim = max(mlist_y) - min(mlist_y)
                z_dim = max(mlist_z) - min(mlist_z)
            except:
                progressDialog.close()
                print("ProBiS H2O MD: Please select setting and files that contain water. \nProblem may have occured due to the water-protein distance selected being too short, or due to the protein structure not having any water molecules in selected range")
                QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "ProBiS H2O MD: Please select setting and files that contain water. \nProblem may have occured due to the water-protein distance selected being too short, or due to the protein structure not having any water molecules in selected range")
                
            system_volume = round(x_dim * y_dim * z_dim)

            dialog.PlainInfo.insertPlainText("System volume is: %d cubic A\n" % (system_volume))
            
            # report list 13,14,15
            report_list_1.append("System volume is: %d cubic A\n" % (system_volume))
            report_list_1.append("IDENTIFIED CLUSTERS: \n")
            report_list_1.append("-" * 25)


            def calculate_clusters(lista, sample_size):
                lista_np_array = np.array(lista)
                selected_eps=dialog.SpinDB.value()
                labels3D = DBSCAN(eps=selected_eps, min_samples=sample_size).fit_predict(lista_np_array)
                n_clusters_3D = len(set(labels3D)) - (1 if -1 in labels3D else 0)

                return int(n_clusters_3D)


            dialog.ListCalculated.clear()
            start_population = 2
            clus_num = calculate_clusters(mlist, start_population)
            cluster_collate_list = []
            max_population = 0
            
            while clus_num >= 1:

                consv = round((float(start_population)/float(entities)), 2)
                # limita ena je overloaded ker je lahko teoreticno prisotnih vec molekul vode na istem mestu v istem kristalu
                # glede na to da je smisel tega orodja v eksperimentalnih podatkih bi bile taksne vode na lokaciji manjsi od 1 A
                # nesmiselne in korigirane s strani kristalografa
                # zato lahko komot v skrajno nenavadno ali eksp-nekorigiranem primeru vrednost consv presega 1
                # taksne primere tukaj reduciramo na vrednost 1 kar pomeni, da je voda na tej lokaciji nastopa v vseh eksperimentalnih entitetah
                if consv > 1:
                    consv = 1.0
                else:
                    pass

                # za report list------------------------------------------------
                text_consv = int(round(consv*10)) * "*"
                st = 10 - len(text_consv)
                text_consv += st * " "
                if str(clus_num) == "1":
                    text = (str(clus_num) + " cluster with " + str(start_population)
                            + " H2O molecules. " +  "consv. " + str(consv))
                else:
                    text = (str(clus_num) + " clusters with " + str(start_population)
                            + " H2O molecules. " +  "consv. " + str(consv))
                num_spaces = 55 - len(text)
                report_list_1.append(text +  + num_spaces * " " + "[" + text_consv + "]")
                # REPORT LIST APPEND--------------------------------------------

                cluster_collate_list.append([clus_num, start_population, text])
                start_population += 1
                clus_num = calculate_clusters(mlist, start_population)
                max_population = start_population

            temp_collate = []
            temp_collate_extended = []
            for cluster_num, start_population, list_text in reversed(cluster_collate_list):
                if cluster_num not in temp_collate:
                    dialog.ListCalculated.addItem(list_text)
                    temp_collate.append(cluster_num)
                    temp_collate_extended.append(list_text.split()[7])
                else:
                    pass
                update_win()

            calculated_items = []
            for x in range(dialog.ListCalculated.count()):
                calculated_items.append(dialog.ListCalculated.item(x))
                update_win()
            
            update_win()
            for i, listbox_entry in enumerate(calculated_items):
                if 0.9 <= float(listbox_entry.text().split()[7]) :
                    dialog.ListCalculated.item(i).setBackground(red10)
                elif 0.8 <=float(listbox_entry.text().split()[7]) < 0.9:
                    dialog.ListCalculated.item(i).setBackground(red09)
                elif 0.7 <=float(listbox_entry.text().split()[7]) < 0.8:
                    dialog.ListCalculated.item(i).setBackground(red08)
                elif 0.6 <=float(listbox_entry.text().split()[7]) < 0.7:
                    dialog.ListCalculated.item(i).setBackground(red07)
                elif 0.5 <=float(listbox_entry.text().split()[7]) < 0.6:
                    dialog.ListCalculated.item(i).setBackground(red06)
                elif 0.4 <=float(listbox_entry.text().split()[7]) < 0.5:
                    dialog.ListCalculated.item(i).setBackground(red05)
                elif 0.3 <=float(listbox_entry.text().split()[7]) < 0.4:
                    dialog.ListCalculated.item(i).setBackground(red04)
                elif 0.2 <=float(listbox_entry.text().split()[7]) < 0.3:
                    dialog.ListCalculated.item(i).setBackground(red03)
                elif 0.1 <=float(listbox_entry.text().split()[7]) < 0.2:
                    dialog.ListCalculated.item(i).setBackground(red02)
                else:
                    dialog.ListCalculated.item(i).setBackground(red01)
            
            update_win()
            # report list  15
            report_list_1.append("-" * 25)
            max_pop_text = "Maximum occupied cluster contains %d H2O molecules \n binding site: %s" % (max_population - 1, SELECTED_SITE[0])
            dialog.PlainInfo.insertPlainText(max_pop_text)
            report_list_1.insert(10, max_pop_text)

            return temp_collate_extended.count("1.0")

        def display_clusters():
            if chain_sel == True:
                consv_1_count = display_cluster_info(master_lista_vod, master_lista_vod_koordinata_x,
                                    master_lista_vod_koordinata_y, master_lista_vod_koordinata_z, epsilon_param)
            else:
                if bsite_space_check == True:
                    consv_1_count = display_cluster_info(master_bsite_lista_vod, master_bsite_lista_vod_koordinata_x,
                                        master_bsite_lista_vod_koordinata_y, master_bsite_lista_vod_koordinata_z, epsilon_param)
                else:
                    consv_1_count = display_cluster_info(master_lista_vod, master_lista_vod_koordinata_x,
                                        master_lista_vod_koordinata_y, master_lista_vod_koordinata_z, epsilon_param)
            update_win()
            return consv_1_count
            

        global epsilon_param
        update_win()
        
        epsilon_param=dialog.SpinDB.value()
        # consv_1_count = display_clusters()
        display_clusters()
        print(("Used epsilon in MD clustering was: " + str(epsilon_param)))

        
        try:
            update_win()
        except:
            pass
        # cleanup
        if platform == "win32":
            os.system("DEL *.pdb /S")
            os.system("DEL *.ent.gz /S")
            os.system("DEL *.srf /S")
            os.system("DEL *.rota.pdb /S")
            os.system("DEL AAA_NOSQL* /S")
            os.system("DEL query.json /S")
            os.system("DEL info.json /S")
            os.system("MOVE /Y srfs.txt srfs.txt.backup")
        else:
            os.system("rm ./*.pdb")
            os.system("rm ./*.ent.gz")
            os.system("rm ./*.srf")
            os.system("rm ./*.rota.pdb")
            os.system("rm ./AAA_NOSQL*")
            os.system("rm ./query.json")
            os.system("rm ./info.json")
            os.system("mv ./srfs.txt ./srfs.txt.backup")
            for element in zacasna_delete_lista:
                os.system("rm %s" %(element))


        progressDialog.close()
        return None

 
class pyMOLinterface:
    """use wonderful pyMol for visualisation of collected results"""
    """thanks! Warren L. DeLano!"""

    @staticmethod
    def pyMOL_water_contacts():    
        prot = dialog.LineTop.text().lower()
        prot1= os.path.normpath(prot).split(os.path.sep)
        target_complex_3 = prot1[-1][0:-4]
            
        # za H-bond mejno razdajo kar 4 A
        cmd.select("bsites", "H2O* around 6")
        cmd.select("byres bsites")
        cmd.zoom("byres bsites")
        cmd.deselect()

        cmd.select("protein_", "polymer and {}".format(target_complex_3))
        cmd.select("ligand_", "organic and {}".format(target_complex_3))
        cmd.select("conserved_waters", "H2O*")
        cmd.select("donors_", "(elem n,o and (neighbor hydro)) and {}".format(target_complex_3))
        cmd.select("acceptors_", "(elem o or (elem n and not (neighbor hydro))) and {}".format(target_complex_3))
        cmd.distance("prot_acceptors", "(protein_ and acceptors_)", "conserved_waters", "4.0")
        cmd.distance("prot_donors", "(protein_ and donors_)", "conserved_waters", "4.0")
        cmd.distance("ligand_acceptors", "(ligand_ and acceptors_)", "conserved_waters", "4.0")
        cmd.distance("ligand_donors", "(ligand_ and donors_)", "conserved_waters", "4.0")
        cmd.distance("inter_cons_H2O", "conserved_waters", "conserved_waters", "4.0")
        cmd.delete("donors_")
        cmd.delete("acceptors_")
        cmd.set("dash_color", "magenta")
        cmd.set("dash_gap", "0.2")
        cmd.set("dash_length", "0.2")
        cmd.set("dash_round_ends","on")
        cmd.set("dash_width","3")

    @staticmethod
    def pyMOL_fetch_system():
        prot = dialog.LineTop.text().lower()
        prot1= os.path.normpath(prot).split(os.path.sep)
        target_complex_3 = prot1[-1].split(".")[0]
        cmd.delete(name = "all")
        cmd.load(dialog.LineTop.text(), target_complex_3)
        cmd.center(target_complex_3)
        cmd.hide("everything", target_complex_3)
        cmd.show ("cartoon", target_complex_3)
        cmd.set("cartoon_color", "white", target_complex_3)
        cmd.hide("lines", "all")
        cmd.util.cbag(selection = target_complex_3)
        cmd.show("surface", target_complex_3)
        cmd.set("transparency", "0.9")
        cmd.set("surface_color", "white")
        cmd.show("sticks", "organic")
        cmd.color("blue", "organic")
        waters = "{}_waters".format(target_complex_3)
        cmd.select(waters, "resn hoh")
        cmd.show("nonbonded", waters)
        cmd.deselect()

    @staticmethod
    def pyMOL_fetch_MD():
        cmd.load_traj(dialog.LineTraj.text())

    @staticmethod
    def pyMOL_chain_box():
        cmd.frame("0")
        pymol.cmd.load_cgo(boundingBox, "box")

    @staticmethod
    def pyMOL_bsite_cluster():
        # display binding sites of clusters
        cmd.frame("0")
        cmd.select("bsites", "H2O* around 6")
        cmd.select("byres bsites")
        cmd.zoom("byres bsites")
        cmd.show("sticks", "byres bsites")
        cmd.util.cbay("byres bsites")
        cmd.set_bond("stick_radius", "0.1", "byres bsites")
        cmd.select("sele", "name ca and byres bsites")
        cmd.label("sele", "'\{}-{}\'.format(resn, resi)")
        cmd.set("label_size", "18")
        cmd.set("label_font_id", "7")
        cmd.show("sticks", "organic")
        cmd.color("blue", "organic")
        cmd.util.cnc ("organic")
        cmd.set_bond("stick_radius", "0.25", "organic")
    @staticmethod
    def pyMOL_display_cluster():
        cmd.frame("0")
        display_clusters_setting = dialog.CheckKeep.isChecked()
        bsite_space_check = dialog.CheckAnalyze.isChecked()
        chain_sel = dialog.CheckCompare.isChecked()

        # za korekcijo in pogled R faktorja
        debye_waller_check = dialog.CheckDebye.isChecked()

        # DBSCAN formatting-----------------------------------------------------

        # binding site clustering
        # BindingSites.bsite_unique_centers
        master_bsite_lista_vod = []
        master_bsite_lista_vod_koordinata_x = []
        master_bsite_lista_vod_koordinata_y = []
        master_bsite_lista_vod_koordinata_z = []
        master_lista_vod = []
        master_lista_vod_koordinata_x = []
        master_lista_vod_koordinata_y = []
        master_lista_vod_koordinata_z = []
        # za Debye Waller
        master_bsite_lista_atom_iso_displacement = []
        master_lista_atom_iso_displacement = []
        # master_lista_names = []
        master_bsite_lista_info = []
        master_lista_info = []

        mlv_datoteka = open("master_water_list.txt", "r")
        dialog.PushDisplay.setText("Wait...")
        dialog.PushDisplay.setEnabled(False)
        update_win()
        for linija in mlv_datoteka:
            vmesna_lista = []
            linija2 = linija.replace("[", "")
            linija3 = linija2.replace("]", "")
            linija4 = linija3.replace(" ", "")
            linija5 = linija4.replace("'", "")
            linija_lista = linija5.split(",")
            x = float(linija_lista[0])
            y = float(linija_lista[1])
            z = float(linija_lista[2])
            B = float(linija_lista[4])
            if B < 0:
                B = 0
            info = str(linija_lista[3]) + " location: " + str(linija_lista[5].strip("\n"))


            # anizotropni displcement bomo implementirali v V2 hopefully
            # + 1.4 je zaradi r H2O
            isotropni_displacement = math.sqrt(B/(8*((math.pi)**2))) + 1.4


            # master_lista_names.append(ime)


            vmesna_lista.append(x)
            vmesna_lista.append(y)
            vmesna_lista.append(z)



            if bsite_space_check == True and chain_sel == False:
                if SELECTED_SITE[4] - 4 <= vmesna_lista[0] <= SELECTED_SITE[5] + 4:
                    if SELECTED_SITE[6] - 4 <= vmesna_lista[1] <= SELECTED_SITE[7] + 4:
                        if SELECTED_SITE[8] - 4 <= vmesna_lista[2] <= SELECTED_SITE[9] + 4:
                            master_bsite_lista_vod.append(vmesna_lista)
                            x2 = float(vmesna_lista[0])
                            master_bsite_lista_vod_koordinata_x.append(x2)
                            y2 = float(vmesna_lista[1])
                            master_bsite_lista_vod_koordinata_y.append(y2)
                            z2 = float(vmesna_lista[2])
                            master_bsite_lista_vod_koordinata_z.append(z2)
                            master_bsite_lista_atom_iso_displacement.append(isotropni_displacement)
                            master_bsite_lista_info.append(info)

            if bsite_space_check == False and chain_sel == False:
                if atom_min_x <= vmesna_lista[0] <= atom_max_x:
                    if atom_min_y <= vmesna_lista[1] <= atom_max_y:
                        if atom_min_z <= vmesna_lista[2] <= atom_max_z:
                            master_lista_vod_koordinata_x.append(x)
                            master_lista_vod_koordinata_y.append(y)
                            master_lista_vod_koordinata_z.append(z)
                            master_lista_vod.append(vmesna_lista)
                            master_lista_atom_iso_displacement.append(isotropni_displacement)
                            master_lista_info.append(info)

            if chain_sel == True:
                if atom_min_x <= vmesna_lista[0] <= atom_max_x:
                    if atom_min_y <= vmesna_lista[1] <= atom_max_y:
                        if atom_min_z <= vmesna_lista[2] <= atom_max_z:
                            master_lista_vod_koordinata_x.append(x)
                            master_lista_vod_koordinata_y.append(y)
                            master_lista_vod_koordinata_z.append(z)
                            master_lista_vod.append(vmesna_lista)
                            master_lista_atom_iso_displacement.append(isotropni_displacement)
                            master_lista_info.append(info)


            else:
                pass
            
            QtWidgets.QApplication.processEvents()

        # /DBSCAN formatting----------------------------------------------------

        # BSITE LOKALNO ALI GLOBALNO NA VERIGI
        if bsite_space_check == True and chain_sel == False:
            master_lista_vod = master_bsite_lista_vod
            master_lista_atom_iso_displacement = master_bsite_lista_atom_iso_displacement
            master_lista_info = master_bsite_lista_info
        else:
            pass

        mlv_datoteka.close()


        try:
            cluster_selection = int(dialog.ListCalculated.currentItem().text().split()[3])
            consv_of_cluster = float(dialog.ListCalculated.currentItem().text().split()[7])
            report_list_1.append("\nBinding site info (name, avg x, y, z, min x, max x, min y, max y, min z, max z; box 4 A around extremes): \n" + str(SELECTED_SITE))
            report_list_1.append("\nExamined cluster with " + str(cluster_selection) + " H2O molecules\n")
            report_list_1.append("-" * 25)

        except:
            QtWidgets.QMessageBox.about(dialog, "ProBiS H2O MD Warning", "Please select clusters to display")
            dialog.PushDisplay.setText("Display")
            dialog.PushDisplay.setEnabled(True)

        labels3D = DBSCAN(eps=epsilon_param, min_samples=cluster_selection).fit_predict(np.array(master_lista_vod))

        i = 0
        tocke = []
        for element in labels3D:
            QtWidgets.QApplication.processEvents()
            temp = []
            if element != -1:
                temp.append(master_lista_vod[i])
                temp.append(element)
                temp.append(master_lista_atom_iso_displacement[i])
                temp.append(master_lista_info[i])
                report_list_1.append(temp)
                tocke.append(temp)

            else:
                pass
            i += 1


        if debye_waller_check == False:

            if display_clusters_setting == False:
                cmd.delete("H2O*")
                try:
                    cmd.delete("iso_disp")
                except:
                    pass
            else:
                pass

            for element in list(set(labels3D)):
                cluster_temp = []
                for sub_element in tocke:
                    if sub_element[1] == element:
                        cluster_temp.append(sub_element[0])
                try:
                    cmd.set_color("clus_color", "[%f, %f, %f]" % (1.0, (1.0 - consv_of_cluster), (1.0 - consv_of_cluster)))

                    pymol.cmd.do("pseudoatom H2O_clus-%d_%.2f, vdw=1, color=clus_color, pos=[%f, %f, %f]" % (element, consv_of_cluster, cluster_temp[0][0], cluster_temp[0][1], cluster_temp[0][2]))
                    cmd.show("spheres", "H2O_clus-%d*" % (element))
                except IndexError:
                    pass
                QtWidgets.QApplication.processEvents()

        else:
            cmd.delete("H2O*")
            try:
                cmd.delete("iso_disp")
            except:
                pass
                
            for element in list(set(labels3D)):
                cluster_temp = []
                for sub_element in tocke:
                    if sub_element[1] == element:
                        sub_element[0].append(sub_element[2])
                        if not sub_element[0] in cluster_temp:
                            cluster_temp.append(sub_element[0])
                try:
                    pymol.cmd.do("pseudoatom H2O_clus, vdw=1, color=red, pos=[%f, %f, %f]" % (cluster_temp[0][0], cluster_temp[0][1], cluster_temp[0][2]))
                    cmd.show("spheres", "H2O_clus")

                    for tocka in cluster_temp:
                        pymol.cmd.do("pseudoatom iso_disp, vdw=%f, color=red, pos=[%f, %f, %f]" % (cluster_temp[0][3], cluster_temp[0][0], cluster_temp[0][1], cluster_temp[0][2]))

                    cmd.show("dots", "iso_disp")

                except IndexError:
                    pass
            
                QtWidgets.QApplication.processEvents()


        # report on cluster
        prot = dialog.LineTop.text().lower()
        prot1= os.path.normpath(prot).split(os.path.sep)
        target_complex_3 = prot1[-1].split(".")[0]
        nova_datoteka = open("report_" + target_complex_3 + ".txt", "w")
        for linija in report_list_1:
            nova_datoteka.write("%s\n" % linija)
        nova_datoteka.close()
        print("report created...")
        dialog.PushDisplay.setText("Display")
        dialog.PushDisplay.setEnabled(True)
        QtWidgets.QApplication.processEvents()

def update_win():
    try:
        progressDialog.label.setText(random.choice(update_messages))
        progressDialog.setFocus()
    except:
        pass
    QtWidgets.QApplication.processEvents()